<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Auth::routes();

Route::get('login/{social}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{social}/callback', 'Auth\LoginController@handleProviderCallback');

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/supplier/all', 'SupplierController@all');
Route::resource('supplier', 'SupplierController');

Route::get('/merchant/all', 'MerchantController@all');
Route::resource('merchant', 'MerchantController');

Route::get('/product/all', 'ProductController@all');
Route::get('/product/category', 'ProductController@category');
Route::post('/product/storecategory', 'ProductController@storeCategory')->name('product.storeCategory');
Route::delete('/product/categorydestroy/{id}', 'ProductController@categoryDestroy')->name('product.categoryDestroy');
Route::resource('product', 'ProductController');

Route::get('/purchase/all', 'PurchaseController@all');
Route::resource('purchase', 'PurchaseController');

Route::get('/sale/all', 'SaleController@all');
Route::resource('sale', 'SaleController');

Route::get('/balance/all', 'BalanceController@all');
Route::post('/balance/bymonth', 'BalanceController@bymonth');
Route::post('/balance/byyear', 'BalanceController@byyear');
Route::resource('balance', 'BalanceController');

Route::get('/staff/all', 'StaffController@all');
Route::resource('staff', 'StaffController');

Route::get('/asset/all', 'AssetController@all');
Route::resource('asset', 'AssetController');

Route::get('/store/all', 'StoreController@all');
Route::resource('store', 'StoreController');

Route::get('/damage/all', 'DamageController@all');
Route::resource('damage', 'DamageController');

Route::get('/user/all', 'UserController@all');
Route::put('/user/updateemail/{id}/', 'UserController@updateemail');
Route::put('/user/updatepassword/{id}/', 'UserController@updatepassword');
Route::resource('user', 'UserController');

Route::get('/transaction/addbank', 'TransactionController@addbank');
Route::post('/transaction/addbankstore', 'TransactionController@addbankstore');
Route::delete('/transaction/destroyaccount/{id}', 'TransactionController@destroyaccount')->name('transaction.destroyaccount');
Route::get('/transaction/all', 'TransactionController@all');
Route::resource('transaction', 'TransactionController');

Route::get('/invoice/{id}/pdf', 'InvoiceController@pdf');
Route::post('/invoice/sendinvoicetoEmail', 'InvoiceController@sendInvoiceToEmail');
Route::put('/invoice/invoicepayment/{id}', 'InvoiceController@invoicepayment');
Route::get('/invoice/all', 'InvoiceController@all');
Route::resource('invoice', 'InvoiceController');

Route::get('/payment/all', 'PaymentController@all');
Route::resource('payment', 'PaymentController');

Route::get('/smsemail/all', 'SmsemailController@all');
Route::post('/smsemail/smsbynumbers', 'SmsemailController@smsbynumbers');
Route::post('/smsemail/smsbygroup', 'SmsemailController@smsbygroup');
Route::post('/smsemail/emailbyemails', 'SmsemailController@emailbyemails');
Route::post('/smsemail/emailbygroups', 'SmsemailController@emailbygroups');
Route::resource('smsemail', 'SmsemailController');

Route::resource('settings', 'SettingsController');
Route::put('/settings/purchasecode/{code}', 'SettingsController@purchasecode');

Route::get('/users/purchases/', 'UsersController@purchases');
Route::get('/users/purchases/view/{id}', 'UsersController@purchasesshow');
Route::get('/users/supplies/', 'UsersController@supplies');
Route::get('/users/supplies/view/{id}', 'UsersController@suppliesshow');
Route::get('/users/invoices/', 'UsersController@invoices');
Route::get('/users/invoices/view/{id}', 'UsersController@invoiceshow');

Route::post('/payment/paypal/store/', 'PaypalController@store');
Route::get('/payment/paypal/status', 'PaypalController@getPaymentStatus');

Route::post('/payment/stripe/', 'StripeController@stripe');
//Route::post('/payment/stripe/', 'StripeController@stripe');


Route::post('/pos/paypal/payment/', 'PosController@payment');
Route::get('/pos/paypal/payment/status', 'PosController@paypalStatus');

// Route::post('/payment/stripe/', 'PosController@stripe');
//Route::post('/payment/stripe/', 'PosController@stripe');

Route::resource('pos', 'PosController');
Route::post('/pos/getProductsByCat/', 'PosController@getProductsByCat');
Route::post('/pos/addToCart/', 'PosController@addToCart');
Route::post('/pos/adjustToCart/', 'PosController@adjustToCart');
Route::post('/pos/updateBalance/', 'PosController@updateBalance');
Route::post('/pos/resetpos/', 'PosController@resetpos');
Route::post('/pos/payemnt/', 'PosController@payment');

/************* Verify Routes *************/
Route::get('/verify/', 'VerifyController@index');
Route::put('/verify/storePurchasecode/{id}', 'VerifyController@storePurchasecode');
