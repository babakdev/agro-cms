;(function($){
    "use strict"

    $(function() {
        //$('.wrapper').overlayScrollbars({ });
        $('.fc-view-container').overlayScrollbars({ });
    });

     $('a, label, input').tooltip();

//    $('li.treeview a').on('mouseover', function () {
//        $(this).children('.material-icons.md-18').attr("style", "color:#fff");
//    });
//
//    $('li.treeview a').on('mouseleave', function () {
//        $(this).children('.material-icons.md-18').attr("style", "color:#3781ff");
//    });

    $('.sidebar-menu li a').on('mouseover', function () {
        $(this).children('.material-icons.md-18').attr("style", "color:#fff");
    });

    $('.sidebar-menu li a').on('mouseleave', function () {
        $(this).children('.material-icons.md-18').attr("style", "color:#3781ff");
    });

    //Hide Sidebar on mobile screen
    $('.sidebar-toggle.mobile').on('click', function () {
         $('.main-sidebar').toggle('slowly');
         // $(this).attr("class", "sidebar-toggle mobile off");
    });

    // $('.sidebar-toggle.mobile.off').on('click', function () {
    //      $('.main-sidebar').hide('slowly');
    //      $(this).attr("class", "sidebar-toggle mobile");
    // });



    var total = 0;
    var quantity = 0;
    var price = 0;
    var commission = 0;

    $('.price').on('change', function () {
        quantity = $(".quantity").val();
        price = $(".price").val();
        total = quantity * price;
        $(".total").val(total);

        commission = $(".commission").val();
        $(".total").val(total - commission);

        $(".quantity").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".price").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".commission").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".total").attr("style", "background:rgba(255, 235, 59, 0.28)");

    });

    $('.quantity').on('change', function () {
        quantity = $(".quantity").val();
        price = $(".price").val();
        total = quantity * price;
        $(".total").val(total);

        commission = $(".commission").val();
        $(".total").val(total - commission);

        $(".quantity").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".price").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".commission").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".total").attr("style", "background:rgba(255, 235, 59, 0.28)");
    });

    $('.commission').on('change', function () {
        commission = $(".commission").val();
        $(".total").val(total - commission);

        $(".quantity").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".price").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".commission").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".total").attr("style", "background:rgba(255, 235, 59, 0.28)");
    });


    $('.product').on('change', function () {
        var element = $(this).find('option:selected');
        var priceMain = element.attr("price");
        $(".price").val(priceMain);

        quantity = $(".quantity").val();
        price = $(".price").val();
        total = quantity * price;
        $(".total").val(total);

        commission = $(".commission").val();
        $(".total").val(total - commission);

        $(".quantity").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".price").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".commission").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".total").attr("style", "background:rgba(255, 235, 59, 0.28)");
        $(".product").attr("style", "background:rgba(255, 235, 59, 0.28)");

    });

    $('textarea').ckeditor();
    CKEDITOR.editorConfig = function (config) {
        config.height = '100px';
    };

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $('.select2').select2();

    $(".delete").click(function () {
        if (!confirm("Do you want to delete this?")) {
            return false;
        }
    });

    $(".addmore").click(function () {
        $(this).text(function (i, text) {
            return text === "-" ? "+" : "-";
        })
        $(this).closest(".form-group").closest(".col-md-12").next('.col-md-12').toggle("Slow");
    });

    $(".alert").delay(5000).hide("Slow");

    $(".photo_input").click(function () {
        $("#photo").click();
    });

    //POS Page Search Products & Ajax Load
    $('.searchBtn').on('click', function (event) {
        event.preventDefault();
        var category = $(this).closest('form').find('select#category').val();
        var url = baseurl + '/pos/getProductsByCat';

        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {"category": category, "_token": $('meta[name="csrf-token"]').attr('content')},
            async: true,
            beforeSend: function () {
                $("div#loading").delay(100).fadeIn();
            },
            success: function (data) {
                $(".dtPosProducts tbody").html(data);
                $("div#loading").delay(100).fadeOut("slow");
            }
        });

    });


    //POS Page Stripe Input
    $('.pos .paymentMethod').on('change', function () {

        var method = $(".pos select.paymentMethod").val();
        if(method == 2){
            $(".pos .stripeInput").show();
        }else{
            $(".pos .stripeInput").hide();
        }

    });


})(jQuery)


$(document).on('click', '#print', function () {
    window.print();
});


//POS Page Add Products To Cart & Ajax Load
$(document).on('click', '.addToCart', function (event) {
    event.preventDefault();

    var itemID = $(this).attr('data-itemID');
    var url = baseurl + '/pos/addToCart';

    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {"itemID": itemID, "_token": $('meta[name="csrf-token"]').attr('content')},
        async: true,
        beforeSend: function () {
            $("div#loading").delay(100).fadeIn();
        },
        success: function (data) {
            // console.log(data);
            $(".posCartTable tbody").html(data.html);
            $(".pos #modal-payment input.total").val(data.total);
            $(".pos #modal-payment input#xxtotal").val(data.total);
            $("div#loading").delay(100).fadeOut("slow");
        }
    });

});

//POS Page Adjust Products To Cart & Ajax Load
$(document).on('click', 'span.increment', function (event) {

    var itemID = $(this).closest('.input-group').find('input.adjustToCart').attr('data-itemID');
    var url = baseurl + '/pos/adjustToCart';

    console.log(itemID);

    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {"type": "plus", "itemID": itemID, "_token": $('meta[name="csrf-token"]').attr('content')},
        async: true,
        beforeSend: function () {
            $("div#loading").delay(100).fadeIn();
        },
        success: function (data) {
            // console.log(data);
            $(".posCartTable tbody").html(data.html);
            $(".pos #modal-payment input.total").val(data.total);
            $(".pos #modal-payment input#xxtotal").val(data.total);
            $("div#loading").delay(100).fadeOut("fast");
        }
    });
});


//POS Page Adjust Products To Cart & Ajax Load
$(document).on('click', 'span.decrement', function (event) {

    var itemID = $(this).closest('.input-group').find('input.adjustToCart').attr('data-itemID');
    var url = baseurl + '/pos/adjustToCart';

    console.log(itemID);

    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {"type": "minus", "itemID": itemID, "_token": $('meta[name="csrf-token"]').attr('content')},
        async: true,
        beforeSend: function () {
            $("div#loading").delay(100).fadeIn();
        },
        success: function (data) {
            // console.log(data);
            $(".posCartTable tbody").html(data.html);
            $(".pos #modal-payment input.total").val(data.total);
            $(".pos #modal-payment input#xxtotal").val(data.total);
            $("div#loading").delay(100).fadeOut("fast");
        }
    });
});


//POS Page Adjust Products To Cart & Ajax Load
$(document).on('click', 'a i.removeToCart', function (event) {

    var itemID = $(this).closest('a').attr('data-itemID');
    var url = baseurl + '/pos/adjustToCart';

    console.log(itemID);

    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {"type": "delete", "itemID": itemID, "_token": $('meta[name="csrf-token"]').attr('content')},
        async: true,
        beforeSend: function () {
            $("div#loading").delay(100).fadeIn();
        },
        success: function (data) {
            // console.log(data);
            $(".posCartTable tbody").html(data.html);
            $(".pos #modal-payment input.total").val(data.total);
            $(".pos #modal-payment input#xxtotal").val(data.total);
            $("div#loading").delay(100).fadeOut("fast");
        }
    });
});
