/*
 * Initialization DataTables
 * Initialize for Exports
 */


$('.dtGlobal').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtPosProducts').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: []
});

$('.dtSupplier').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtMerchant').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtProduct').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtPurchase').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtBalance').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtBankAcc').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtStaff').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtSale').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtbyyear').DataTable({
    dom: 'Bfrtip',
    "paging":   false,
    "ordering": false,
    searching: false,
    info: false,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtBank').DataTable({
    dom: 'Bfrtip',
    "paging":   false,
    "ordering": false,
    searching: false,
    info: false,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtStore').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtInvoice').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});

$('.dtAsset').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});



$('.dtPayment').DataTable({
    dom: 'Bfrtip',
    searching: true,
    info: true,
    responsive: true,
    buttons: [
        {
            extend: 'print',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-print fa-fw"></i> Print',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'copy',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-copy fa-fw"></i> Copy',
            titleAttr: 'Print',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
            titleAttr: 'PDF',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
            titleAttr: 'Excel',
            className: 'btn-primary mRight10'
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
            titleAttr: 'CSV',
            className: 'btn-primary mRight10'
        }

    ]
});
