-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 04:23 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abacus`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(191) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `photo`, `title`, `value`, `description`, `created_at`, `updated_at`) VALUES
(1, '1517294872.jpg', 'Pickup Truck', 10000, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-01-29 07:38:31', '2018-01-30 00:47:53'),
(2, '1517233217.png', 'Pickup Truck', 10000, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-01-29 07:40:18', '2018-01-29 07:40:18'),
(3, NULL, 'TEST ASSETS', 1700, '<p>THIS A TEST ASSETS</p>', '2018-03-16 08:42:08', '2018-03-16 08:42:08'),
(4, NULL, 'hola', 0, NULL, '2018-04-26 11:01:56', '2018-04-26 11:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `balances`
--

CREATE TABLE `balances` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `balancedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` int(191) NOT NULL,
  `by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `balances`
--

INSERT INTO `balances` (`id`, `type`, `title`, `description`, `balancedate`, `amount`, `by`, `created_at`, `updated_at`) VALUES
(1, 'Expense', 'Goods', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-04-05 18:00:00', 8000, 'Don', '2018-01-17 02:49:24', '2018-04-05 08:51:50'),
(2, 'Income', 'Sale', NULL, '2018-01-16 18:00:00', 1000, NULL, '2018-01-17 02:50:41', '2018-01-17 03:24:20'),
(3, 'Expense', 'Net Bill', NULL, '2018-11-29 18:00:00', 1500, NULL, '2018-01-17 03:03:56', '2018-11-29 13:16:07'),
(7, 'Income', 'Service Charge', NULL, '0000-00-00 00:00:00', 9000, NULL, '2018-01-19 10:02:25', '2018-01-19 10:02:25'),
(8, 'Income', 'Payment (Seth)', 'Payment From Merchant (Seth)', '2018-11-19 18:00:00', 500, 'asda', '2018-11-20 01:39:03', '2018-11-20 01:39:03'),
(9, 'Income', 'POS Payment(  )', NULL, '2018-11-27 10:21:09', 10, 'asda', '2018-11-27 10:21:09', '2018-11-27 10:21:09'),
(10, 'Income', 'POS Payment( Zal )', '<p>dsfsdfsdfs</p>', '2018-11-27 10:34:31', 20, 'asda', '2018-11-27 10:34:31', '2018-11-27 10:34:31'),
(11, 'Income', 'POS Payment( Prince John | 1726562944)', '<p>This is a test&nbsp;</p>', '2018-11-27 10:48:06', 5, 'asda', '2018-11-27 10:48:06', '2018-11-27 10:48:06'),
(12, 'Income', 'POS Payment ( Prince Pal-1726562944)', 'This is Testing', '2018-11-27 11:50:44', 1, 'asda', '2018-11-27 11:50:44', '2018-11-27 11:50:44'),
(13, 'Income', 'POS Payment ( Zal-1726562944)', 'tstese', '2018-11-29 04:16:21', 1000, 'asda', '2018-11-29 04:16:21', '2018-11-29 04:16:21'),
(14, 'Income', 'POS Payment ( Zal-1726562944)', 'test', '2018-11-29 04:20:12', 100000, 'asda', '2018-11-29 04:20:12', '2018-11-29 04:20:12'),
(15, 'Income', 'POS Payment ( Zal-1726562944)', 'tests', '2018-11-29 04:35:46', 1000, 'asda', '2018-11-29 04:35:46', '2018-11-29 04:35:46'),
(16, 'Income', 'POS Payment ( Zal-1726562944)', 'tests', '2018-11-29 04:38:08', 100000, 'asda', '2018-11-29 04:38:08', '2018-11-29 04:38:08'),
(17, 'Income', 'POS Payment ( Zal-1726562944)', 'test', '2018-11-29 04:55:13', 10, 'asda', '2018-11-29 04:55:13', '2018-11-29 04:55:13'),
(18, 'Income', 'POS Payment ( Zal-1726562944)', 'test', '2018-11-29 05:24:25', 10, 'asda', '2018-11-29 05:24:25', '2018-11-29 05:24:25'),
(19, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 06:41:24', 600, 'asda', '2018-11-29 06:41:24', '2018-11-29 06:41:24'),
(20, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 06:42:14', 300, 'asda', '2018-11-29 06:42:14', '2018-11-29 06:42:14'),
(21, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 06:47:17', 10, 'asda', '2018-11-29 06:47:17', '2018-11-29 06:47:17'),
(22, 'Income', 'POS Payment ( Zal-1726562944)', 'dfgdf', '2018-11-29 06:59:03', 10, 'asda', '2018-11-29 06:59:03', '2018-11-29 06:59:03'),
(23, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 07:02:19', 300, 'asda', '2018-11-29 07:02:19', '2018-11-29 07:02:19'),
(24, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 07:53:33', 300, 'asda', '2018-11-29 07:53:33', '2018-11-29 07:53:33'),
(25, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 07:55:42', 300, 'asda', '2018-11-29 07:55:42', '2018-11-29 07:55:42'),
(26, 'Income', 'POS Payment ( Zal-1726562944)', '', '2018-11-29 08:11:38', 10, 'asda', '2018-11-29 08:11:38', '2018-11-29 08:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE `bankaccounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `swift` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bankaccounts`
--

INSERT INTO `bankaccounts` (`id`, `name`, `account`, `branch`, `swift`, `address`, `created_at`, `updated_at`) VALUES
(4, 'IDBI Bank', '52261081280', 'Nashik', NULL, NULL, '2018-03-18 05:48:06', '2018-03-18 05:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `damages`
--

CREATE TABLE `damages` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `damages`
--

INSERT INTO `damages` (`id`, `product`, `date`, `description`, `category`, `merchant`, `supplier`, `unit`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, '3', '04-02-2018', '<p>Return From Shop</p>', 'Bamboo Basket', '1', '23', NULL, '10', '1800', '2018-02-03 15:04:03', '2018-02-03 15:31:36'),
(2, '3', '12-01-2018', NULL, 'Bamboo Basket', '2', '55', '45454', '88', '445454', '2018-03-16 05:59:43', '2018-03-16 05:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `saleid` int(191) DEFAULT NULL,
  `merchant` int(191) NOT NULL,
  `invoicedate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentdue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentaccount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` int(191) DEFAULT NULL,
  `other` int(191) DEFAULT NULL,
  `discount` int(191) DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `paymentstatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidamount` int(191) DEFAULT NULL,
  `paidmethod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidinfo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity1` int(191) NOT NULL,
  `quantity2` int(191) DEFAULT NULL,
  `quantity3` int(191) DEFAULT NULL,
  `quantity4` int(191) DEFAULT NULL,
  `quantity5` int(191) DEFAULT NULL,
  `quantity6` int(191) DEFAULT NULL,
  `quantity7` int(191) DEFAULT NULL,
  `quantity8` int(191) DEFAULT NULL,
  `quantity9` int(191) DEFAULT NULL,
  `quantity10` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `saleid`, `merchant`, `invoicedate`, `orderid`, `paymentdue`, `paymentmethod`, `paymentaccount`, `tax`, `other`, `discount`, `note`, `paymentstatus`, `paidamount`, `paidmethod`, `paidinfo`, `product1`, `product2`, `product3`, `product4`, `product5`, `product6`, `product7`, `product8`, `product9`, `product10`, `quantity1`, `quantity2`, `quantity3`, `quantity4`, `quantity5`, `quantity6`, `quantity7`, `quantity8`, `quantity9`, `quantity10`, `created_at`, `updated_at`, `customer`, `customer_phone`) VALUES
(19, 33, 1, '30-04-2018', '2018-04-30-58', '30-04-2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-29 14:02:51', '2018-04-30 11:21:58', NULL, NULL),
(20, NULL, 1, '12-04-2018', '515-445-4554', '12-04-2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-30 11:30:12', '2018-04-30 11:30:12', NULL, NULL),
(21, 34, 1, '27-09-2018', '2018-09-27-38', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-14 10:34:11', '2018-11-14 10:34:11', NULL, NULL),
(22, 35, 1, '14-11-2018', '2018-11-14-33', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-14 10:45:37', '2018-11-14 10:45:37', NULL, NULL),
(23, 36, 2, '16-11-2018', '2018-11-16-10', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', -5000, 'Cash', '000', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-16 01:34:03', '2018-11-17 03:45:56', NULL, NULL),
(24, 37, 1, '20-11-2018', '2018-11-20-89', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 2500, 'Cash', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-19 12:38:19', '2018-11-20 01:39:03', NULL, NULL),
(25, 39, 0, '2018-11-27 16:34:31', '2018-11-27-73', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 20, 'paypal', 'princejohn18-buyer@gmail.com (2YSWLX34QS722)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 10:34:31', '2018-11-27 10:34:31', NULL, NULL),
(26, 40, 0, '2018-11-27 16:48:06', '2018-11-27-77', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 5, 'paypal', 'princejohn18-buyer@gmail.com (2YSWLX34QS722)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 10:48:06', '2018-11-27 10:48:06', NULL, NULL),
(27, 41, 0, '2018-11-27 17:50:44', '2018-11-27-14', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 1, 'paypal', 'princejohn18-buyer@gmail.com (2YSWLX34QS722)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 11:50:44', '2018-11-27 11:50:44', NULL, NULL),
(28, 42, 0, '2018-11-29 10:16:21', '2018-11-29-38', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 1000, 'Stripe', 'ch_1DbmZFA4JNh6ZWXnE4cvqlRg (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 04:16:21', '2018-11-29 04:16:21', NULL, NULL),
(29, 43, 0, '2018-11-29 10:20:12', '2018-11-29-63', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 100000, 'Stripe', 'ch_1DbmczA4JNh6ZWXnpSYAbb2q (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 04:20:12', '2018-11-29 04:20:12', NULL, NULL),
(30, 44, 0, '2018-11-29 10:35:46', '2018-11-29-54', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 1000, 'Stripe', 'ch_1Dbms4A4JNh6ZWXnOXgmRkH1 (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 04:35:46', '2018-11-29 04:35:46', NULL, NULL),
(31, 45, 0, '2018-11-29 10:38:08', '2018-11-29-76', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 100000, 'Stripe', 'ch_1DbmuLA4JNh6ZWXnJGNDirIX (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 04:38:08', '2018-11-29 04:38:08', NULL, NULL),
(32, 46, 0, '2018-11-29 10:55:13', '2018-11-29-76', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 10, 'Stripe', 'ch_1DbnAsA4JNh6ZWXn32f2cSYu (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 04:55:13', '2018-11-29 04:55:13', NULL, NULL),
(33, 47, 0, '2018-11-29 11:24:25', '2018-11-29-76', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 10, 'Stripe', 'ch_1Dbnd9A4JNh6ZWXnfp6gOhOb (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 05:24:25', '2018-11-29 05:24:25', NULL, NULL),
(34, 48, 0, '2018-11-29 12:41:24', '2018-11-29-36', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 600, 'Cash', 'N/A', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 06:41:24', '2018-11-29 06:41:24', NULL, NULL),
(35, 49, 0, '2018-11-29 12:42:14', '2018-11-29-15', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 300, 'Cash', 'N/A', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 06:42:14', '2018-11-29 06:42:14', NULL, NULL),
(36, 50, 0, '2018-11-29 12:47:17', '2018-11-29-18', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 10, 'Cash', 'N/A', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 06:47:17', '2018-11-29 06:47:17', NULL, NULL),
(37, 51, 0, '2018-11-29 12:59:03', '2018-11-29-26', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 10, 'Cash', 'N/A', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 06:59:03', '2018-11-29 06:59:03', NULL, NULL),
(38, 52, 0, '2018-11-29 13:02:19', '2018-11-29-29', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 300, 'Stripe', 'ch_1Dbp9sA4JNh6ZWXnhPhxHeYc (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 07:02:19', '2018-11-29 07:02:19', NULL, NULL),
(39, 53, 0, '2018-11-29 13:53:33', '2018-11-29-61', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 300, 'Stripe', 'ch_1DbpxQA4JNh6ZWXn1bYGdaWI (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 07:53:33', '2018-11-29 07:53:33', 'Zal', '1726562944'),
(40, 54, 0, '2018-11-29 13:55:42', '2018-11-29-50', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 300, 'Stripe', 'ch_1DbpzVA4JNh6ZWXnHkIepZ8U (4242)', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 07:55:42', '2018-11-29 07:55:42', 'Zal', '1726562944'),
(41, 55, 0, '2018-11-29 14:11:38', '2018-11-29-44', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Paid', 10, 'Cash', 'N/A', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-29 08:11:38', '2018-11-29 08:11:38', 'Zal', '1726562944');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":9:{s:7:\\\"toEmail\\\";s:22:\\\"princepeal25@gmail.com\\\";s:4:\\\"data\\\";a:2:{s:5:\\\"email\\\";s:22:\\\"princepeal25@gmail.com\\\";s:11:\\\"messagetext\\\";s:16:\\\"<p>edfsdfsdf<\\/p>\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";i:15;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1523346404, 1523346389);

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE `merchants` (
  `id` int(10) UNSIGNED NOT NULL,
  `merchantid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tradelicense` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otherlicense` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `youtube` text COLLATE utf8mb4_unicode_ci,
  `map` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `merchantid`, `photo`, `fname`, `lname`, `phone`, `fax`, `email`, `company`, `tradelicense`, `otherlicense`, `address`, `about`, `facebook`, `twitter`, `youtube`, `map`, `created_at`, `updated_at`) VALUES
(1, 'Mer1001', '1516033854.jpeg', 'Seth', 'D. Stahl', '651-662-8077', NULL, 'SethDStahl@dayrep.com', 'Apple Inc.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-15 10:30:55', '2018-02-28 11:51:39'),
(2, 'Mer1002', '1519840711.jpeg', 'Yolanda', 'S. Hill', '240-212-6997', NULL, 'YolandaSHill@dayrep.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.youtube.com/', NULL, '2018-02-28 11:57:33', '2018-02-28 12:01:43'),
(4, '1000100', NULL, 'John', 'Smith', '000-000-0000', NULL, 'johnsmith@johnsmithsemail.com', 'The ABC Company', NULL, NULL, '<p>12345 Main Street</p>', NULL, NULL, NULL, NULL, NULL, '2018-04-07 01:20:15', '2018-04-07 01:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_12_15_001348_create_suppliers_table', 1),
(13, '2017_12_16_121756_add_photo_columns_to_suppliers_table', 2),
(16, '2018_01_02_105358_create_merchants_table', 3),
(19, '2018_01_07_180011_create_pcategories_table', 5),
(22, '2018_01_03_170801_create_products_table', 7),
(23, '2018_01_09_120420_create_purchases_table', 8),
(25, '2018_01_15_170516_create_sales_table', 9),
(29, '2018_01_16_131016_create_balances_table', 10),
(32, '2018_01_17_165236_create_staff_table', 11),
(34, '2018_01_22_124341_create_invoices_table', 12),
(35, '2017_12_14_150553_create_settings_table', 13),
(37, '2018_01_29_120012_create_assets_table', 14),
(38, '2018_01_30_081159_create_stores_table', 15),
(39, '2018_01_30_123256_create_bankaccounts_table', 16),
(40, '2018_01_30_145657_create_transactions_table', 17),
(45, '2018_02_03_205356_create_damages_table', 19),
(46, '2014_10_12_000000_create_users_table', 20),
(47, '2018_02_06_053709_create_smsemails_table', 21),
(48, '2018_02_16_164320_create_jobs_table', 21),
(49, '2018_03_01_051331_create_payments_table', 22),
(50, '2018_03_07_072404_create_failed_jobs_table', 23),
(51, '2018_11_29_133817_add_customer_info_to_invoice', 24);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('productiondjovany@gmail.com', '$2y$10$q09yVYCrjgARipLfQcFeeOOhjJhRJU.lfBhQ/WqaYRVXw3qxYCEkO', '2018-03-16 16:30:51');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoiceid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payeremail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payerfname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payerlname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payerid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `invoiceid`, `paymentid`, `paymentmethod`, `payeremail`, `payerfname`, `payerlname`, `payerid`, `amount`, `currency`, `created_at`, `updated_at`) VALUES
(1, '4', 'PAY-5RF88156B8765045PLKLZKRI', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '25.19', 'USD', '2018-02-28 23:53:41', '2018-02-28 23:53:41'),
(2, '4', 'PAY-71T0965313120472JLKLZSEI', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '25.19', 'USD', '2018-03-01 00:10:00', '2018-03-01 00:10:00'),
(3, '0', 'PAY-96G30927BG9131015LP6W4AA', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '10.00', 'USD', '2018-11-27 10:19:12', '2018-11-27 10:19:12'),
(4, '0', 'PAY-57524449C94765134LP6W5TA', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '10.00', 'USD', '2018-11-27 10:21:09', '2018-11-27 10:21:09'),
(5, '0', 'PAY-6PN563475L8152217LP6XD2I', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '20.00', 'USD', '2018-11-27 10:34:31', '2018-11-27 10:34:31'),
(6, '26', 'PAY-62352929GP4585458LP6XKFQ', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '5.00', 'USD', '2018-11-27 10:48:06', '2018-11-27 10:48:06'),
(7, '27', 'PAY-4AT141611D3165725LP6YHKI', 'paypal', 'princejohn18-buyer@gmail.com', 'test', 'buyer', '2YSWLX34QS722', '1.00', 'USD', '2018-11-27 11:50:44', '2018-11-27 11:50:44'),
(8, '28', 'txn_1DbmZGA4JNh6ZWXn6tgOgMcC', 'Stripe', 'ch_1DbmZFA4JNh6ZWXnE4cvqlRg', 'ch_1DbmZFA4JNh6ZWXnE4cvqlRg', 'ch_1DbmZFA4JNh6ZWXnE4cvqlRg', 'card_1DbmZDA4JNh6ZWXnBnleY5AB', '1000', 'usd', '2018-11-29 04:16:21', '2018-11-29 04:16:21'),
(9, '29', 'txn_1Dbmd0A4JNh6ZWXnhJTc9uJU', 'Stripe', 'ch_1DbmczA4JNh6ZWXnpSYAbb2q', 'ch_1DbmczA4JNh6ZWXnpSYAbb2q', 'ch_1DbmczA4JNh6ZWXnpSYAbb2q', 'card_1DbmcMA4JNh6ZWXnydkKJe8u', '100000', 'usd', '2018-11-29 04:20:12', '2018-11-29 04:20:12'),
(10, '30', 'txn_1Dbms4A4JNh6ZWXnTBDrnGzO', 'Stripe', 'ch_1Dbms4A4JNh6ZWXnOXgmRkH1', 'ch_1Dbms4A4JNh6ZWXnOXgmRkH1', 'ch_1Dbms4A4JNh6ZWXnOXgmRkH1', 'card_1DbmqUA4JNh6ZWXnpzXWSQi1', '1000', 'usd', '2018-11-29 04:35:46', '2018-11-29 04:35:46'),
(11, '31', 'txn_1DbmuLA4JNh6ZWXnXrtHrfTa', 'Stripe', 'ch_1DbmuLA4JNh6ZWXnJGNDirIX', 'ch_1DbmuLA4JNh6ZWXnJGNDirIX', 'ch_1DbmuLA4JNh6ZWXnJGNDirIX', 'card_1DbmuJA4JNh6ZWXnQCLcx2C7', '100000', 'usd', '2018-11-29 04:38:08', '2018-11-29 04:38:08'),
(12, '32', 'txn_1DbnAsA4JNh6ZWXnFLiLw097', 'Stripe', 'ch_1DbnAsA4JNh6ZWXn32f2cSYu', 'ch_1DbnAsA4JNh6ZWXn32f2cSYu', 'ch_1DbnAsA4JNh6ZWXn32f2cSYu', 'card_1DbnArA4JNh6ZWXnIZI6JZef', '10', 'usd', '2018-11-29 04:55:13', '2018-11-29 04:55:13'),
(13, '33', 'txn_1Dbnd9A4JNh6ZWXnaFd1BknK', 'Stripe', 'ch_1Dbnd9A4JNh6ZWXnfp6gOhOb', 'ch_1Dbnd9A4JNh6ZWXnfp6gOhOb', 'ch_1Dbnd9A4JNh6ZWXnfp6gOhOb', 'card_1Dbnd7A4JNh6ZWXnrVMZb0TT', '10', 'usd', '2018-11-29 05:24:25', '2018-11-29 05:24:25'),
(14, '34', 'N/A', 'Cash', 'N/A', 'Zal', 'N/A', 'N/A', '600', 'USD', '2018-11-29 06:41:24', '2018-11-29 06:41:24'),
(15, '35', 'N/A', 'Cash', 'N/A', 'Zal', 'N/A', 'N/A', '300', 'USD', '2018-11-29 06:42:14', '2018-11-29 06:42:14'),
(16, '36', 'N/A', 'Cash', 'N/A', 'Zal', 'N/A', 'N/A', '10', 'USD', '2018-11-29 06:47:17', '2018-11-29 06:47:17'),
(17, '37', 'N/A', 'Cash', 'N/A', 'Zal', 'N/A', 'N/A', '10', 'USD', '2018-11-29 06:59:03', '2018-11-29 06:59:03'),
(18, '38', 'txn_1Dbp9tA4JNh6ZWXnchC5k6WL', 'Stripe', 'ch_1Dbp9sA4JNh6ZWXnhPhxHeYc', 'ch_1Dbp9sA4JNh6ZWXnhPhxHeYc', 'ch_1Dbp9sA4JNh6ZWXnhPhxHeYc', 'card_1Dbp9rA4JNh6ZWXnnE3NeIVO', '300', 'usd', '2018-11-29 07:02:19', '2018-11-29 07:02:19'),
(19, '39', 'txn_1DbpxQA4JNh6ZWXnsMiVtN9d', 'Stripe', 'ch_1DbpxQA4JNh6ZWXn1bYGdaWI', 'ch_1DbpxQA4JNh6ZWXn1bYGdaWI', 'ch_1DbpxQA4JNh6ZWXn1bYGdaWI', 'card_1DbpxPA4JNh6ZWXnpiVJMvdX', '300', 'usd', '2018-11-29 07:53:33', '2018-11-29 07:53:33'),
(20, '40', 'txn_1DbpzWA4JNh6ZWXn4GGpECJr', 'Stripe', 'ch_1DbpzVA4JNh6ZWXnHkIepZ8U', 'ch_1DbpzVA4JNh6ZWXnHkIepZ8U', 'ch_1DbpzVA4JNh6ZWXnHkIepZ8U', 'card_1DbpzUA4JNh6ZWXnkeQHYvY8', '300', 'usd', '2018-11-29 07:55:42', '2018-11-29 07:55:42'),
(21, '41', 'N/A', 'Cash', 'N/A', 'Zal', 'N/A', 'N/A', '10', 'USD', '2018-11-29 08:11:38', '2018-11-29 08:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `pcategories`
--

CREATE TABLE `pcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pcategories`
--

INSERT INTO `pcategories` (`id`, `category`, `created_at`, `updated_at`) VALUES
(17, 'Carbon Body', NULL, NULL),
(18, 'Iron Case', NULL, NULL),
(20, 'Bamboo Basket', NULL, NULL),
(21, 'Jute', NULL, NULL),
(22, 'Fiber', NULL, NULL),
(23, 'Metal Body', NULL, NULL),
(24, 'Metal Body', '2018-01-10 23:56:44', '2018-01-10 23:56:44'),
(27, 'Service', '2018-04-07 01:22:20', '2018-04-07 01:22:20'),
(28, 'Ram', '2018-04-09 23:32:58', '2018-04-09 23:32:58');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitquantity` int(191) DEFAULT NULL,
  `materials` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `design` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacturedate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiredate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(191) NOT NULL,
  `profit` int(191) DEFAULT NULL,
  `costtitle1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costtitle2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costtitle3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costtitle4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costtitle5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost1` int(191) DEFAULT NULL,
  `cost2` int(191) DEFAULT NULL,
  `cost3` int(191) DEFAULT NULL,
  `cost4` int(191) DEFAULT NULL,
  `cost5` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `photo`, `title`, `description`, `code`, `category`, `supplier`, `model`, `unit`, `unitquantity`, `materials`, `weight`, `dimension`, `color`, `design`, `capacity`, `quality`, `origin`, `warehouse`, `manufacturedate`, `expiredate`, `link`, `price`, `profit`, `costtitle1`, `costtitle2`, `costtitle3`, `costtitle4`, `costtitle5`, `cost1`, `cost2`, `cost3`, `cost4`, `cost5`, `created_at`, `updated_at`) VALUES
(3, '1519842050.jpeg', 'Street Phone', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '0114-574-9751', 'Carbon Body', '55', 'BB1502', 'Unit', 1000, 'Metal', '5 KG', 'Height 30\",  Width 30\"', 'Bule', 'Modern design', '10 KG', 'Export', 'Australia', 'London', '04-04-2018', '15-08-2018', '#', 300, 50, 'Sample Cost 1', 'Sample Cost 2', 'Sample Cost 3', 'Sample Cost 4', 'Sample Cost 5', 20, 25, 10, 0, 0, '2018-02-28 12:20:50', '2018-04-29 11:22:49'),
(4, '1519842432.jpg', 'Desktop Telephone', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'CFB16', 'Carbon Body', '56', 'BB1502', 'Unit', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 300, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:27:12', '2018-04-29 11:25:45'),
(5, '1519842480.jpeg', 'Iphone', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Iphone X', 'Carbon Body', '55', 'Iphone X', 'Unit', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 999, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:28:00', '2018-04-29 11:28:22'),
(6, '1519842050.jpeg', 'Street Phone', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '0114-574-9751', 'Carbon Body', '55', 'BB1502', 'Unit', 1000, 'Metal', '5 KG', 'Height 30\",  Width 30\"', 'Bule', 'Modern design', '10 KG', 'Export', 'Australia', 'London', '04-04-2018', '15-08-2018', '#', 300, 50, 'Sample Cost 1', 'Sample Cost 2', 'Sample Cost 3', 'Sample Cost 4', 'Sample Cost 5', 20, 25, 10, 0, 0, '2018-02-28 12:20:50', '2018-04-29 11:22:49'),
(7, '1519842432.jpg', 'Desktop Telephone', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'CFB16', 'Carbon Body', '56', 'BB1502', 'Unit', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 300, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:27:12', '2018-04-29 11:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `purchasedate` timestamp NULL DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(191) DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(191) NOT NULL,
  `commission` int(191) DEFAULT NULL,
  `total` int(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `title`, `photo`, `description`, `purchasedate`, `category`, `supplier`, `unit`, `quantity`, `company`, `price`, `commission`, `total`, `created_at`, `updated_at`) VALUES
(4, 'desktop', '1519842812.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-04-07 18:00:00', 'Iron Case', '56', 'Set', 6, 'Microsoft', 1000, 15, 5985, '2018-02-28 12:33:32', '2018-04-06 08:58:15'),
(5, 'HTC', '1519842863.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-04-09 18:00:00', 'Carbon Body', '56', 'Unit', 3, 'HTC', 750, NULL, 2250, '2018-02-28 12:34:23', '2018-04-05 00:39:18'),
(6, 'Desktop Telephone', '1519842939.jpg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-04-14 18:00:00', 'Carbon Body', '55', 'Unit', 4, 'Motorola', 950, NULL, 3800, '2018-02-28 12:35:39', '2018-04-29 12:37:42'),
(12, 'Laptop', NULL, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-04-29 12:02:21', 'Carbon Body', '55', 'Unit', 100, NULL, 150, NULL, 15000, '2018-04-29 12:02:21', '2018-04-29 12:02:21');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `saledate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission` int(191) DEFAULT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `product`, `description`, `saledate`, `category`, `merchant`, `unit`, `quantity`, `company`, `price`, `commission`, `total`, `created_at`, `updated_at`) VALUES
(35, '3,5,4,5', '<p>test</p>', '2018-11-14 00:00:00', '', '1', '0', '1,2,10,4', NULL, '1,650,350,500', 0, '0', '2018-11-14 10:45:37', '2018-11-14 10:45:37'),
(37, '3,4,5,5,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-11-20 00:00:00', '', '1', '0', '5,5,3,5,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', NULL, '50,100,300,200,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 50, '0', '2018-11-19 12:38:19', '2018-11-19 13:27:15'),
(38, '3,', NULL, '2018-11-27 16:21:09', '', '0', '0', '1,', NULL, '300', 290, '0', '2018-11-27 10:21:09', '2018-11-27 10:21:09'),
(39, '3,10,', '<p>dsfsdfsdfs</p>', '2018-11-27 16:34:31', '', '0', '0', '1,1,', NULL, '300,300,', 580, '0', '2018-11-27 10:34:31', '2018-11-27 10:34:31'),
(40, '3,10,', '<p>This is a test&nbsp;</p>', '2018-11-27 16:48:06', '', '0', '0', '1,1,', NULL, '300,300,', 595, '0', '2018-11-27 10:48:06', '2018-11-27 10:48:06'),
(41, '9,', 'This is Testing', '2018-11-27 17:50:44', '', '0', '0', '1,', NULL, '300,', 299, '0', '2018-11-27 11:50:44', '2018-11-27 11:50:44'),
(42, '3,4,', 'tstese', '2018-11-29 10:16:21', '', '0', '0', '2,2,', NULL, '300,300,', 1190, '0', '2018-11-29 04:16:21', '2018-11-29 04:16:21'),
(43, '3,', 'test', '2018-11-29 10:20:12', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 04:20:12', '2018-11-29 04:20:12'),
(44, '3,4,', 'tests', '2018-11-29 10:35:46', '', '0', '0', '1,1,', NULL, '300,300,', 590, '0', '2018-11-29 04:35:46', '2018-11-29 04:35:46'),
(45, '3,', 'tests', '2018-11-29 10:38:08', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 04:38:08', '2018-11-29 04:38:08'),
(46, '3,', 'test', '2018-11-29 10:55:13', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 04:55:13', '2018-11-29 04:55:13'),
(47, '3,', 'test', '2018-11-29 11:24:25', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 05:24:25', '2018-11-29 05:24:25'),
(48, '3,4,', '', '2018-11-29 12:41:24', '', '0', '0', '1,1,', NULL, '300,300,', 0, '0', '2018-11-29 06:41:24', '2018-11-29 06:41:24'),
(49, '3,', '', '2018-11-29 12:42:14', '', '0', '0', '1,', NULL, '300,', 0, '0', '2018-11-29 06:42:14', '2018-11-29 06:42:14'),
(50, '3,', '', '2018-11-29 12:47:17', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 06:47:17', '2018-11-29 06:47:17'),
(51, '3,', 'dfgdf', '2018-11-29 12:59:03', '', '0', '0', '1,', NULL, '300,', 290, '0', '2018-11-29 06:59:03', '2018-11-29 06:59:03'),
(52, '3,', '', '2018-11-29 13:02:19', '', '0', '0', '1,', NULL, '300,', 0, '0', '2018-11-29 07:02:19', '2018-11-29 07:02:19'),
(53, '3,', '', '2018-11-29 13:53:33', '', '0', '0', '1,', NULL, '300,', 0, '0', '2018-11-29 07:53:33', '2018-11-29 07:53:33'),
(54, '3,', '', '2018-11-29 13:55:42', '', '0', '0', '1,', NULL, '300,', 0, '0', '2018-11-29 07:55:42', '2018-11-29 07:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchasecode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `photo`, `title`, `tag`, `currency`, `email`, `phone`, `address`, `city`, `country`, `postal`, `license1`, `license2`, `license3`, `purchasecode`, `created_at`, `updated_at`) VALUES
(1, '1543585701.png', 'Abacus', 'Manufacture & Sell CRM with POS', 'USD', 'support@onezeroart.com', '0000', 'Mexico', 'Nuevo Laredo', 'MX', '123456', NULL, NULL, NULL, '', '2018-01-23 04:05:37', '2018-11-30 08:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `smsemails`
--

CREATE TABLE `smsemails` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `photo`, `fname`, `lname`, `phone`, `position`, `dob`, `gender`, `education`, `height`, `weight`, `nationality`, `blood`, `religion`, `marital`, `nid`, `address`, `created_at`, `updated_at`) VALUES
(1, '1519843641.jpeg', 'Kathy', 'L. Spencer', '323-440-0993', 'Manager', '08-02-1988', 'Female', 'Hons.', '6 Feet', '85 KG', 'USA', 'AB+', 'Christian', 'Single', '1546761327546874354', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-01-17 13:43:47', '2018-02-28 12:47:21'),
(2, NULL, 'Worker', 'Bob', '000-000-0000', 'Worker', '01-04-1999', 'm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-07 01:18:08', '2018-04-07 01:18:08');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `managerphone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `photo`, `name`, `slogan`, `phone`, `time`, `location`, `address`, `manager`, `managerphone`, `staff`, `website`, `facebook`, `twitter`, `youtube`, `map`, `description`, `created_at`, `updated_at`) VALUES
(1, '1517308887.jpg', 'Cane Fruit Craft', 'Awesome Craft For Your Home', '1726562944', '8:00 AM to 5:00 PM', 'Londn', 'London', '1', '1726562944', '15', 'onezeroart.com', 'https://www.facebook.com/onezeroart/', NULL, NULL, NULL, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>', '2018-01-30 04:41:27', '2018-01-30 05:33:48');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplierid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `youtube` text COLLATE utf8mb4_unicode_ci,
  `map` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `photo`, `supplierid`, `fname`, `lname`, `phone`, `fax`, `email`, `company`, `cheque`, `bank`, `account`, `paypal`, `other`, `address`, `about`, `facebook`, `twitter`, `youtube`, `map`, `created_at`, `updated_at`) VALUES
(55, '1522920926.jpg', 'SPL5421', 'Larry', 'J. Rapp', '443-331-0157', '27414243', 'LarryJRapp@armyspy.com', 'One Zero Art', 'Larry', 'Larry J. Rapp', '1502', 'EleanorJDiggs@dayrep.com', 'Strip', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-05 08:24:41'),
(56, '1522923635.jpg', 'SPL5422', 'Lucius', 'R. Stringer', '267-345-0922', NULL, 'LuciusRStringer@rhyta.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 11:41:57', '2018-04-05 04:20:35');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(191) NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `date`, `name`, `type`, `amount`, `note`, `created_at`, `updated_at`) VALUES
(10, '07-03-2018', '4', 'Increase', 10000, 'Sale', '2018-03-21 05:30:52', '2018-03-21 05:30:52'),
(11, '29-03-2018', '4', 'Increase', 25000, 'Consulting Fees', '2018-03-28 14:22:06', '2018-03-28 14:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `photo`, `name`, `email`, `password`, `phone`, `dob`, `gender`, `supplier`, `merchant`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, NULL, 'pretty', 'tajin@one2mail.info', '$2y$10$TUbeoq0kPE1.ItzIvJmYW.q8skG95PhuqByib5aFndLv2SnVt400K', '86756453423', '22-03-2018', 'male', '55', '1', 'supplier', 'oRwMRpzcTSwoubP5tb1STPYP4BrQkycCJHfjRSei0O4tYIdjpI6qxR896bHw', '2018-03-16 02:46:59', '2018-04-05 09:08:07'),
(4, NULL, 'Ahmad', 'ahmad@example.com', '$2y$10$.6wpNGvMyd0rGAC60oE1EO3EfJTJ.HcWNB.d5pIdaRI8EZuwOe60G', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-16 03:00:16', '2018-03-16 03:00:16'),
(6, NULL, 'Diovany Deus', 'productiondjovany@gmail.com', '$2y$10$ENdjsJlbirOakqGH/qJ5fO1lxNUE9KZd1blPblSJ.vn1LLU83qVBy', NULL, NULL, NULL, NULL, NULL, 'user', 'AqkgUsMrBk6xixlzOIpU6JaUwvIQ4gRHIGpae9ikAqYUVQLc2AfJ9vPlSczn', '2018-03-16 12:46:16', '2018-03-16 12:46:16'),
(7, NULL, 'rr', 'demo@gmail.com', '$2y$10$mmlxC..MfJHWgBFOVFs0zuMrFchDcwmet5f3d6cEx/xVajm/eX5v.', NULL, NULL, NULL, NULL, NULL, 'user', 'MoNlbz9DA8FkH0hOx1N7PtvQR7FdvTBKGFe7qWyDUKctHz1CRo7kqz64pXpz', '2018-03-16 23:35:47', '2018-03-18 17:58:32'),
(8, NULL, 'Avishek Mondal', 'avishek.development@gmail.com', '$2y$10$1tJ9DLx/KV1yowCH3eqUwecNltjkhROp8P6DOYPimKu9JH6monNRC', '8013197028', '02-04-1995', 'Male', NULL, NULL, 'supplier', 'nbEwslYVqC57te06ia6JiK2McRk6O1hZSvBSeGzqXmajV26inno4KpEf7fwI', '2018-03-17 07:36:41', '2018-03-17 07:36:41'),
(9, NULL, 'Avishek Mondal', 'avishek1.development@gmail.com', '$2y$10$QumAYUBZV0fVMV36WTeIIO87Vy0q6/4xdbD2ubLIxFN8cxVspkZ.C', '8013197028', '28-03-2018', 'Male', NULL, NULL, 'staff', NULL, '2018-03-17 07:39:31', '2018-03-17 07:39:31'),
(10, NULL, 'Saiful', 'msobuj097@gmail.com', '$2y$10$8S2E2/UdDVwCH4JfpEfHhe.3zJa266CgPLfluqX08s9ndyncMkIxa', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-17 10:41:28', '2018-03-17 10:41:28'),
(11, NULL, 'nakryt', 'nakrytik@mail.ru', '$2y$10$k8emqL7D1ZX/2KM3/J6x6eSrxFdujgutaKFmpuq23VtpFdYGULLMW', NULL, NULL, NULL, NULL, NULL, 'user', 'GxCfapuETdqdr24IxrApVf1fUHvU0ALnVNww6bHdyQavNnCNGUN6iuCO9GHD', '2018-03-18 11:32:40', '2018-03-18 11:32:40'),
(12, NULL, 'coral', 'coralpacs@gmail.com', '$2y$10$ZUwkmh9kgJCvKAEyy5SL/.nytr4LInrG7iCJ1vuTmE2qUT6tqZ9d.', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-19 03:10:59', '2018-03-19 03:10:59'),
(13, NULL, 'Faisal Ibrahim', 'faisal.aan048@gmail.com', '$2y$10$YA/a5ojLWteH2KMR7ROAT.xXMrA0ZuC/Btq90svV7XWiq6Nk/kLa2', NULL, NULL, NULL, NULL, NULL, 'user', 'KdnYbwUrWj7oDGSrQwi1wnuixBJkvrN5IqUGaHDjWrOynTolo7pExNxpv8g6', '2018-03-20 01:35:04', '2018-03-20 01:35:04'),
(15, NULL, 'mike', 'test@test.com', '$2y$10$TCWKQJflzOMF96419To0DeoXUh01yUIZEBSYD3q3A976v5.INsS22', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-22 08:09:54', '2018-03-22 08:09:54'),
(16, NULL, 'Richard Dunne', 'richard.dunne21@gmail.com', '$2y$10$vFlcZGvckTjYYsqkxD17EOhgw/i5X0LtQHJWfuoiYl6YjR4S9Ok0S', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-25 01:28:06', '2018-03-25 01:28:06'),
(17, NULL, 'Demo User', 'demo@site.com', '$2y$10$vlKuO5culsxA85P5e4hnI.Or.CfLTiuRsppQkm8P477RCNDTLE4BG', '123456', '01-03-2018', 'Male', NULL, NULL, 'staff', 'TnqbBwQwUWvpzmvrqPo714XoFSJhjlGJ6BT6kO1weVVVDvcYJwUQlZCZgXWM', '2018-03-27 10:05:34', '2018-03-27 10:05:34'),
(18, NULL, 'niks', 'niks@mailinator.com', '$2y$10$Xzre75Fn/YKJuj6wOD1Ttuur09cJLio9.o983pt7ngdX9ts4oqqni', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-27 23:28:30', '2018-03-27 23:28:30'),
(19, NULL, 'kajze', 'g4fsee@gmail.com', '$2y$10$H30r7RebMIL9p8/zinxLruxxRX1ZIx9m.ecaBIV5pdtvSr3jHpVdS', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-03-30 17:24:05', '2018-03-30 17:24:05'),
(20, NULL, 'MarkSilver', 'marksilverceo2018@gmail.com', '$2y$10$bIpx0p9.jPZGxMSOEfrLoOkAhsJx3fA79l22nDollY6xs8KgV0pPa', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-04-03 13:47:20', '2018-04-03 13:47:20'),
(24, NULL, 'প্রিন্স পিয়াল', 'princepeal@yahoo.com', '$2y$10$HtP0iikHuWcmxr/VLR4SLOd0QlGpTtraNkJFnP9tRoG4z3t7G20Um', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', NULL, '2018-04-07 07:47:11', '2018-04-07 07:47:11'),
(25, NULL, 'Eugen Ionda', 'eugen.ionda@gmail.com', '$2y$10$jjveH9LSYot4rghBPjbAveIDPxoFAIIzbowolT8RKs8Eu/bnYlOvO', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-04-11 16:04:56', '2018-04-11 16:04:56'),
(26, NULL, 'Bilal Alani', 'nwa3ir@yahoo.com', '$2y$10$MQXQg/Se.tngkHCgSKcz1OrgWYcTBkIiO4ntuD58yW4gtK6l8aEKG', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', NULL, '2018-04-13 20:09:41', '2018-04-13 20:09:41'),
(27, NULL, 'Hamka Mannan', 'hamka.window@gmail.com', '$2y$10$nf1ClwqE4uGYyGtn0cUw8uE24didfYPaftEl4Z5EosDHOiodmfdyO', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', NULL, '2018-04-14 00:55:45', '2018-04-14 00:55:45'),
(28, NULL, 'PKL smkkencana', 'pklsmkkencana@gmail.com', '$2y$10$nY7cvKUU2VcACxT3eXFVuOXjI3bQ.8fHB3yP4yiXps0Yuy6hmWMRS', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', NULL, '2018-04-14 01:34:38', '2018-04-14 01:34:38'),
(29, NULL, 'Pavel Tarasik', 'tpavell2012@gmail.com', '$2y$10$HJWCoLZFCybODjp7AwWOC.CXathCneLIfZ/OqbJ5TDNkgI2lB10VC', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', NULL, '2018-04-14 06:30:27', '2018-04-14 06:31:52'),
(30, NULL, 'rodrigo', 'rbarbosaribeiro@gmail.com', '$2y$10$T6OF3sG0w0SnCCYukHoQ0ezTp9yC2zi6qyY/rveHzUCTpJLZJruqq', NULL, NULL, NULL, NULL, NULL, 'user', '7fUM4fFp2BEoXejlsEQS1TYokY4gmiK3Db94CYD0bTA0w0jvbd80nDst1JBp', '2018-04-14 07:21:38', '2018-04-14 07:21:38'),
(31, NULL, 'asda', 'admin@site.com', '$2y$10$F1/lb.Slxn.6rIb3dcWD4.rD9GlLh05S0BTOU2VbQ9EA98bm.q1fu', '123546', '19-04-2018', '.', '55', '2', 'admin', 'lH5Pd3ykbT9VrP714hr39vZi7GUBPKGBH3y8rJFfGNYkgRtd2VgR4JGuMvYg', '2018-04-14 10:22:33', '2018-04-23 14:53:42'),
(32, NULL, 'asd', 'asd@asd.com', '$2y$10$9J2bcpQpKpZqa1wRmT3ufOFFlHfVEB6v0SmUwIAfQaRvXEjUtDg/G', NULL, NULL, NULL, NULL, NULL, 'user', 'LJiwuyF7L0OR5b0Lo8T3y08m68dtQ5TOYfiXXIwsqFSheYZNoSIQ7dzHluqu', '2018-04-17 15:51:30', '2018-04-17 15:51:30'),
(33, NULL, 'Binoj', 'binoj@brandmoustache.com', '$2y$10$iFg6SvW55wpT.dVK2beWGOvphS9y5RbPkAVvp5qDkqPUZgXElLMAu', NULL, NULL, NULL, NULL, NULL, 'user', 'CgCBhaRw9zVOqcvjPcMqcyg2QFJ8T0g3fkC3fyQ5bu7Z03AEE5qWvImSfOy7', '2018-04-18 22:41:16', '2018-04-18 22:41:16'),
(34, NULL, 'alguno', 'user@sitesite.com', '$2y$10$TEOLHRdZ7v2MRs4X2r5EfewSIBmc3q1ZoLZlechCAsMWwqmdbVCeS', NULL, NULL, NULL, NULL, NULL, 'user', 'ljsWdzFzHrgcdQumKUVHTitqW55zMgGQFFC7sbROq1dO8pDC0khHapyyH1qp', '2018-04-18 23:33:14', '2018-04-18 23:33:14'),
(35, NULL, 'Omri Amos', 'omriamos@gmail.com', '$2y$10$qc.3suFgYX5It9Xy33jBNe5B.XhWtjoG4.G3skH7cWIL8K0T9LQKC', NULL, NULL, NULL, NULL, NULL, 'merchant-supplier', 'lRvA7dM9zb1YltmubkvjDyqLSPsSdNHYSGN7ZckiDYl5BGE3MskKy9yYQSWz', '2018-04-22 02:39:06', '2018-04-22 02:39:06'),
(36, NULL, 'teste', 'teste@teste.com', '$2y$10$Rv7nUj6Is28d2ovrprVLVeEYZXtoW7pBIwWcMyqs6/13MMZGpWo8O', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2018-04-26 11:46:13', '2018-04-26 11:46:13'),
(37, NULL, 'User', 'user@site.com', '$2y$10$kLQIrC./U57VYplgCYmB7.fHKeFCLcJRxnhEz9cNB/wdZdqwMotZe', NULL, NULL, NULL, NULL, NULL, 'user', '39iaJnr6Gi1k5FZt5abR7OpHbCanU475wSMaXGMo3YfnBVlfVxY8PNle2Z9I', '2018-04-26 13:17:02', '2018-04-26 13:17:02'),
(38, NULL, 'Luis Hernandez', 'luis.hernandez028@hotmail.com', '$2y$10$ovVE7vIWKJSx3jMkwk3ziuyTwY21FC186Tu8qEvY2062rf/BDmJgi', '8671613883', '28-02-2018', 'Male', NULL, NULL, 'admin', NULL, '2018-04-28 19:39:20', '2018-04-28 19:39:20'),
(39, NULL, 'sdfsdf', 'admisdfsdfsfn@site.com', '$2y$10$Bg5ZzEsaBya2wfiT1m4kQe.DdXGLmwGfj3/8NkFfHnS7.BZv2YxUe', '1726562944', '01-05-2018', 'sdf', '55', NULL, 'supplier', NULL, '2018-04-30 12:15:05', '2018-04-30 12:15:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balances`
--
ALTER TABLE `balances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `damages`
--
ALTER TABLE `damages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pcategories`
--
ALTER TABLE `pcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smsemails`
--
ALTER TABLE `smsemails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `balances`
--
ALTER TABLE `balances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `damages`
--
ALTER TABLE `damages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pcategories`
--
ALTER TABLE `pcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `smsemails`
--
ALTER TABLE `smsemails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
