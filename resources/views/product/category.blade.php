@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  دسته بندی جدید</h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['action' => 'ProductController@storeCategory', 'files' => true, 'method' => 'POST']) !!}

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group">
                                {{ Form::label('category', 'نام')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::text('category', '', ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                                </div>
                            </div>
                        </div>
                    </div>


                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه دسته بندی ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtGlobal text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>دسته بندی</th>
                                <th style="width: 20%">عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($categories) > 0 )

                            @foreach($categories as $category)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $category->category }}</td>
                                    <td>
                                        <form action="{{ route('product.categoryDestroy', $category->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>


    </div>
</section>


@endsection
