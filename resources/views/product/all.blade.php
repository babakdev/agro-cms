@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">محصولات</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtProduct text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام</th>
                                <th>دسته بندی</th>
                                <th>تامین کننده</th>
                                <th>میزان فروش رفته</th>
                                <th>موجودی</th>
                                <th>قیمت</th>
                                <th>هزینه و سود</th>
                                <th>قیمت خالص</th>
                                <th style = "width:15%">عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($products) > 0 )

                            @foreach($products as $product)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->category }}</td>
                                    <td><a title="Supplier Details" href="{{ url('supplier', $product->supplier) }}">{{ Helper::getNameByID($product->supplier, 'suppliers') }}</a> </td>
                                   <td>{{ Helper::getProductSale($product->id) . " " . $product->unit }}</td>
                                    <td>{{ Helper::getProductStock($product->id) - Helper::getProductSale($product->id) . " " . $product->unit}}</td>
                                    <td>{{ number_format($product->price) }}</td>
                                    <td>{{ number_format($product->cost1 + $product->cost2 + $product->cost3 + $product->cost4 + $product->cost5 + $product->profit) }}</td>
                                    <td>{{ number_format($product->price + $product->cost1 + $product->cost2 + $product->cost3 + $product->cost4 + $product->cost5 + $product->profit) }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('product', $product->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('product', $product->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
