@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ $product->title }}</h3>
                    <h5 class="widget-user-desc">{{ $product->category }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        
                                        <?php if($product->photo){ ?>
                                            <img src="{{ URL::asset('images/product/' . $product->photo)}}" alt="Product Photo">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="Product Photo">
                                        <?php } ?>
                                            
                                    </div>
                                    <!-- /.widget-user-image -->
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $product->title }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>کد</td>
                                                <td>{{ $product->code }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>دسته بندی</td>
                                                <td>{{ $product->category }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تامین کننده</td>
                                                <td><a title="Supplier Details" href="{{ url('supplier', $product->supplier) }}">{{ Helper::getNameByID($product->supplier, 'suppliers') }}</a> </td>                                                
                                            </tr>
                                            <tr>
                                                <td>مدل</td>
                                                <td>{{ $product->model }}</td>
                                            </tr>
                                            <tr>
                                                <td>واحد</td>
                                                <td>{{ $product->unitquantity . " قطعه در یک  " . $product->unit }}</td>
                                            </tr>                                            
                                            <tr>
                                                <td>مواد</td>
                                                <td>{{ $product->materials }}</td>
                                            </tr>
                                            <tr>
                                                <td>وزن</td>
                                                <td>{{ $product->weight }}</td>
                                            </tr>
                                            <tr>
                                                <td>ابعاد</td>
                                                <td>{{ $product->dimension }}</td>
                                            </tr>
                                            <tr>
                                                <td>رنگ</td>
                                                <td>{{ $product->color }}</td>
                                            </tr>
                                            <tr>
                                                <td>طراحی شده توسط</td>
                                                <td>{{ $product->design }}</td>
                                            </tr>
                                            <tr>
                                                <td>گنجایش</td>
                                                <td>{{ $product->capacity }}</td>
                                            </tr>
                                            <tr>
                                                <td>کیفیت</td>
                                                <td>{{ $product->quality }}</td>
                                            </tr>
                                            <tr>
                                                <td>سازنده</td>
                                                <td>{{ $product->origin }}</td>
                                            </tr>
                                            <tr>
                                                <td>نام انبار</td>
                                                <td>{{ $product->warehouse }}</td>
                                            </tr>
                                            <tr>
                                                <td>تاریخ ساخت</td>
                                                <td>{{ $product->manufacturedate }}</td>
                                            </tr>                                            
                                            <tr>
                                                <td>تاریخ انقضا</td>
                                                <td>{{ $product->expiredate }}</td>
                                            </tr>
                                            <tr>
                                                <td>لینک رسمی محصول</td>
                                                <td><a href="{{ $product->link }}" target="_blank">{{ $product->link }}</a></td>
                                            </tr>
                                            <tr>
                                                <td>فروش</td>
                                                <td>{{ number_format($product->price) . " " . $product->unit }} </td>
                                            </tr>
                                            <tr>
                                                <td>موجودی</td>
                                                <td>{{ number_format($product->price) . " " . $product->unit }} </td>
                                            </tr>
                                        <hr>
                                            <tr>
                                                <td>قیمت</td>
                                                <td>(+) {{ number_format($product->price) }}</td>
                                            </tr>
                                            @if($product->costtitle1 && $product->cost1)
                                            <tr>
                                                <td>{{ $product->costtitle1  }} </td>
                                                <td>(+) {{ number_format($product->cost1) }}</td>
                                            </tr>
                                            @endif
                                            @if($product->costtitle2 && $product->cost2)
                                            <tr>
                                                <td>{{ $product->costtitle2  }} </td>
                                                <td>(+) {{ number_format($product->cost2) }}</td>
                                            </tr>
                                            @endif
                                            @if($product->costtitle3 && $product->cost3)
                                            <tr>
                                                <td>{{ $product->costtitle3  }} </td>
                                                <td>(+) {{ number_format($product->cost3) }}</td>
                                            </tr>
                                            @endif
                                            @if($product->costtitle4 && $product->cost4)
                                            <tr>
                                                <td>{{ $product->costtitle4  }} </td>
                                                <td>(+) {{ number_format($product->cost4) }}</td>
                                            </tr>
                                            @endif
                                            @if($product->costtitle5 && $product->cost5)
                                            <tr>
                                                <td>{{ $product->costtitle5  }} </td>
                                                <td>(+) {{ number_format($product->cost5) }}</td>
                                            </tr>
                                            @endif
                                            
                                            <tr>
                                                <td>نرخ سود</td>
                                                <td>(+) {{ number_format($product->profit) }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>قیمت خالص محصول</strong></td>
                                                <td><strong>(=) {{ number_format($product->price + $product->cost1 + $product->cost2 + $product->cost3 + $product->cost4 + $product->cost5 + $product->profit) }}</strong></td>
                                            </tr>
                                            
                                            
                                                                                   
                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $product->description !!}
                                </div>
                                
                                
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection