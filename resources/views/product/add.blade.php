@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  محصول جدید</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'ProductController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('producttitle', 'نام')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('producttitle', '', ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیحات', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>  

                    @php $arrayCatLists = array(' ' => 'انتخاب دسته بندی'); @endphp
                    @if(count($categorieslist) > 0 )                            
                    @foreach($categorieslist as $category)
                    @php                                 
                    $arrayCatLists[$category->category] = $category->category;
                    @endphp
                    @endforeach                        
                    @endif

                    @php $supplierslist = array(' ' => 'انتخاب تامین کننده'); @endphp
                    @if(count($suppliers) > 0 )                            
                    @foreach($suppliers as $supplier)
                    @php 
                    $supplierslist[$supplier->id] = $supplier->fname;
                    @endphp
                    @endforeach                        
                    @endif   

                    <div class="col-md-12">
                        <h3 class="text-center text-bold margin-bottom">مشخصات محصول</h3>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('code', 'کد')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('code', '', ['class' => 'form-control', 'placeholder' => 'کد'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('', 'دسته بندی محصول')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('category', $arrayCatLists, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('supplier', 'تامین کننده محصول')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('model', 'مدل')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('model', '', ['class' => 'form-control', 'placeholder' => 'مدل محصول'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('unit', 'عدد/ست/دستگاه')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('unit', '', ['class' => 'form-control', 'placeholder' => 'عدد/ست/دستگاه', 'title' => 'Only Letter Ex: Unit, Important For Stock Management', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('unitquantity', 'تعداد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('unitquantity', '', ['class' => 'form-control', 'placeholder' => 'تعداد', 'title' => 'Insert Quantity Ex: 100, Important For Stock Management', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('materials', 'مواد')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('materials', '', ['class' => 'form-control', 'placeholder' => 'مواد'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('weight', 'وزن')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('weight', '', ['class' => 'form-control', 'placeholder' => 'وزن'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('dimension', 'ابعاد')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('dimension', '', ['class' => 'form-control', 'placeholder' => 'ابعاد'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('color', 'رنگ')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('color', '', ['class' => 'form-control', 'placeholder' => 'رنگ'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('design', 'طراحی شده توسط')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('design', '', ['class' => 'form-control', 'placeholder' => 'طراحی'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('capacity', 'گنجایش')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('capacity', '', ['class' => 'form-control', 'placeholder' => 'گنجایش'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('quality', 'کیفیت')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('quality', '', ['class' => 'form-control', 'placeholder' => 'کیفیت'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('origin', 'سازنده')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('origin', '', ['class' => 'form-control', 'placeholder' => 'سازنده'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('warehouse', 'نام انبار')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('warehouse', '', ['class' => 'form-control', 'placeholder' => 'نام انبار'] )}}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('manufacturedate', 'تاریخ ساخت')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('manufacturedate', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ ساخت'] )}}
                            </div>
                        </div>
                    </div>                        
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('expiredate', 'انقضا')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('expiredate', '', ['class' => 'form-control datepicker', 'placeholder' => 'انقضا'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('link', 'لینک رسمی محصول')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('link', '', ['class' => 'form-control', 'placeholder' => 'لینک رسمی محصول'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h3 class="text-center text-bold">قیمت، ارزش و سود</h3>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('price', 'قیمت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('price', '', ['class' => 'form-control', 'placeholder' => 'قیمت', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('profit', 'حاشیه سود ناخالص')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('profit', '', ['class' => 'form-control', 'placeholder' => 'سود بابت هر فروش', 'required'] )}}
                            </div>
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('costtitle1', 'عنوان هزینه 01')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('costtitle1', '', ['class' => 'form-control', 'placeholder' => 'عنوان هزینه 01'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cost1', 'هزینه 01')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('cost1', '', ['class' => 'form-control', 'placeholder' => 'هزینه 01'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('costtitle2', 'عنوان هزینه 02')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('costtitle2', '', ['class' => 'form-control', 'placeholder' => 'عنوان هزینه 02'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cost2', 'هزینه 02')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('cost2', '', ['class' => 'form-control', 'placeholder' => 'هزینه 02'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('costtitle3', 'عنوان هزینه 03')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('costtitle3', '', ['class' => 'form-control', 'placeholder' => 'عنوان هزینه 03'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cost3', 'هزینه 03')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('cost3', '', ['class' => 'form-control', 'placeholder' => 'هزینه 03'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('costtitle4', 'عنوان هزینه 04')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('costtitle4', '', ['class' => 'form-control', 'placeholder' => 'عنوان هزینه 04'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cost4', 'هزینه 04')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('cost4', '', ['class' => 'form-control', 'placeholder' => 'هزینه 04'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('costtitle5', 'عنوان هزینه 05')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('costtitle5', '', ['class' => 'form-control', 'placeholder' => 'عنوان هزینه 05'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cost5', 'هزینه 05')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('cost5', '', ['class' => 'form-control', 'placeholder' => 'هزینه 05'] )}}
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>                        



                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

