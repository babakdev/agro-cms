@extends('layouts.pos')
@section('content')

@php $supplierslist = array('' => 'انتخاب تامین کننده'); @endphp
@if(count($suppliers) > 0 )
@foreach($suppliers as $supplier)
@php
$supplierslist[$supplier->id] = $supplier->fname;
@endphp
@endforeach
@endif

@php $pcategorilist = array('' => 'انتخاب دسته بندی'); @endphp
@if(count($categorieslist) > 0 )
@foreach($categorieslist as $category)
@php
$pcategorilist[$category->category] = $category->category;
@endphp
@endforeach
@endif

<?php

$arrayMethod = array('' => 'انتخاب روش پرداخت');
$arrayMethod[0] = "نقدی";
$arrayMethod[1] = "چک";
// $arrayMethod[2] = "Stripe";
// $arrayMethod[3] = "Others";


if(session()->has('posDiscount')){
    $discount = session()->get('posDiscount'); //Item Discount
}else{
    $discount = 0; //Item Discount
}

if(session()->has('posTax')){
    $tax = session()->get('posTax'); //Item Discount
}else{
    $tax = 0; //Item Discount
}

if(session()->has('posShipping')){
    $shipping = session()->get('posShipping'); //Item Discount
}else{
    $shipping = 0; //Item Discount
}

?>

<section class="content text-center">
    <div class="row">

        <?php if(session()->has('posLastInvoice')){ ?>

            <div class="col-md-2 col-md-offset-5 topActionBar">
                <a href="{{ url('/invoice/' . session()->get('posLastInvoice')) }}" target="_blank"><button class="btn btn-lg btn-primary"><i class="material-icons">receipt</i> آخرین فاکتورها</button></a>
            </div>

        <?php } ?>

        <div class="col-md-7">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> بررسی </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover posCartTable">
                        <thead>
                            <tr>
                                <th>تصویر</th>
                                <th>محصول</th>
                                <th>تعداد</th>
                            	<th>قیمت</th>
                            	<th>جمع</th>
                            	<th><i class="material-icons">delete</i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = array();
                            if(session()->has('productIDs')){
                                $itemIDs = session()->get('productIDs'); //Item IDs array
                                $totalItmes = count($itemIDs);
                                $itemIDs = array_count_values($itemIDs);
                                $html = '';
                                foreach ($itemIDs as $key => $item) {
                                    $product = Helper::getProduct($key); //get product by ID Helper Function
                                    $total[] = $product->price * $item; //total price
                                    $html .= '<tr>
                                            <td>';
                                                if($product->photo){
                                                    $html .= '<img class="img-60" src=" ' . asset("images/product/" . $product->photo) . ' " alt="Product Photo">';
                                                } else{
                                                    $html .= '<img class="img-60" src=" ' . asset("images/default.png") . ' " alt="Product Photo">';
                                                }
                                            $html .= '</td>';
                                            $html .= '<td> ' . $product->title . '</td>';
                                            $html .= '<td class="input-group"><span class="decrement"><i class="material-icons">remove_circle</i></span><input disabled class="form-control text-center adjustToCart" data-itemID="' . $product->id . '" type="text" min="1" value="' . $item . '"><span class="increment"><i class="material-icons">add_circle</i></span></td>';
                                            $html .= '<td> ' . number_format($product->price) . '</td>';
                                            $html .= '<td> ' . number_format($product->price * $item) . '</td>';
                                            $html .= '<td><a class="text-red" href="#" data-itemID="' . $product->id . '"><i class="material-icons removeToCart">delete</i></a></td>';
                                        $html .= '</tr>';
                                } echo $html; ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>تعداد <strong>{{ number_format($totalItmes) }}</strong></td>
                                    <td>کل</td>
                                    <td><strong>{{ number_format(array_sum($total)) }}</strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>تخفیف</td>
                                    <td><strong>(-) {{ number_format($discount) }}</strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>مالیات</td>
                                    <td><strong>(+) {{ number_format($tax) }}</strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>هزینه حمل</td>
                                    <td><strong>(+) {{ number_format($shipping) }}</strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>جمع نهایی قابل پرداخت</td>
                                    <td><strong>(=) {{ number_format((array_sum($total) - $discount) + ($tax + $shipping)) }}</strong></td>
                                    <td></td>
                                </tr>
                            <?php }else{ ?>
                                <tr>
                                    <td>سبد فروش خالی</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div> <!-- End of Box -->

            <div class="col-md-12 actionBar">
                <div class="col-md-4">
                    <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#modal-discount"><i class="material-icons">attach_money</i> مالیات/هزینه حمل</button>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#modal-payment"><i class="material-icons">payment</i> اطلاعات پرداخت</button>
                </div>
                <div class="col-md-4">
                    {!! Form::open(['action' => 'PosController@resetpos', 'method' => 'POST']) !!}
                    <button type="submit" class="btn btn-lg btn-danger"><i class="material-icons">delete_forever</i> کنسل</button>
                    {!! Form::close() !!}
                </div>
            </div>



            <div class="modal fade" id="modal-discount">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">تخفیف/مالیات/حمل</h4>
                  </div>
                  <div class="modal-body">

                      {!! Form::open(['action' => 'PosController@updateBalance', 'files' => true, 'method' => 'POST']) !!}

                      <div class="col-md-12">
                          <div class="form-group">
                              {{ Form::label('discount', 'کل تخفیف')}}
                              <div class="input-group">
                                  {{ Form::number('discount', $discount, ['class' => 'form-control', 'placeholder' => 'کل تخفیف', 'min' => '0'] )}}
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group">
                              {{ Form::label('tax', 'مالیات')}}
                              <div class="input-group">
                                  {{ Form::number('tax', $tax, ['class' => 'form-control', 'placeholder' => 'مالیات', 'min' => '0'] )}}
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group">
                              {{ Form::label('shipping', 'هزینه حمل')}}
                              <div class="input-group">
                                  {{ Form::number('shipping', $shipping, ['class' => 'form-control', 'placeholder' => 'هزینه حمل', 'min' => '0'] )}}
                              </div>
                          </div>
                      </div>


                      <div class="col-md-12">
                          <div class="form-group">
                              <div class="input-group">
                                  {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                              </div>
                          </div>
                      </div>

                      {!! Form::close() !!}

                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="modal-payment">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">اطلاعات پرداخت</h4>
                  </div>
                  <div class="modal-body">

                      {!! Form::open(['action' => 'PosController@payment', 'files' => true, 'method' => 'POST', 'id' => 'payment-form']) !!}

                      <div class="col-md-6">
                          <div class="form-group">
                              {{ Form::label('customer', 'نام مشتری')}} <span class="text-red"> (*)</span>
                              <div class="input-group">
                                  {{ Form::text('customer', '', ['class' => 'form-control', 'placeholder' => 'نام مشتری', 'required'] )}}
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group">
                              {{ Form::label('customer_phone', 'تلفن مشتری')}} <span class="text-red"> (*)</span>
                              <div class="input-group">
                                  {{ Form::text('customer_phone', '', ['class' => 'form-control', 'placeholder' => 'تلفن مشتری', 'required'] )}}
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group">
                              {{ Form::label('xxtotal', 'مبلغ کل')}}
                              <div class="input-group">
                                  <input type="hidden" class="total" name="total" value="{{ (array_sum($total) - $discount) + ($tax + $shipping) }}">
                                  {{ Form::number('xxtotal', (array_sum($total) - $discount) + ($tax + $shipping), ['class' => 'form-control', 'placeholder' => 'مبلغ کل', 'min' => '0', 'disabled'] )}}
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group">
                              {{ Form::label('method', 'نحوه پرداخت')}} <span class="text-red"> (*)</span>
                              <div class="input-group">
                                  {{ Form::select('method', $arrayMethod, null, ['class' => 'form-control paymentMethod' ] )}}
                              </div>
                          </div>
                      </div>


                      <div class="col-md-12 stripeInput" style="display:none;">
                          <div class="form-group">
                              {{ Form::label('method', 'کارت اعتباری')}}
                              <div class="input-group">
                                  <div id="card-element">
                                    <!-- a Stripe Element will be inserted here. -->
                                  </div>
                                  <!-- Used to display form errors -->
                                  <div id="card-errors"></div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-12">
                          <div class="form-group">
                              {{ Form::label('description', 'توضیحات')}}
                              <div class="input-group">
                                  {{ Form::textarea('description', '', ['class' => 'form-control' ] )}}
                              </div>
                          </div>
                      </div>

                      <div class="col-md-12">
                          <div class="form-group">
                              <div class="input-group">
                                  {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                              </div>
                          </div>
                      </div>
                      {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>

        </div>
        <div class="col-md-5">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> محصولات </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <form action="#" method="post">

                        <div class="col-md-12">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="input-group">
                                        {{ Form::select('category', $pcategorilist, null, ['id' => 'category', 'class' => 'form-control', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="searchBtn btn btn-primary"><i class="material-icons md-16">search</i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </from>


                    <table class="table table-bordered table-hover dtPosProducts">
                        <thead>
                            <tr>
                                <th>تصویر</th>
                                <th>محصول</th>
                            	<th>قیمت</th>
                            	<th><i class="material-icons">add_circle</i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($products) > 0 )
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <?php if($product->photo){ ?>
                                                <img class="img-60" src="{{ URL::asset('images/product/' . $product->photo)}}" alt="Product Photo">
                                            <?php } else{ ?>
                                                <img class="img-60" src="{{ URL::asset('images/default.png')}}" alt="Product Photo">
                                            <?php } ?>
                                        </td>
                                        <td>{{ $product->title }}</td>
                                        <td>{{ number_format($product->price) }}</td>
                                        <td><a class="addToCart" href="#" data-itemID="{{ $product->id }}"><i class="material-icons">add_circle</i></a></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
