@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">محصولات خسارت دیده</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtProduct text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>محصول</th>
                                <th>دسته بندی</th>
                                <th>تامین کننده</th>
                                <th>بازرگان</th>
                                <th>تعداد</th>
                                <th>کل مبلغ</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($damages) > 0 )

                            @foreach($damages as $damage)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($damage->damagedate)->format('j M Y') }}</td>
                                    <td><a title="Product Details" href="{{ url('product', $damage->product)}}">{{ Helper::getProductTitle( $damage->product ) }}</a></td>
                                    <td>{{ $damage->category }}</td>
                                    <td><a title="Supplier Details" href="{{ url('supplier', $damage->supplier)}}">{{ Helper::getNameByID($damage->supplier, 'suppliers') }}</a></td>
                                    <td><a title="Merchant Details" href="{{ url('merchant', $damage->merchant)}}">{{ Helper::getNameByID($damage->merchant, 'merchants') }}</a></td>
                                    <td>{{ number_format($damage->quantity, 2) }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($damage->total, 2) }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('damage', $damage->id)}}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('damage', $damage->id)}}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('damage.destroy', $damage->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
