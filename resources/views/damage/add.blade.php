@extends('layouts.app')

@section('content')

@php $arrayCatLists = array(' ' => 'انتخاب دسته بندی'); @endphp
@if(count($categorieslist) > 0 )                            
@foreach($categorieslist as $category)
@php                                 
$arrayCatLists[$category->category] = $category->category;
@endphp
@endforeach                        
@endif

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )                            
@foreach($merchants as $merchant)
@php 
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach                        
@endif

@php $supplierlist = array(' ' => 'انتخاب تامین کننده'); @endphp
@if(count($suppliers) > 0 )                            
@foreach($suppliers as $supplier)
@php 
$supplierlist[$supplier->id] = $supplier->fname;
@endphp
@endforeach                        
@endif

@php $productslist = array(' ' => 'انتخاب محصول'); @endphp
@if(count($products) > 0 )                            
@foreach($products as $product)
@php 
$productslist[$product->id] = $product->title;
@endphp
@endforeach                        
@endif   

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  محصول خسارت دیده جدید</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'DamageController@store', 'files' => true, 'method' => 'POST']) !!}

                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('product', 'انتخاب محصول')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('product', $productslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('date', 'تاریخ خسارت/ارجاع')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('date', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ خسارت/ارجاع', 'required'] )}}
                            </div>
                        </div>
                    </div>  
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیح')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیح', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('category', 'انتخاب دسته بندی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('category', $arrayCatLists, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('merchant', 'انتخاب بازرگان (مرجوع کننده)')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('merchant', $merchantslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('supplier', 'انتخاب تامین کننده')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierlist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('unit', 'واحد شمارش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('unit', '', ['class' => 'form-control', 'placeholder' => 'عدد/ست/دستگاه', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('quantity', 'تعداد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('quantity', '', ['class' => 'form-control quantity', 'placeholder' => 'تعداد', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('total', 'کل مبلغ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('total', '', ['class' => 'form-control total', 'placeholder' => 'کل مبلغ', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>   
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>

@endsection

