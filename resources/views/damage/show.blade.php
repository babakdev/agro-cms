@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">
                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ Helper::getProductTitle($damage->product) }}</h3>
                    <h5 class="widget-user-desc">{{ $damage->category }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">دلیل خسارت / ارجاع</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>محصول</td>
                                                <td><a title="Product Details" href="/product/{{ $damage->product }}">{{ Helper::getProductTitle( $damage->product ) }}</a></td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>تاریخ</td>
                                                <td>{{ Carbon::parse($damage->date)->format('j M Y') }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>دسته بندی</td>
                                                <td>{{ $damage->category }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تامین کننده</td>
                                                <td><a title="Supplier Details" href="/supplier/{{ $damage->supplier }}">{{ Helper::getNameByID($damage->supplier, 'suppliers') }}</a></td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>بازرگان</td>
                                                <td><a title="Merchant Details" href="/merchant/{{ $damage->merchant }}">{{ Helper::getNameByID($damage->merchant, 'merchants') }}</a></td>                                                
                                            </tr> 
                                            <tr>
                                                <td>تعداد</td>
                                                <td>{{ $damage->quantity }} {{ $damage->unit }}</td>
                                            </tr> 
                                            <tr>
                                                <td><strong>کل مبلغ</strong></td>
                                                <td><strong> {{ Helper::getCurrency(). " " . number_format($damage->total, 2) }}</strong></td>
                                            </tr>
                                            
                                                                                   
                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $damage->description !!}
                                </div>
                                
                                
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection