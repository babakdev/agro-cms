@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ $asset->title }}</h3>
                    <h5 class="widget-user-desc">{{ Helper::getCurrency(). " " . number_format($asset->value, 2) }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        
                                        <?php if($asset->photo){ ?>
                                            <img src="{{ URL::asset('images/asset/' . $asset->photo)}}" alt="Asset Photo">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="Asset Photo">
                                        <?php } ?>
                                            
                                    </div>
                                    <!-- /.widget-user-image -->
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $asset->title }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>ارزش</td>
                                                <td>{{ Helper::getCurrency(). " " . number_format($asset->value, 2) }}</td>                                                
                                            </tr>    
                                            
                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $asset->description !!}
                                </div>
                                
                                
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection