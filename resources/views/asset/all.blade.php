@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه دارایی ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtAsset text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>عنوان</th>
                                <th>ارزش</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($assets) > 0 )

                            @foreach($assets as $asset)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $asset->title }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($asset->value, 2) }}</td><td>
                                        <span class="badge bg-blue"><a href="{{ url('asset', $asset->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('asset', $asset->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('asset.destroy', $asset->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
