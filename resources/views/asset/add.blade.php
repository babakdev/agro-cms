@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  دارایی جدید  </h3>
                    <h3 class="box-title left">
                        <button type="button" class="btn btn-primary photo_input float-right">انتخاب عکس - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'AssetController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('title', 'نام دارایی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'نام دارایی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('value', 'ارزش دارایی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('value', '', ['class' => 'form-control', 'placeholder' => 'ارزش دارایی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">                                                                  
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیحات'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>  
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

