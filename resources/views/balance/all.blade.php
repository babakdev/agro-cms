@extends('layouts.app')
@section('content')
@include('inc.balancecounter')

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه ورودی های درآمد / هزینه</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtBalance text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>ماهیت</th>
                                <th>عنوان</th>
                                <th>مقدار</th>
                                <th>توسط</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($balances) > 0 )

                            @foreach($balances as $balance)

                                <tr @if( $balance->type == "Expense") style="background:#FFF9C4 !important" @endif>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($balance->balancedate)->format('j M Y') }}</td>
                                    <td>{{ $balance->type }}</td>
                                    <td>{{ $balance->title }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($balance->amount, 2) }}</td>
                                    <td>{{ $balance->by }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('balance', $balance->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('balance', $balance->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('balance.destroy', $balance->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
