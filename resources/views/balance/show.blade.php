@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ $balance->title }}</h3>
                    <h5 class="widget-user-desc">{{ $balance->type }}</h5>
                </div>
                
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>عنوان</td>
                                                <td>{{ $balance->title }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>تاریخ</td>
                                                <td>{{ Carbon::parse($balance->balancedate)->format('j M Y') }}</td>                                                
                                            </tr>                                            
                                            
                                            <tr>
                                                <td>تایید شده توسط</td>
                                                <td>{{ $balance->by }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <td><strong>کل مبلغ</strong></td>
                                                <td><strong> {{ Helper::getCurrency(). " " . number_format($balance->amount, 2) }}</strong></td>
                                            </tr>

                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $balance->description !!}
                                </div>

                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection