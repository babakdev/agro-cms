@extends('layouts.app')

@section('content')

@include('inc.balancecounter')
        
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Browse Entries By Year</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtbyyear">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>ماه</th>
                                <th>درآمد</th>
                                <th>هزینه</th>
                                <th>تراز</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <tr>
                                <td>01</td>                                
                                <td>January</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($janincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($janexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($janincome - $janexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>02</td>                                
                                <td>February</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($febincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($febexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($febincome - $febexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>03</td>                                
                                <td>March</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($marincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($marexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($marincome - $marexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>04</td>                                
                                <td>April</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($aprincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($aprexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($aprincome - $aprexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>05</td>                                
                                <td>May</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($mayincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($mayexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($mayincome - $mayexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>06</td>                                
                                <td>June</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($junincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($junexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($junincome - $junexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>07</td>                                
                                <td>July</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($julincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($julexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($julincome - $julexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>08</td>                                
                                <td>August</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($augincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($augexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($augincome - $augexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>09</td>                                
                                <td>September</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($sepincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($sepexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($sepincome - $sepexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>10</td>                                
                                <td>October</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($octincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($octexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($octincome - $octexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>11</td>                                
                                <td>November</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($novincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($novexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($novincome - $novexpense, 2) }}</td>
                            </tr>
                            <tr>
                                <td>12</td>                                
                                <td>December</td>                                
                                <td>{{ Helper::getCurrency(). " " . number_format($decincome, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($decexpense, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format($decincome - $decexpense, 2) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

