@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  اضافه کردن درآمد / هزینه</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'BalanceController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('type', 'ماهیت ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('type', [' ' => 'انتخاب کنید', 'Income' => 'درآمد', 'Expense' => 'هزینه'], null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('title', 'عنوان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'عنوان', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیح')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیح', 'rows' => 3] )}}
                            </div>
                        </div>
                    </div> 
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('balancedate', 'تاریخ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('balancedate', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('amount', 'مقدار ریالی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('amount', '', ['class' => 'form-control', 'placeholder' => 'مقدار ریالی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('by', 'تایید شده توسط')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('by', '', ['class' => 'form-control', 'placeholder' => 'تایید شده توسط'] )}}
                            </div>
                        </div>
                    </div>             
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div> 
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

