
@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  ویرایش درآمد / هزینه</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['BalanceController@update', $balance->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('type', 'ماهیت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('type', [' ' => 'ماهیت', 'Income' => 'درآمد', 'Expense' => 'هزینه'], $balance->type, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('title', 'عنوان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', $balance->title, ['class' => 'form-control', 'placeholder' => 'عنوان', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیح')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', $balance->description, ['class' => 'form-control', 'placeholder' => 'توضیح', 'rows' => 3] )}}
                            </div>
                        </div>
                    </div> 
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('balancedate', 'تاریخ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('balancedate', $balance->balancedate, ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('amount', 'مقدار ریالی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('amount', $balance->amount, ['class' => 'form-control', 'placeholder' => 'مقدار ریالی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('by', 'تایید شده توسط')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('by', $balance->by, ['class' => 'form-control', 'placeholder' => 'تایید شده توسط'] )}}
                            </div>
                        </div>
                    </div>             
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div> 
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

