@extends('layouts.app')

@section('content')

@include('inc.balancecounter')
        
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">ورودی ها بر حسب ماه</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtBalance">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>ماهیت</th>
                                <th>عنوان</th>
                                <th>مقدار</th>
                                <th>توسط</th>
                                <th style="width: 20%">عملیات</th>
                            </tr>
                        </thead> 
                        <tbody>
                            
                            @if(count($bymonths) > 0 )
                            
                            @foreach($bymonths as $bymonth)
                            
                                <tr @if( $bymonth->type == "Expense") style="background:#FFF9C4 !important" @endif>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($bymonth->balancedate)->format('j M Y') }}</td>
                                    <td>{{ $bymonth->type }}</td>
                                    <td>{{ $bymonth->title }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($bymonth->amount, 2) }}</td>
                                    <td>{{ $bymonth->by }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="/balance/{{ $bymonth->id }}" title="نمایش">View</a></span>
                                        <span class="badge bg-yellow"><a href="/balance/{{ $bymonth->id }}/edit/" title="ویرایش">ویرایش</a></span>
                                        <form action="{{ route('balance.destroy', $bymonth->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف">Delete</button></span>
                                        </form>				  
                                    </td>
                                </tr>

                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

