@extends('layouts.access')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-default access">
                <h3 class="box-title text-center">
                    {{ Helper::getSettings()->title }} <?php if(Helper::getSettings()->tag !== " "){?> | {{ Helper::getSettings()->tag }} <?php } ?>
                    <img class="customLogo" src="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" alt="Logo">
                    <br>Classic Login</h3>
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-3">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ایمیل" required autofocus>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-3">
                                <input id="password" type="password" class="form-control" name="password" placeholder="رمزعبور" required>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> مرا به خاطر بسپار
                                    </label>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        رمزعبور خود را فراموش کرده اید؟
                                    </a>
                                    <br>
                                    <button type="submit" class="btn btn-primary">ورود</button>
                                    <a href="{{ url('/register') }}"><button class="btn btn-primary">عضویت</button></a>

                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="social-login">
                        <h3 class="box-title text-center">  ورود با جیمیل</h3>

                        <div class="col-md-8 col-md-offset-3">
                            <p class="social-icon">
                                {{-- <a href="{{ url('/login/facebook')}}" class="btn btn-facebook">Facebook</a> --}}
                                <a href="{{ url('/login/google')}}" class="btn btn-google">Google</a>
                            </p>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
