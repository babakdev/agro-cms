@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> تنظیمات</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">لوگوی شرکت - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['SettingsController@update', $settings->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}
                                        
                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('title', 'عنوان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', $settings->title, ['class' => 'form-control', 'placeholder' => 'عنوان', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('tag', 'عنوان دوم')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('tag', $settings->tag, ['class' => 'form-control', 'placeholder' => 'Tag Line', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('currency', 'انتخاب واحد پولی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                
                                <select name="currency" class="form-control" required>                                    
                                    <option selected value="{{ $settings->currency }}">{{ $settings->currency }}</option>
                                    <option value="DZD">Algeria Dinars </option>
                                    <option value="ARP">Argentina Pesos </option>
                                    <option value="AUD">Australia Dollars </option>
                                    <option value="ATS">Austria Schillings </option>
                                    <option value="BSD">Bahamas Dollars </option>
                                    <option value="BBD">Barbados Dollars </option>
                                    <option value="BDT">Bangladesh Taka </option>
                                    <option value="BEF">Belgium Francs </option>
                                    <option value="BMD">Bermuda Dollars </option>
                                    <option value="BRR">Brazil Real </option>
                                    <option value="BGL">Bulgaria Lev </option>
                                    <option value="CAD">Canada Dollars </option>
                                    <option value="CLP">Chile Pesos </option>
                                    <option value="CNY">China Yuan Renmimbi </option>
                                    <option value="CYP">Cyprus Pounds </option>
                                    <option value="CSK">Czech Republic Koruna </option>
                                    <option value="DKK">Denmark Kroner </option>
                                    <option value="NLG">Dutch Guilders </option>
                                    <option value="XCD">Eastern Caribbean Dollars </option>
                                    <option value="EGP">Egypt Pounds </option>
                                    <option value="EUR">Euro </option>
                                    <option value="FJD">Fiji Dollars </option>
                                    <option value="FIM">Finland Markka </option>
                                    <option value="FRF">France Francs </option>
                                    <option value="DEM">Germany Deutsche Marks </option>
                                    <option value="GRD">Greece Drachmas </option>
                                    <option value="HKD">Hong Kong Dollars </option>
                                    <option value="HUF">Hungary Forint </option>
                                    <option value="ISK">Iceland Krona </option>
                                    <option value="INR">India Rupees </option>
                                    <option value="IDR">Indonesia Rupiah </option>
                                    <option value="IEP">Ireland Punt </option>
                                    <option value="IRR">Iranian Riyal </option>
                                    <option value="ITL">Italy Lira </option>
                                    <option value="JMD">Jamaica Dollars </option>
                                    <option value="JPY">Japan Yen </option>
                                    <option value="JOD">Jordan Dinar </option>
                                    <option value="KRW">Korea (South) Won </option>
                                    <option value="LBP">Lebanon Pounds </option>
                                    <option value="LUF">Luxembourg Francs </option>
                                    <option value="MYR">Malaysia Ringgit </option>
                                    <option value="MXP">Mexico Pesos </option>
                                    <option value="NLG">Netherlands Guilders </option>
                                    <option value="NZD">New Zealand Dollars </option>
                                    <option value="NOK">Norway Kroner </option>
                                    <option value="PKR">Pakistan Rupees </option>
                                    <option value="PHP">Philippines Pesos </option>
                                    <option value="PLZ">Poland Zloty </option>
                                    <option value="PTE">Portugal Escudo </option>
                                    <option value="ROL">Romania Leu </option>
                                    <option value="RUR">Russia Rubles </option>
                                    <option value="SAR">Saudi Arabia Riyal </option>
                                    <option value="SGD">Singapore Dollars </option>
                                    <option value="SKK">Slovakia Koruna </option>
                                    <option value="ZAR">South Africa Rand </option>
                                    <option value="KRW">South Korea Won </option>
                                    <option value="ESP">Spain Pesetas </option>
                                    <option value="SDD">Sudan Dinar </option>
                                    <option value="SEK">Sweden Krona </option>
                                    <option value="CHF">Switzerland Francs </option>
                                    <option value="TWD">Taiwan Dollars </option>
                                    <option value="THB">Thailand Baht </option>
                                    <option value="TTD">Trinidad and Tobago Dollars </option>
                                    <option value="TRL">Turkey Lira </option>
                                    <option value="GBP">United Kingdom Pounds </option>
                                    <option value="USD">USD United States Dollars </option>
                                    <option value="VEB">Venezuela Bolivar </option>
                                    <option value="ZMK">Zambia Kwacha </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('email', 'ایمیل شرکت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('email', $settings->email, ['class' => 'form-control', 'placeholder' => 'ایمیل', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن شرکت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('phone', $settings->phone, ['class' => 'form-control', 'placeholder' => 'تلفن', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('address', 'آدرس')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('address', $settings->address, ['class' => 'form-control', 'placeholder' => 'آدرس', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('city', 'شهر')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('city', $settings->city, ['class' => 'form-control', 'placeholder' => 'شهر', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('country', 'کشور')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('country', $settings->country, ['class' => 'form-control', 'placeholder' => 'کشور', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('postal', 'کدپستی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('postal', $settings->postal, ['class' => 'form-control', 'placeholder' => 'کدپستی', 'required'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('license1', 'تحت لیسانس 01')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('license1', $settings->license1, ['class' => 'form-control', 'placeholder' => 'تحت لیسانس 01'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('license2', 'تحت لیسانس 02')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('license2', $settings->license2, ['class' => 'form-control', 'placeholder' => 'تحت لیسانس 02'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('license3', 'تحت لیسانس 03')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('license3', $settings->license3, ['class' => 'form-control', 'placeholder' => 'تحت لیسانس 03'] )}}
                            </div>
                        </div>
                    </div>                    
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div> 

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

