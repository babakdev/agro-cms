<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($todayincome->sum('amount'), 2) }}</h3>
              <p>جمع درآمد امروز</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($monthincome->sum('amount'), 2) }}</h3>
              <p>جمع درآمد ماهیانه</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($yearincome->sum('amount'), 2) }}</h3>
              <p>درآمد سالیانه</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>


        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($todayexpense->sum('amount'), 2) }}</h3>
              <p>جمع هزینه امروز</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($monthexpense->sum('amount'), 2) }}</h3>
              <p>جمع هزینه ماهیانه</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($yearexpense->sum('amount'), 2) }}</h3>
              <p>جمع هزینه سالیانه</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>


        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($balances->where('type', 'Income')->sum('amount'), 2) }}</h3>
              <p>کل درآمد</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($balances->where('type', 'Expense')->sum('amount'), 2) }}</h3>
              <p>کل هزینه کردها</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box ">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format($balances->where('type', 'Income')->sum('amount') - $balances->where('type', 'Expense')->sum('amount') , 2)}}</h3>
              <p>تراز مالی</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>


        @php
            $arrayYears = array(
                ' ' => 'انتخاب سال',
                date('Y') => date('Y'),
                '2017' => '2017',
                '2016' => '2016',
                '2015' => '2015',
                '2014' => '2014',
                '2013' => '2013',
                '2012' => '2012',
                '2011' => '2011',
                '2010' => '2010',
            );
        @endphp

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  بر حسب ماه</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'BalanceController@bymonth', 'files' => true, 'method' => 'POST']) !!}


                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('month', 'انتخاب ماه')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::selectMonth('month', null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('year', 'انتخاب سال')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::select('year', $arrayYears, null, ['class' => 'form-control', 'required'])}}
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>



        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  بر حسب سال</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'BalanceController@byyear', 'files' => true, 'method' => 'POST']) !!}


                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('year', 'انتخاب سال')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::select('year', $arrayYears, null, ['class' => 'form-control', 'required'])}}
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
