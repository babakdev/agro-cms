@extends('layouts.app')

@section('content')
<?php if (Auth::user()->role == "admin") { ?>

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-md-6">
            <div class="box box-solid purchasensale">
                <div class="box-header">
                    <h3 class="box-title"><i class="material-icons">equalizer</i> خرید و فروش</h3>
                    <canvas id="purchasensale" width="400" height="150"></canvas>
                </div>
            </div>
        </section>

        <section class="col-md-6">
            <div class="box box-solid incomenexpanse">
                <div class="box-header">
                    <h3 class="box-title"><i class="material-icons">equalizer</i> درآمد و هزینه</h3>
                    <canvas id="incomenexpanse" width="400" height="150"></canvas>
                </div>
            </div>
        </section>
    </div>

    <div class="row">
        <div class="btmMediumSpace"></div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="nav-tabs-custom homepage-counter">
                <ul class="nav nav-tabs mb-20">
                  <li class="active"><a href="#today" data-toggle="tab">امروز</a></li>
                  <li><a href="#week" data-toggle="tab">این هفته</a></li>
                  <li><a href="#month" data-toggle="tab">این ماه</a></li>
                  <li><a href="#year" data-toggle="tab">سال جاری</a></li>
                  <li><a href="#total" data-toggle="tab">کل</a></li>
              </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="today">
                      <div class="col-lg-8">
                          <div class="col-lg-6 col-xs-12">
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todayincome, 2) }}</h3>
                                      <p> درآمد </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todayexpense, 2) }}</h3>
                                      <p> هزینه </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todaypurchase, 2) }}</h3>
                                      <p> خرید </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format(Helper::todayTSale(), 2) }}</h3>
                                      <p> فروش </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>
                      </div> <!-- Counter -->

                      <div class="col-lg-4">
                         <div class="col-md-12 pieChart" style="position: relative; height:40vh; width:35vw; right:25%">
                             <canvas id="todayChart"></canvas>
                         </div>
                      </div> <!-- Chart -->
                  </div>  <!--End Today -->

                  <div class="tab-pane" id="week">
                      <div class="col-lg-8">
                          <div class="col-lg-6 col-xs-12">
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todayincome, 2) }}</h3>
                                      <p> درآمد </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todayexpense, 2) }}</h3>
                                      <p> هزینه </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format($todaypurchase, 2) }}</h3>
                                      <p> خرید </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-6 col-xs-12">
                              <!-- small box -->
                              <div class="small-box counter-box">
                                  <div class="inner">
                                      <h3>{{ Helper::getCurrency(). " " . number_format(Helper::weekTSale(), 2) }}</h3>
                                      <p> فروش </p>
                                  </div>
                                  <div class="icon">
                                      <i class="material-icons">equalizer</i>
                                  </div>
                              </div>
                          </div>
                      </div> <!-- Counter -->

                      <div class="col-lg-4">
                          <div class="col-md-12 pieChart" style="position: relative; height:40vh; width:35vw; right:25%">
                               <canvas id="weekChart"></canvas>
                          </div>
                      </div> <!-- Chart -->
                  </div> <!-- End Of week -->

                  <div class="tab-pane" id="month">
                      <div class="col-lg-8">
                            <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($monthincome, 2) }}</h3>
                                  <p> درآمد </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($monthexpense, 2) }}</h3>
                                  <p> هزینه </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($monthpurchase, 2) }}</h3>
                                  <p> خرید </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format(Helper::monthTSale(), 2) }}</h3>
                                  <p> فروش </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>
                  </div>
                      <div class="col-lg-4">
                          <div class="col-md-12 pieChart" style="position: relative; height:40vh; width:35vw; right:25%">
                               <canvas id="monthChart"></canvas>
                          </div>
                      </div> <!-- Chart -->
                  </div> <!-- End of Month -->

                  <div class="tab-pane" id="year">
                      <div class="col-lg-8">
                          <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($yearincome, 2) }}</h3>
                                  <p> درآمد </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($yearexpense, 2) }}</h3>
                                  <p> هزینه </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($yearpurchase, 2) }}</h3>
                                  <p> خرید </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format(Helper::yearTSale(), 2) }}</h3>
                                  <p> فروش </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>
                    </div>
                      <div class="col-lg-4">
                          <div class="col-md-12 pieChart" style="position: relative; height:40vh; width:35vw; right:25%">
                               <canvas id="yearChart"></canvas>
                          </div>
                      </div> <!-- Chart -->
                  </div> <!-- End of Year -->

                  <div class="tab-pane" id="total">
                      <div class="col-lg-8">
                        <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($balances->where('type', 'Income')->sum('amount'), 2) }}</h3>
                                  <p> درآمد </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                          <div class="small-box counter-box">
                              <div class="inner">
                                  <h3>{{ Helper::getCurrency(). " " . number_format($purchases->sum('total'), 2) }}</h3>
                                  <p> خرید </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6 col-xs-12">
                          <!-- small box -->
                          <div class="small-box counter-box">
                               <div class="inner">
                                   <h3>{{ Helper::getCurrency(). " " . number_format($balances->where('type', 'Expense')->sum('amount'), 2) }}</h3>
                                   <p> هزینه </p>
                              </div>
                              <div class="icon">
                                  <i class="material-icons">equalizer</i>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                        <!-- small box -->
                        <div class="small-box counter-box">
                          <div class="inner">
                            <h3>{{ Helper::getCurrency(). " " . number_format(Helper::tpsalesamount(), 2) }}</h3>
                            <p>فروش </p>
                          </div>
                          <div class="icon">
                            <i class="material-icons">equalizer</i>
                          </div>
                        </div>
                      </div>
                    </div>
                      <div class="col-lg-4">
                         <div class="col-md-12 pieChart" style="position: relative; height:40vh; width:35vw; right:25%">
                               <canvas id="totalChart"></canvas>
                          </div>
                      </div> <!-- Chart -->
                  </div> <!-- End of Total -->


                </div>
              </div>
              <!-- nav-tabs-custom -->
        </div>
        </div>

        <div class="row">
            <div class="btmMediumSpace"></div>
        </div>



    <div class="row">

        <div class="col-lg-12 col-xs-12">
            <div class="nav-tabs-custom homepage-counter">
                <ul class="nav nav-tabs mb-20">
                  <li class="active"><a href="#in_ex_events" data-toggle="tab">رویدادهای درآمد و هزینه </a></li>
                  <li><a href="#pu_sal_events" data-toggle="tab">رویدادهای خرید و فروش</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="in_ex_events">
                            <section class="col-md-6">
                                <div class="box box-solid incomeCal">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="material-icons">date_range</i> درآمدها</h3>
                                        <div id="incomeCal"></div>
                                    </div>
                                </div>
                            </section>

                            <section class="col-md-6">
                                <div class="box box-solid expenseCal">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="material-icons">date_range</i> هزینه ها</h3>
                                        <div id="expenseCal"></div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    <!--End of Income $expense Clander-->
                    <div class="tab-pane" id="pu_sal_events">

                                <section class="col-md-6">
                                    <div class="box box-solid purchaseCal">
                                        <div class="box-header">
                                            <h3 class="box-title"><i class="material-icons">date_range</i> خریدها</h3>
                                            <div id="purchaseCal"></div>
                                        </div>
                                    </div>
                                </section>

                                <section class="col-md-6">
                                    <div class="box box-solid saleCal">
                                        <div class="box-header">
                                            <h3 class="box-title"><i class="material-icons">date_range</i> فروش</h3>
                                            <div id="saleCal"></div>
                                        </div>
                                    </div>
                                </section>
                    </div>
                    <!--End of Purchase & Salrs Clander-->
                </div>
            </div>
              <!-- nav-tabs-custom -->
        </div>
    </div>


        <div class="row">
        <?php } else { ?>

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box counter-box">
                    <div class="inner">
                        <h3>{{ number_format($userPurchase, 2) }}</h3>
                        <p>کل خریدها</p>
                    </div>
                    <div class="icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box counter-box">
                    <div class="inner">
                        <h3>{{ number_format($userSupplie, 2) }}</h3>
                        <p>کل موجودی</p>
                    </div>
                    <div class="icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box counter-box">
                    <div class="inner">
                        <h3>{{ number_format($userInvoice, 2) }}</h3>
                        <p>کل فاکتورها</p>
                    </div>
                    <div class="icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                </div>
            </div>


        <?php } ?>

    </div>


    <?php if (Auth::user()->role == "admin") { ?>

        <script>
            $(document).ready(function () {

                var ctx = document.getElementById("purchasensale");
                    var purchasensaleChart = new Chart(ctx, {
                    type: 'line',
                            data: {
                                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                                    datasets: [{
                                    label: 'Sale',
                                        data: [{{ $jansale }}, {{ $febsale }}, {{ $marsale }}, {{ $aprsale }}, {{ $maysale }}, {{ $junsale }}, {{ $julsale }}, {{ $augsale }}, {{ $sepsale }}, {{ $octsale }}, {{ $novsale }}, {{ $decsale }}],
                                        backgroundColor: "rgb(139,195,74, 0.7)"
                                    }, {
                                    label: 'Purchase',
                                        data: [{{ $janpurchase }}, {{ $febpurchase }}, {{ $marpurchase }}, {{ $aprpurchase }}, {{ $maypurchase }}, {{ $junpurchase }}, {{ $julpurchase }}, {{ $augpurchase }}, {{ $seppurchase }}, {{ $octpurchase }}, {{ $novpurchase }}, {{ $decpurchase }}],
                                        backgroundColor: "rgb(156,39,176, 0.7)"
                                    }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            fontColor: '#8d8d8d'
                                        }
                                    }],
                                    xAxes: [{
                                        ticks: {
                                            fontColor: '#8d8d8d'
                                        }
                                    }]
                                }
                            }
                    });

                    var ctx = document.getElementById("incomenexpanse");
                    var incomenexpanseChart = new Chart(ctx, {
                    type: 'line',
                        data: {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                            datasets: [{
                            label: 'Income',
                                data: [{{ $janincome }}, {{ $febincome }}, {{ $marincome }}, {{ $aprincome }}, {{ $mayincome }}, {{ $junincome }}, {{ $julincome }}, {{ $augincome }}, {{ $sepincome }}, {{ $octincome }}, {{ $novincome }}, {{ $decincome }}],
                                backgroundColor: "rgb(139,195,74, 0.7)"
                            }, {
                            label: 'Expense',
                                data: [{{ $janexpense }}, {{ $febexpense }}, {{ $marexpense }}, {{ $aprexpense }}, {{ $mayexpense }}, {{ $junexpense }}, {{ $julexpense }}, {{ $augexpense }}, {{ $sepexpense }}, {{ $octexpense }}, {{ $novexpense }}, {{ $decexpense }}],
                                backgroundColor: "rgb(156,39,176, 0.7)"
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        fontColor: '#8d8d8d'
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        fontColor: '#8d8d8d'
                                    }
                                }]
                            }
                        }
                    });


                    //Today Round Chart
                    var ctxTodayChart = document.getElementById("todayChart");
                    var ctxTodayChart = new Chart(ctxTodayChart, {
                        type: 'doughnut',
                        data: {
                            labels: ['Income', 'Expense', 'Purchase', 'Sale'],
                            datasets: [{
                                label: "Today Chart",
                                data: [{{ $todayincome }}, {{ $todayexpense }}, {{ $todaypurchase }}, {{ Helper::todayTSale() }}],
                                backgroundColor: ["rgb(100, 181, 246)", "rgb(229, 115, 115)", "rgb(255, 213, 79)", "rgb(174, 213, 129)"]
                            }]
                        },
                        options: {
                             legend: {
                                display: true
                             },
                             tooltips: {
                                enabled: true
                             }
                        }
                    });

                    //Week Round Chart
                    var ctxweekChart = document.getElementById("weekChart");
                    var ctxweekChart = new Chart(ctxweekChart, {
                        type: 'doughnut',
                        data: {
                            labels: ['Income', 'Expense', 'Purchase', 'Sale'],
                            datasets: [{
                                label: "Week Chart",
                                data: [{{ $weekincome }}, {{ $weekexpense }}, {{ $weekpurchase }}, {{ Helper::weekTSale() }}],
                                backgroundColor: ["rgb(100, 181, 246)", "rgb(229, 115, 115)", "rgb(255, 213, 79)", "rgb(174, 213, 129)"]
                            }]
                        },
                        options: {
                             legend: {
                                display: true
                             },
                             tooltips: {
                                enabled: true
                             }
                        }
                    });

                    //Month Round Chart
                    var ctxmonthChart = document.getElementById("monthChart");
                    var ctxmonthChart = new Chart(ctxmonthChart, {
                        type: 'doughnut',
                        data: {
                            labels: ['Income', 'Expense', 'Purchase', 'Sale'],
                            datasets: [{
                                label: "Month Chart",
                                data: [{{ $monthincome }}, {{ $monthexpense }}, {{ $monthpurchase }}, {{ Helper::monthTSale() }}],
                                backgroundColor: ["rgb(100, 181, 246)", "rgb(229, 115, 115)", "rgb(255, 213, 79)", "rgb(174, 213, 129)"]
                            }]
                        },
                        options: {
                             legend: {
                                display: true
                             },
                             tooltips: {
                                enabled: true
                             }
                        }
                    });


                    //Year Round Chart
                    var ctxyearChart = document.getElementById("yearChart");
                    var ctxyearChart = new Chart(ctxyearChart, {
                        type: 'doughnut',
                        data: {
                            labels: ['Income', 'Expense', 'Purchase', 'Sale'],
                            datasets: [{
                                label: "Year Chart",
                                data: [{{ $yearincome }}, {{ $yearexpense }}, {{ $yearpurchase }}, {{ Helper::yearTSale() }}],
                                backgroundColor: ["rgb(100, 181, 246)", "rgb(229, 115, 115)", "rgb(255, 213, 79)", "rgb(174, 213, 129)"]
                            }]
                        },
                        options: {
                             legend: {
                                display: true
                             },
                             tooltips: {
                                enabled: true
                             }
                        }
                    });


                    //Total Round Chart
                    var ctxtotalChart = document.getElementById("totalChart");
                    var ctxtotalChart = new Chart(ctxtotalChart, {
                        type: 'doughnut',
                        data: {
                            labels: ['Income', 'Expense', 'Purchase', 'Sale'],
                            datasets: [{
                                label: "Year Chart",
                                data: [{{ $balances->where('type', 'Income')->sum('amount') }}, {{ $balances->where('type', 'Expense')->sum('amount') }}, {{ $purchases->sum('total') }}, {{ Helper::tpsalesamount() }}],
                                backgroundColor: ["rgb(100, 181, 246)", "rgb(229, 115, 115)", "rgb(255, 213, 79)", "rgb(174, 213, 129)"]
                            }]
                        },
                        options: {
                             legend: {
                                display: true
                             },
                             tooltips: {
                                enabled: true
                             }
                        }
                    });

                    //Purchase Calendar
                    $('#purchaseCal').fullCalendar({
                        header: {
                        left: 'title',
                                center: 'prev,next today ',
                                right: 'month'
                        },
                        events: [
                                <?php $i = 0; $count = count($purchases); foreach ($purchases as $purchase) { $i++; ?>
                                { title  : '<?php echo $purchase->title; ?>',
                                    start  : '<?php echo date('Y-m-d', strtotime($purchase->purchasedate)); ?>',
                                    url: '<?php echo url('purchase', $purchase->id); ?>'} <?php if ($i < $count) { echo ","; } } ?>
                            ],
                        eventRender: function(event, element) {
                            $(element).tooltip({title: event.title});
                        },
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                                    return false;
                            }
                        }
                    });



                    $('#saleCal').fullCalendar({
                        header: {
                        left: 'title',
                                center: 'prev,next today ',
                                right: 'month'
                        },
                        events: [
                            <?php $i = 0; $count = count($sales); foreach ($sales as $sale) { $i++; ?>
                            { title  : '<?php echo Helper::getProductTitle($sale->product); ?>',
                                start  : '<?php echo date('Y-m-d', strtotime($sale->saledate)); ?>',
                                url: '<?php echo url('sale', $sale->id); ?>' } <?php if ($i < $count) { echo ","; } ?> <?php } ?>
                        ],
                        eventRender: function(event, element) {
                            $(element).tooltip({title: event.title});
                        },
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                                return false;
                            }
                        }
                    });


                    $('#incomeCal').fullCalendar({
                        header: {
                            left: 'title',
                            center: 'prev,next today ',
                            right: 'month'
                        },
                        events: [
                            <?php $i = 0; $count = count($incomes); foreach ($incomes as $income) { $i++; ?>
                            { title  : '<?php echo $income->title; ?>',
                                start  : '<?php echo date('Y-m-d', strtotime($income->balancedate)); ?>',
                                url: '<?php echo url('balance', $income->id); ?>' }<?php if ($i < $count) { echo ","; } } ?>
                        ],
                        eventRender: function(event, element) {
                            $(element).tooltip({title: event.title});
                        },
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                                return false;
                            }
                        }
                    });


                    $('#expenseCal').fullCalendar({
                        header: {
                            left: 'title',
                            center: 'prev,next today ',
                            right: 'month'
                        },
                        events: [
                            <?php $i = 0; $count = count($expenses); foreach ($expenses as $expense) { $i++; ?>
                            { title  : '<?php echo $expense->title; ?>',
                            start  : '<?php echo date('Y-m-d', strtotime($expense->balancedate)); ?>',
                            url: '<?php echo url('balance', $expense->id); ?>' }<?php if ($i < $count) { echo ","; } } ?>
                        ],
                        eventRender: function(event, element) {
                            $(element).tooltip({title: event.title});
                        },
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                            return false;
                            }
                        }
                    });
            });

        </script>
    <?php } ?>

    @endsection
