@extends('layouts.app')

@section('content')

<h1 class="text-center" style=" color: #dd4b39; font-weight: bold;">Please Verify Your Purchase Code</h1>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Please Insert Purchase Code Below To Active Abacus</h3>
                    <h4 class="box-title right"><a href="https://codecanyon.net/user/onezeroart/portfolio" target="_blank">Buy Now</a></h4>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['VerifyController@storePurchasecode', 1 ], 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group">
                                {{ Form::label('purchasecode', 'Purchase Code')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::text('purchasecode', '', ['class' => 'form-control', 'placeholder' => 'Please Insert Purchase Code', 'required'] )}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-offset-1">
                                <div class="input-group">
                                    {{ Form::submit('Submit', ['class' => 'btn btn-primary'] )}}
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        
    </div>
</section>

@endsection
