@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">
                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ $purchase->title }}</h3>
                    <h5 class="widget-user-desc">{{ $purchase->category }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        
                                        <?php if($purchase->photo){ ?>
                                            <img src="{{ URL::asset('images/purchase/' . $purchase->photo)}}" alt="Purchase Photo">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="Purchase Photo">
                                        <?php } ?>
                                            
                                    </div>
                                    <!-- /.widget-user-image -->
                                    
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>عنوان خرید</td>
                                                <td>{{ $purchase->title }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>تاریخ</td>
                                                <td>{{ Carbon::parse($purchase->purchasedate)->format('j M Y') }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>دسته بندی</td>
                                                <td>{{ $purchase->category }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تامین کننده</td>
                                                <td><a title="Supplier Details" href="{{ url('supplier', $purchase->supplier) }}">{{ Helper::getNameByID($purchase->supplier, 'suppliers') }}</a></td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>تعداد</td>
                                                <td>{{ $purchase->quantity }} {{ $purchase->unit }}</td>
                                            </tr>                                            
                                            <tr>
                                                <td>کارخانه</td>
                                                <td>{{ $purchase->company }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>قیمت</td>
                                                <td>(+) {{ number_format($purchase->price) }}</td>
                                            </tr>
                                            <tr>
                                                <td>کمیسیون</td>
                                                <td>(-) {{ number_format($purchase->commission) }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>قیمت کل</strong></td>
                                                <td><strong>(=) {{ number_format($purchase->total) }}</strong></td>
                                            </tr>
                                            
                                                                                   
                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $purchase->description !!}
                                </div>
                                
                                
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection