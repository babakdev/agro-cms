@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  خرید جدید</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'PurchaseController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('title', 'عنوان خرید/سفارش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'عنوان', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیحات', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>  

                    @php $arrayCatLists = array(' ' => 'انتخاب دسته بندی'); @endphp
                    @if(count($categorieslist) > 0 )                            
                    @foreach($categorieslist as $category)
                    @php                                 
                    $arrayCatLists[$category->category] = $category->category;
                    @endphp
                    @endforeach                        
                    @endif

                    @php $supplierslist = array(' ' => 'انتخاب تامین کننده'); @endphp
                    @if(count($suppliers) > 0 )                            
                    @foreach($suppliers as $supplier)
                    @php 
                    $supplierslist[$supplier->id] = $supplier->fname;
                    @endphp
                    @endforeach                        
                    @endif  
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('purchasedate', 'تاریخ سفارش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('purchasedate', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ سفارش', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('', 'دسته بندی خرید')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('category', $arrayCatLists, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('supplier', 'تامین کننده')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('unit', 'واحد شمارش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('unit', '', ['class' => 'form-control', 'placeholder' => 'عدد/ست/دستگاه/کارتن', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('quantity', 'تعداد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('quantity', '', ['class' => 'form-control quantity', 'placeholder' => 'تعداد', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('company', 'نام کارخانه')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('company', '', ['class' => 'form-control', 'placeholder' => 'نام کارخانه'] )}}
                            </div>
                        </div>
                    </div>                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('price', 'قیمت اختصاصی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('price', '', ['class' => 'form-control price', 'placeholder' => 'قیمت اختصاصی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('commission', 'کمیسیون')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('commission', '', ['class' => 'form-control commission', 'placeholder' => 'کمیسیون'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('total', 'قیمت کل')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('total', '', ['class' => 'form-control total', 'placeholder' => 'قیمت کل', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>                        



                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

