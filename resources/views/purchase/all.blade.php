@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه سفارشات/خریدها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtPurchase text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>عنوان</th>
                                <th>دسته بندی</th>
                                <th>تامین کننده</th>
                                <th>تعداد</th>
                                <th>قیمت</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($purchases) > 0 )

                            @foreach($purchases as $purchase)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($purchase->purchasedate)->format('j M Y') }}</td>
                                    <td>{{ $purchase->title }}</td>
                                    <td>{{ $purchase->category }}</td>
                                    <td><a title="Supplier Details" href="{{ url('supplier', $purchase->supplier) }}">{{ Helper::getNameByID($purchase->supplier, 'suppliers') }}</a></td>
                                    <td>{{ number_format($purchase->quantity) }}</td>
                                    <td>{{ number_format($purchase->total) }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('purchase', $purchase->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('purchase', $purchase->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('purchase.destroy', $purchase->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
