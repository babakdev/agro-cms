@extends('layouts.app')
@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> حساب بانکی جدید</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'TransactionController@addbankstore', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('name', 'نام')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('account', 'شماره حساب')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::text('account', '', ['class' => 'form-control', 'placeholder' => 'شماره حساب', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('branch', 'شعبه')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::text('branch', '', ['class' => 'form-control', 'placeholder' => 'شعبه', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('swift', 'شبا')}}
                            <div class="input-group">
                                {{ Form::text('swift', '', ['class' => 'form-control', 'placeholder' => 'شبا'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('address', 'آدرس')}}
                            <div class="input-group">
                                {{ Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'آدرس'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">حسابهای بانکی</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtBankAcc text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام</th>
                                <th>شماره حساب</th>
                                <th>شعبه</th>
                                <th>شبا</th>
                                <th>آدرس</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($bankaccounts) > 0 )

                            @foreach($bankaccounts as $account)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $account->name }}</td>
                                    <td>{{ $account->account }}</td>
                                    <td>{{ $account->branch }}</td>
                                    <td>{{ $account->swift }}</td>
                                    <td>{{ $account->address }}</td>
                                    <td>
                                        <form action="{{ route('transaction.destroyaccount', $account->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>


@endsection
