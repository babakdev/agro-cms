@extends('layouts.app')
@section('content')

@php $bankaccountslist = array(' ' => 'حساب بانکی خود را اضافه نمایید'); @endphp
@if(count($bankaccounts) > 0 )                            
@foreach($bankaccounts as $account)
@php 
$bankaccountslist[$account->id] = $account->name . " (" . $account->account .  ")" ;
@endphp
@endforeach                        
@endif   


<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> تراکنش جدید</h3>                    
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'TransactionController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('date', 'تاریخ تراکنش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('date', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ تراکنش', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('name', 'انتخاب بانک و حساب')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('name', $bankaccountslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('type', 'ماهیت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('type', ['' => 'ماهیت تراکنش را انتخاب نمایید', 'Increase' => 'افزایشی', 'Decrease' => 'کاهشی'], null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('amount', 'مبلغ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::number('amount', '', ['class' => 'form-control', 'placeholder' => 'مبلغ را وارد نمایید', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('note', 'توضیحات')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('note', '', ['class' => 'form-control', 'placeholder' => 'توضیحات'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>  
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

