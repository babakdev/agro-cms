@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> تراکنش ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtBank text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>حساب بانکی</th>
                                <th>مبلغ</th>
                                <th>توضیح</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($transactions) > 0 )

                            @foreach($transactions as $transaction)

                                <tr @if( $transaction->type == "Decrease") style="background:#FFF9C4 !important" @endif>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $transaction->date }}</td>
                                    <td>{{ Helper::getBank($transaction->name)->name . " (" . Helper::getBank($transaction->name)->account . ")" }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($transaction->amount, 2) }}</td>
                                    <td>{{ $transaction->note }}</td>
                                    <td>
                                        <span class="badge bg-yellow"><a href="{{ url('transaction', $transaction->id)}}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('transaction.destroy', $transaction->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
