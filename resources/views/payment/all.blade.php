@extends('layouts.app')
@section('content')

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">پرداخت ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtPayment text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>شماره فاکتور #</th>
                                <th>نحوه پرداخت</th>
                                <th>پیگیری پرداخت</th>
                                <th>مبلغ</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($payments) > 0 )

                            @foreach($payments as $payment)

                                <tr @if( $payment->type == "هزینه") style="background:#FFF9C4 !important" @endif>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $payment->invoiceid }}</td>
                                    <td>{{ ucfirst($payment->paymentmethod) }}</td>
                                    <td>{{ $payment->paymentid }}</td>
                                    <td>{{ number_format($payment->amount, 2) }}</td>
                                    <td>
                                        <form action="{{ route('payment.destroy', $payment->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
