@extends('layouts.app')
@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ number_format($sales->count(), 2) }}</h3>
              <p>فروش ها</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ number_format(Helper::tpsales(), 2) }}</h3>
              <p>تعداد محصول فروش رفته</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ number_format(Helper::tpsalesamount()) }}</h3>
              <p>مبلغ کل فروش ها</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">فروش ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtSale text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>نام محصول</th>
                                <th>بازرگان</th>
                                <th>تعداد</th>
                                <th>مبلغ کل</th>
                                <th>پرداخت شده</th>
                                <th>ناشی از</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($sales) > 0 )

                            @foreach($sales as $sale)

                            @php
                            $totalSubtotal = array();
                            $productIDs = $sale->product; //Product IDs
                            $productQtys = $sale->quantity; //Product Quantity
                            $productPrices = $sale->price; //Product price

                            //Product IDs
                            $productArr = explode(",", $productIDs);
                            foreach ($productArr as $prokey => $provalue){
                                if ($provalue == "") {
                                    unset($productArr[$prokey]);
                                }
                            }

                            //Product Quantity
                            $productQtyArr = explode(",", $productQtys);
                            foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                                if ($proQtyvalue == "") {
                                    unset($productQtyArr[$proQtykey]);
                                }
                            }

                            //Product Price
                            $productPriceArr = explode(",", $productPrices);
                            foreach ($productPriceArr as $proPrikey => $proPrivalue){
                                if ($proPrivalue == "") {
                                    unset($productPriceArr[$proPrikey]);
                                }
                            }

                            foreach ($productArr as $productkey => $product){
                                $totalSubtotal[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                            }

                            $totalSubtotal = array_sum($totalSubtotal);


                            @endphp

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($sale->saledate)->format('j M Y') }}</td>
                                    <td>

                                            <?php for($sq=0; count($productArr) > $sq; $sq++){
                                                if(!empty($productArr[$sq])){ ?>
                                                    <a title="جزییات محصول" href="{{ url('product', $productArr[$sq]) }}">{{ Helper::getProductTitle( $productArr[$sq] ) }}</a><br>
                                                <?php }
                                                if($sq === 1){ ?>
                                                    <a title="نمایش جزییات" href="{{ url('sale', $sale->id) }}">...</a><br>
                                                <?php break;
                                                }
                                            } ?>

                                    </td>
                                    <td><a title="Merchant Details" href="{{ url('merchant', $sale->merchant) }}">{{ Helper::getNameByID($sale->merchant, 'merchants') }}</a></td>
                                    <td>{{ number_format(array_sum($productQtyArr), 2) }}</td>
                                    <td><strong>{{ number_format($totalSubtotal - $sale->commission, 2)  }}</strong></td>

                                    <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                        <td><strong> <?php if( Helper::invoiceBySale($sale->id)){ echo number_format(Helper::invoiceBySale($sale->id)->paidamount); } ?></strong></td>
                                    <?php if( Helper::invoiceBySale($sale->id)->paidamount < ($totalSubtotal - $sale->commission) ){ ?>
                                        <td><strong> {{ number_format( Helper::invoiceBySale($sale->id)->paidamount - ($totalSubtotal - $sale->commission)) }}</strong></td>
                                    <?php }else{ ?>
                                        <td>N/A</td>
                                    <?php } }else{ ?>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                    <?php } ?>

                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('sale', $sale->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('sale', $sale->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('sale.destroy', $sale->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
