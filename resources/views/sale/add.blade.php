@extends('layouts.app')

@section('content')

@php $arrayCatLists = array(' ' => 'انتخاب دسته بندی'); @endphp
@if(count($categorieslist) > 0 )
@foreach($categorieslist as $category)
@php
$arrayCatLists[$category->category] = $category->category;
@endphp
@endforeach
@endif

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )
@foreach($merchants as $merchant)
@php
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach
@endif

@php $productslist = array('' => 'انتخاب محصول'); @endphp
@if(count($products) > 0 )
@foreach($products as $product)
@php
$productslist[$product->id] = $product->title;
@endphp
@endforeach
@endif

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  فروش جدید</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'SaleController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product1', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product1', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity1', ' تعداد ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::number('quantity1', '', ['class' => 'form-control', 'placeholder' => 'تعداد', 'required'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price1', ' قیمت ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::number('price1', '', ['class' => 'form-control', 'placeholder' => 'قیمت', 'required'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total1', ' قیمت کل ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::number('total1', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل', 'required'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product2', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product2', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity2', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity2', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price2', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price2', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total2', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total2', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product3', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product3', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity3', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity3', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price3', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price3', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total3', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total3', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product4', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product4', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity4', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity4', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price4', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price4', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total4', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total4', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product5', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product5', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity5', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity5', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price5', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price5', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total5', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total5', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product6', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product6', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity6', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity6', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price6', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price6', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total6', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total6', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product7', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product7', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity7', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity7', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price7', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price7', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total7', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total7', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product8', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product8', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity8', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity8', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price8', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price8', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total8', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total8', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product9', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product9', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity9', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity9', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price9', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price9', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total9', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total9', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product10', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product10', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity10', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity10', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price10', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price10', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total10', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total10', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product11', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product11', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity11', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity11', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price11', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price11', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total11', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total11', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product12', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product12', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity12', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity12', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price12', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price12', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total12', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total12', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product13', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product13', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity13', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity13', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price13', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price13', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total13', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total13', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product14', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product14', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity14', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity14', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price14', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price14', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total14', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total14', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product15', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product15', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity15', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity15', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price15', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price15', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total15', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total15', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product16', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product16', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity16', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity16', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price16', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price16', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total16', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total16', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product17', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product17', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity17', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity17', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price17', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price17', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total17', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total17', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product18', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product18', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity18', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity18', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price18', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price18', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total18', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total18', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product19', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product19', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity19', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity19', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price19', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price19', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total19', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total19', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product20', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product20', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity20', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity20', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price20', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price20', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total20', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total20', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product21', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product21', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity21', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity21', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price21', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price21', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total21', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total21', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product22', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product22', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity22', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity22', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price22', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price22', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total22', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total22', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product23', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product23', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity23', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity23', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price23', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price23', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total23', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total23', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product24', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product24', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity24', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity24', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price24', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price24', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total24', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total24', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product25', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product25', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity25', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity25', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price25', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price25', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total25', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total25', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product26', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product26', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity26', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity26', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price26', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price26', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total26', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total26', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product27', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product27', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity27', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity27', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price27', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price27', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total27', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total27', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product28', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product28', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity28', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity28', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price28', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price28', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total28', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total28', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product29', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product29', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity29', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity29', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price29', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price29', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total29', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total29', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product30', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product30', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity30', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity30', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price30', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price30', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total30', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total30', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product31', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product31', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity31', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity31', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price31', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price31', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total31', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total31', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product32', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product32', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity32', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity32', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price32', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price32', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total32', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total32', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product33', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product33', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity33', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity33', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price33', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price33', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total33', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total33', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product34', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product34', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity34', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity34', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price34', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price34', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total34', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total34', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product35', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product35', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity35', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity35', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price35', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price35', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total35', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total35', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product36', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product36', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity36', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity36', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price36', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price36', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total36', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total36', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product37', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product37', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity37', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity37', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price37', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price37', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total37', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total37', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product38', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product38', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity38', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity38', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price38', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price38', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total38', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total38', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product39', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product39', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity39', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity39', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price39', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price39', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total39', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total39', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product40', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product40', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity40', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity40', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price40', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price40', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total40', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total40', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product41', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product41', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity41', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity41', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price41', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price41', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total41', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total41', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product42', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product42', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity42', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity42', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price42', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price42', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total42', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total42', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product43', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product43', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity43', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity43', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price43', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price43', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total43', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total43', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product44', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product44', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity44', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity44', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price44', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price44', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total44', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total44', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product45', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product45', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity45', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity45', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price45', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price45', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total45', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total45', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product46', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product46', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity46', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity46', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price46', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price46', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total46', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total46', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product47', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product47', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity47', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity47', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price47', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price47', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total47', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total47', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product48', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product48', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity48', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity48', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price48', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price48', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total48', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total48', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product49', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product49', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity49', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity49', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price49', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price49', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total49', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total49', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product50', ' محصول')}}
                                <div class="input-group">
                                    {{ Form::select('product50', $productslist, null, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity50', ' تعداد ')}}
                                <div class="input-group">
                                    {{ Form::number('quantity50', '', ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price50', ' قیمت ')}}
                                <div class="input-group">
                                    {{ Form::number('price50', '', ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total50', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total50', '', ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                         <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div> 
                    </div>

                     <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('product', 'انتخاب محصول')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                <select name="product" class="form-control product" required>
                                    <option value="" price="">انتخاب محصول</option>
                                    <?php if(count($products) > 0 ){ foreach($products as $product){ ?>
                                        <?php $productTotal = $product->price + $product->cost1 + $product->cost2 + $product->cost3 + $product->cost4 + $product->cost5 + $product->profit; ?>
                                        <option value="<?php echo $product->id; ?>" price="<?php echo $productTotal; ?>"><?php echo $product->title . " (" . $productTotal  . ")"; ?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیحات', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('saledate', 'تاریخ فروش/تحویل')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::text('saledate', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ فروش', 'required'] )}}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('merchant', 'انتخاب بازرگان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::select('merchant', $merchantslist, null, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('quantity', 'تعداد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::number('quantity', '', ['class' => 'form-control quantity', 'placeholder' => 'تعداد', 'required'] )}}
                            </div>
                        </div>
                    </div> -->

                     <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('price', 'قیمت اختصاصی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::number('price', '', ['class' => 'form-control price', 'placeholder' => 'قیمت اختصاصی', 'required'] )}}
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('commission', 'کمیسیون فروش')}}
                            <div class="input-group">
                                {{ Form::number('commission', '', ['class' => 'form-control commission', 'placeholder' => 'کمیسیون فروش'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('total', 'قیمت کل ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::number('total', '', ['class' => 'form-control total', 'placeholder' => 'قیمت کل', 'required'] )}}
                            </div>
                        </div>
                    </div> 

                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
