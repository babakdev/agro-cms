@extends('layouts.app')
@section('content')
<?php

$totalSubtotal = array();
$totalProfit = array();
$productIDs = $sale->product; //Product IDs
$productQtys = $sale->quantity; //Product Quantity
$productPrices = $sale->price; //Product price

//Product IDs
$productArr = explode(",", $productIDs);
foreach ($productArr as $prokey => $provalue){
    if ($provalue == "") {
        unset($productArr[$prokey]);
    }
}

//Product Quantity
$productQtyArr = explode(",", $productQtys);
foreach ($productQtyArr as $proQtykey => $proQtyvalue){
    if ($proQtyvalue == "") {
        unset($productQtyArr[$proQtykey]);
    }
}

//Product Price
$productPriceArr = explode(",", $productPrices);
foreach ($productPriceArr as $proPrikey => $proPrivalue){
    if ($proPrivalue == "") {
        unset($productPriceArr[$proPrikey]);
    }
}

foreach ($productArr as $productkey => $product){
    $totalSubtotal[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
    if(!empty($product)){
        if(Helper::getProduct($product) != false){
            $totalProfit[] = Helper::getProduct($product)->profit;
        }
    }
}


?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="box-header with-border">
                    <h3 class="box-title left">
                        <?php $totalProducts = array();
                            for($sq=0; count($productArr) > $sq; $sq++){
                                if(!empty($productArr[$sq])){
                                    $totalProducts[] = $productArr[$sq];
                                }
                            } echo count($totalProducts) . " محصولات"; ?>
                    </h3>
                    <h3 class="box-title left">
                        <?php
                            if(!empty($invoice)){ ?>
                                <a href="{{ url('invoice', $invoice) }}" target="_blank" class="btn btn-primary float-left"><i class="material-icons md-18">receipt</i> مشاهده فاکتور</a>
                            <?php }else{ ?>
                                <a href="#" class="btn btn-primary float-left"><i class="material-icons md-18">receipt</i> فاکتورهای ناموجود</a>
                        <?php } ?>
                    </h3>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>محصول</td>
                                                <td>
                                                    <?php for($sq=0; count($productArr) > $sq; $sq++){
                                                        if(!empty($productArr[$sq])){ ?>
                                                            <a title="جزییات محصول" href="{{ url('product', $productArr[$sq]) }}">{{ Helper::getProductTitle( $productArr[$sq] ) }}</a><br>
                                                        <?php }
                                                    } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>تاریخ فروش</td>
                                                <td>{{ Carbon::parse($sale->saledate)->format('j M Y') }}</td>
                                            </tr>
                                            <tr>
                                                <td>بازرگان</td>
                                                <td><a title="مشخصات بازرگان" href="{{ url('merchant', $sale->merchant) }}">{{ Helper::getNameByID($sale->merchant, 'merchants') }}</a></td>
                                            </tr>
                                            <tr>
                                                <td>تعداد</td>
                                                <td>{{ number_format(array_sum($productQtyArr)) }}</td>
                                            </tr>
                                            <tr>
                                                <td>کارخانه</td>
                                                <td>{{ $sale->company }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>قیمت کل</strong></td>
                                                <td><strong> {{ number_format(array_sum($totalSubtotal)) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td><strong>کمیسیون</strong></td>
                                                <td><strong>(-) {{ number_format($sale->commission) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td><strong>خالص کل</strong></td>
                                                <td><strong> (=) {{ number_format(array_sum($totalSubtotal) - $sale->commission) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td><strong><span class="badge bg-green">مبلغ پرداختی</span></strong></td>
                                                <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                                    <td><strong> <?php if( Helper::invoiceBySale($sale->id)){ echo number_format(Helper::invoiceBySale($sale->id)->paidamount); } ?></strong></td>
                                                <?php }else{ ?>
                                                    <td>N/A</td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td><strong><span class="badge bg-red">ناشی از</span></strong></td>
                                                <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                                    <?php if( Helper::invoiceBySale($sale->id)->paidamount < (array_sum($totalSubtotal) - $sale->commission) ){ ?>
                                                        <td><strong> <?php echo number_format( Helper::invoiceBySale($sale->id)->paidamount - (array_sum($totalSubtotal) - $sale->commission) ) ?></strong></td>
                                                    <?php }?>
                                                <?php }else{ ?>
                                                    <td>N/A</td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td><strong><span class="badge bg-default">سود ناخالص</span></strong></td>
                                                <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                                        <td><strong> <?php echo number_format(array_sum($totalProfit)); ?></strong></td>
                                                <?php }else{ ?>
                                                    <td>N/A</td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td><strong><span class="badge bg-red">زیان</span></strong></td>
                                                <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                                    <?php if( Helper::invoiceBySale($sale->id)->paidamount < (array_sum($totalSubtotal) - $sale->commission) ){ ?>
                                                        <td><strong> <?php echo number_format( Helper::invoiceBySale($sale->id)->paidamount - ((array_sum($totalSubtotal) - $sale->commission) - array_sum($totalProfit)) ) ?></strong></td>
                                                    <?php }?>
                                                <?php }else{ ?>
                                                    <td>0</td>
                                                <?php } ?>
                                            </tr>

                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $sale->description !!}
                                </div>


                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection
