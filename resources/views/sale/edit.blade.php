@extends('layouts.app')

@section('content')

@php $arrayCatLists = array(' ' => 'انتخاب دسته بندی'); @endphp
@if(count($categorieslist) > 0 )
@foreach($categorieslist as $category)
@php
$arrayCatLists[$category->category] = $category->category;
@endphp
@endforeach
@endif

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )
@foreach($merchants as $merchant)
@php
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach
@endif

@php $productslist = array('' => 'انتخاب محصول'); @endphp
@if(count($products) > 0 )
@foreach($products as $product)
@php
$productslist[$product->id] = $product->title;
@endphp
@endforeach
@endif

@php
$productIDs = $sale->product; //محصول IDs
$productQtys = $sale->quantity; //محصول تعداد
$productPrices = $sale->price; //محصول price

//محصول IDs
$productArr = explode(",", $productIDs);

//محصول تعداد
$productQtyArr = explode(",", $productQtys);

//محصول قیمت
$productPriceArr = explode(",", $productPrices);

@endphp

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  ویرایش فروش</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['SaleController@update', $sale->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}

                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product1', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(0, $productArr)) ? $product1 = $productArr[0] : $product1 = "";?>
                                    {{ Form::select('product1', $productslist, $product1, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity1', ' تعداد ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    <?php (array_key_exists(0, $productQtyArr)) ? $quantity1 = $productQtyArr[0] : $quantity1 = "";?>
                                    {{ Form::number('quantity1', $quantity1, ['class' => 'form-control', 'placeholder' => 'تعداد', 'required'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price1', ' قیمت ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    <?php (array_key_exists(0, $productPriceArr)) ? $price1 = $productPriceArr[0] : $price1= "";?>
                                    {{ Form::number('price1', $price1, ['class' => 'form-control', 'placeholder' => 'قیمت', 'required'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total1', ' قیمت کل ')}} <span class="text-red"> (*)</span>
                                <div class="input-group">
                                    {{ Form::number('total1', $price1 * $quantity1, ['class' => 'form-control', 'placeholder' => 'قیمت کل', 'required'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product2', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(1, $productArr)) ? $product2 = $productArr[1] : $product2 = "";?>
                                    {{ Form::select('product2', $productslist, $product2, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity2', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(1, $productQtyArr)) ? $quantity2 = $productQtyArr[1] : $quantity2 = "";?>
                                    {{ Form::number('quantity2', $quantity2, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price2', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(1, $productPriceArr)) ? $price2 = $productPriceArr[1] : $price2 = "";?>
                                    {{ Form::number('price2', $price2, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total2', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total2', $price2 * $quantity2, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product3', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(2, $productArr)) ? $product3 = $productArr[2] : $product3 = "";?>
                                    {{ Form::select('product3', $productslist, $product3, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity3', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(2, $productQtyArr)) ? $quantity3 = $productQtyArr[2] : $quantity3 = "";?>
                                    {{ Form::number('quantity3', $quantity3, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price3', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(2, $productPriceArr)) ? $price3 = $productPriceArr[2] : $price3 = "";?>
                                    {{ Form::number('price3', $price3, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total3', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total3', $price3 * $quantity3, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product4', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(3, $productArr)) ? $product4 = $productArr[3] : $product4 = "";?>
                                    {{ Form::select('product4', $productslist, $product4, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity4', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(3, $productQtyArr)) ? $quantity4 = $productQtyArr[3] : $quantity4 = "";?>
                                    {{ Form::number('quantity4', $quantity4, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price4', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(3, $productPriceArr)) ? $price4 = $productPriceArr[3] : $price4 = "";?>
                                    {{ Form::number('price4', $price4, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total4', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total4', (int)$price4 * (int)$quantity4, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product5', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(4, $productArr)) ? $product5 = $productArr[4] : $product5 = "";?>
                                    {{ Form::select('product5', $productslist, $product5, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity5', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(4, $productQtyArr)) ? $quantity5 = $productQtyArr[4] : $quantity5 = "";?>
                                    {{ Form::number('quantity5', $quantity5, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price5', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(4, $productPriceArr)) ? $price5 = $productPriceArr[4] : $price5 = "";?>
                                    {{ Form::number('price5', $price5, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total5', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total5', (int)$price5 * (int)$quantity5, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product6', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(5, $productArr)) ? $product6 = $productArr[5] : $product6 = "";?>
                                    {{ Form::select('product6', $productslist, $product6, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity6', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(5, $productQtyArr)) ? $quantity6 = $productQtyArr[5] : $quantity6 = "";?>
                                    {{ Form::number('quantity6', $quantity6, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price6', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(5, $productPriceArr)) ? $price6 = $productPriceArr[5] : $price6 = "";?>
                                    {{ Form::number('price6', $price6, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total6', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total6', (int)$price6 * (int)$quantity6, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product7', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(6, $productArr)) ? $product7 = $productArr[6] : $product7 = "";?>
                                    {{ Form::select('product7', $productslist, $product7, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity7', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(6, $productQtyArr)) ? $quantity7 = $productQtyArr[6] : $quantity7 = "";?>
                                    {{ Form::number('quantity7', $quantity7, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price7', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(6, $productPriceArr)) ? $price7 = $productPriceArr[6] : $price7 = "";?>
                                    {{ Form::number('price7', $price7, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total7', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total7', (int)$price7 * (int)$quantity7, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product8', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(7, $productArr)) ? $product8 = $productArr[7] : $product8 = "";?>
                                    {{ Form::select('product8', $productslist, $product8, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity8', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(7, $productQtyArr)) ? $quantity8 = $productQtyArr[7] : $quantity8 = "";?>
                                    {{ Form::number('quantity8', $quantity8, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price8', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(7, $productPriceArr)) ? $price8 = $productPriceArr[7] : $price8 = "";?>
                                    {{ Form::number('price8', $price8, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total8', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total8', (int)$price8 * (int)$quantity8, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product9', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(8, $productArr)) ? $product9 = $productArr[8] : $product9 = "";?>
                                    {{ Form::select('product9', $productslist, $product9, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity9', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(8, $productQtyArr)) ? $quantity9 = $productQtyArr[8] : $quantity9 = "";?>
                                    {{ Form::number('quantity9', $quantity9, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price9', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(8, $productPriceArr)) ? $price9 = $productPriceArr[8] : $price9 = "";?>
                                    {{ Form::number('price9', $price9, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total9', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total9', (int)$price9 * (int)$quantity9, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product10', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(9, $productArr)) ? $product10 = $productArr[9] : $product10 = "";?>
                                    {{ Form::select('product10', $productslist, $product10, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity10', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(9, $productQtyArr)) ? $quantity10 = $productQtyArr[9] : $quantity10  = "";?>
                                    {{ Form::number('quantity10', $quantity10, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price10', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(9, $productPriceArr)) ? $price10 = $productPriceArr[9] : $price10 = "";?>
                                    {{ Form::number('price10', $price10, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total10', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total10', (int)$price10 * (int)$quantity10, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product11', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(10, $productArr)) ? $product11 = $productArr[10] : $product11 = "";?>
                                    {{ Form::select('product11', $productslist, $product11, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity11', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(10, $productQtyArr)) ? $quantity11 = $productQtyArr[10] : $quantity11 = "";?>
                                    {{ Form::number('quantity11', $quantity11, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price11', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(10, $productPriceArr)) ? $price11 = $productPriceArr[10] : $price11 = "";?>
                                    {{ Form::number('price11', $price11, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total11', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total11', (int)$price11 * (int)$quantity11, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product12', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(11, $productArr)) ? $product12 = $productArr[11] : $product12 = "";?>
                                    {{ Form::select('product12', $productslist, $product12, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity12', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(11, $productQtyArr)) ? $quantity12 = $productQtyArr[11] : $quantity12 = "";?>
                                    {{ Form::number('quantity12', $quantity12, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price12', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(11, $productPriceArr)) ? $price12 = $productPriceArr[11] : $price12 = "";?>
                                    {{ Form::number('price12', $price12, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total12', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total12', (int)$price12 * (int)$quantity12, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product13', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(12, $productArr)) ? $product13 = $productArr[12] : $product13 = "";?>
                                    {{ Form::select('product13', $productslist, $product13, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity13', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(12, $productQtyArr)) ? $quantity13 = $productQtyArr[12] : $quantity13 = "";?>
                                    {{ Form::number('quantity13', $quantity13, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price13', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(12, $productPriceArr)) ? $price13 = $productPriceArr[12] : $price13 = "";?>
                                    {{ Form::number('price13', $price13, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total13', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total13', (int)$price13 * (int)$quantity13, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product14', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(13, $productArr)) ? $product14 = $productArr[13] : $product14 = "";?>
                                    {{ Form::select('product14', $productslist, $product14, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity14', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(13, $productQtyArr)) ? $quantity14 = $productQtyArr[13] : $quantity14 = "";?>
                                    {{ Form::number('quantity14', $quantity14, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price14', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(13, $productPriceArr)) ? $price14 = $productPriceArr[13] : $price14 = "";?>
                                    {{ Form::number('price14', $price14, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total14', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total14', (int)$price14 * (int)$quantity14, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product15', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(14, $productArr)) ? $product15 = $productArr[14] : $product15 = "";?>
                                    {{ Form::select('product15', $productslist, $product15, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity15', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(14, $productQtyArr)) ? $quantity15 = $productQtyArr[14] : $quantity15 = "";?>
                                    {{ Form::number('quantity15', $quantity15, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price15', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(14, $productPriceArr)) ? $price15 = $productPriceArr[14] : $price15 = "";?>
                                    {{ Form::number('price15', $price15, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total15', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total15', (int)$price15 * (int)$quantity15, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product16', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(15, $productArr)) ? $product16 = $productArr[15] : $product16 = "";?>
                                    {{ Form::select('product16', $productslist,  $product16, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity16', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(15, $productQtyArr)) ? $quantity16 = $productQtyArr[15] : $quantity16 = "";?>
                                    {{ Form::number('quantity16', $quantity16, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price16', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(15, $productPriceArr)) ? $price16 = $productPriceArr[15] : $price16 = "";?>
                                    {{ Form::number('price16', $price16, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total16', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total16', (int)$price16 * (int)$quantity16, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product17', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(16, $productArr)) ? $product17 = $productArr[16] : $product17 = "";?>
                                    {{ Form::select('product17', $productslist, $product17, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity17', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(16, $productQtyArr)) ? $quantity17 = $productQtyArr[16] : $quantity17 = "";?>
                                    {{ Form::number('quantity17', $quantity17, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price17', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(16, $productPriceArr)) ? $price17 = $productPriceArr[16] : $price17 = "";?>
                                    {{ Form::number('price17', $price17, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total17', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total17', (int)$price17 * (int)$quantity17, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product18', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(17, $productArr)) ? $product18 = $productArr[17] : $product18 = "";?>
                                    {{ Form::select('product18', $productslist, $product18, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity18', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(17, $productQtyArr)) ? $quantity18 = $productQtyArr[17] : $quantity18 = "";?>
                                    {{ Form::number('quantity18', $quantity18, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price18', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(17, $productPriceArr)) ? $price18 = $productPriceArr[17] : $price18 = "";?>
                                    {{ Form::number('price18', $price18, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total18', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total18', (int)$price18 * (int)$quantity18, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product19', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(18, $productArr)) ? $product19 = $productArr[18] : $product19 = "";?>
                                    {{ Form::select('product19', $productslist, $product19, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity19', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(18, $productQtyArr)) ? $quantity19 = $productQtyArr[18] : $quantity19 = "";?>
                                    {{ Form::number('quantity19', $quantity19, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price19', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(18, $productPriceArr)) ? $price19 = $productPriceArr[18] : $price19 = "";?>
                                    {{ Form::number('price19', $price19, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total19', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total19', (int)$price19 * (int)$quantity19, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product20', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(19, $productArr)) ? $product20 = $productArr[19] : $product20 = "";?>
                                    {{ Form::select('product20', $productslist, $product20, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity20', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(19, $productQtyArr)) ? $quantity20 = $productQtyArr[19] : $quantity20 = "";?>
                                    {{ Form::number('quantity20', $quantity20, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price20', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(19, $productPriceArr)) ? $price20 = $productPriceArr[19] : $price20 = "";?>
                                    {{ Form::number('price20', $price20, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total20', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total20', (int)$price20 * (int)$quantity20, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product21', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(20, $productArr)) ? $product21 = $productArr[20] : $product21 = "";?>
                                    {{ Form::select('product21', $productslist, $product21, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity21', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(20, $productQtyArr)) ? $quantity21 = $productQtyArr[20] : $quantity21 = "";?>
                                    {{ Form::number('quantity21', $quantity21, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price21', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(20, $productPriceArr)) ? $price21 = $productPriceArr[20] : $price21 = "";?>
                                    {{ Form::number('price21', $price21, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total21', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total21', (int)$price21 * (int)$quantity21, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product22', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(21, $productArr)) ? $product22 = $productArr[21] : $product22 = "";?>
                                    {{ Form::select('product22', $productslist, $product22, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity22', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(21, $productQtyArr)) ? $quantity22 = $productQtyArr[21] : $quantity22 = "";?>
                                    {{ Form::number('quantity22', $quantity22, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price22', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(21, $productPriceArr)) ? $price22 = $productPriceArr[21] : $price22 = "";?>
                                    {{ Form::number('price22', $price22, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total22', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total22', (int)$price22 * (int)$quantity22, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product23', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(22, $productArr)) ? $product23 = $productArr[22] : $product23 = "";?>
                                    {{ Form::select('product23', $productslist, $product23, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity23', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(22, $productQtyArr)) ? $quantity23 = $productQtyArr[22] : $quantity23 = "";?>
                                    {{ Form::number('quantity23', $quantity23, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price23', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(22, $productPriceArr)) ? $price23 = $productPriceArr[22] : $price23 = "";?>
                                    {{ Form::number('price23', $price23, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total23', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total23', (int)$price23 * (int)$quantity23, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product24', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(23, $productArr)) ? $product24 = $productArr[23] : $product24 = "";?>
                                    {{ Form::select('product24', $productslist, $product24, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity24', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(23, $productQtyArr)) ? $quantity24 = $productQtyArr[23] : $quantity24 = "";?>
                                    {{ Form::number('quantity24', $quantity24, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price24', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(23, $productPriceArr)) ? $price24 = $productPriceArr[23] : $price24 = "";?>
                                    {{ Form::number('price24', $price24, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total24', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total24', (int)$price24 * (int)$quantity24, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product25', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(24, $productArr)) ? $product25 = $productArr[24] : $product25 = "";?>
                                    {{ Form::select('product25', $productslist, $product25, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity25', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(24, $productQtyArr)) ? $quantity25 = $productQtyArr[24] : $quantity25 = "";?>
                                    {{ Form::number('quantity25', $quantity25, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price25', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(24, $productPriceArr)) ? $price25 = $productPriceArr[24] : $price25 = "";?>
                                    {{ Form::number('price25', $price25, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total25', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total25', (int)$price25 * (int)$quantity25, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product26', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(25, $productArr)) ? $product26 = $productArr[25] : $product26 = "";?>
                                    {{ Form::select('product26', $productslist, $product26, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity26', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(25, $productQtyArr)) ? $quantity26 = $productQtyArr[25] : $quantity26 = "";?>
                                    {{ Form::number('quantity26', $quantity26, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price26', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(25, $productPriceArr)) ? $price26 = $productPriceArr[25] : $price26 = "";?>
                                    {{ Form::number('price26', $price26, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total26', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total26', (int)$price26 * (int)$quantity26, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product27', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(26, $productArr)) ? $product27 = $productArr[26] : $product27 = "";?>
                                    {{ Form::select('product27', $productslist, $product27, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity27', ' تعداد ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(26, $productQtyArr)) ? $quantity27 = $productQtyArr[26] : $quantity27 = "";?>
                                    {{ Form::number('quantity27', $quantity27, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price27', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(26, $productPriceArr)) ? $price27 = $productPriceArr[26] : $price27 = "";?>
                                    {{ Form::number('price27', $price27, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total27', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total27', (int)$price27 * (int)$quantity27, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product28', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(27, $productArr)) ? $product28 = $productArr[27] : $product28 = "";?>
                                    {{ Form::select('product28', $productslist, $product28, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity28', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(27, $productQtyArr)) ? $quantity28 = $productQtyArr[27] : $quantity28 = "";?>
                                    {{ Form::number('quantity28',$quantity28, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price28', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(27, $productPriceArr)) ? $price28 = $productPriceArr[27] : $price28 = "";?>
                                    {{ Form::number('price28', $price28, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total28', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total28', (int)$price28 * (int)$quantity28, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product29', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(28, $productArr)) ? $product29 = $productArr[28] : $product29 = "";?>
                                    {{ Form::select('product29', $productslist, $product29, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity29', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(28, $productQtyArr)) ? $quantity29 = $productQtyArr[28] : $quantity29 = "";?>
                                    {{ Form::number('quantity29', $quantity29, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price29', ' قیمت ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(28, $productPriceArr)) ? $price29 = $productPriceArr[28] : $price29 = "";?>
                                    {{ Form::number('price29', $price29, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total29', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total29', (int)$price29 * (int)$quantity29, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product30', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(29, $productArr)) ? $product30 = $productArr[29] : $product30 = "";?>
                                    {{ Form::select('product30', $productslist, $product30, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity30', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(29, $productQtyArr)) ? $quantity30 = $productQtyArr[29] : $quantity30 = "";?>
                                    {{ Form::number('quantity30', $quantity30, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price30', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(29, $productPriceArr)) ? $price30 = $productPriceArr[29] : $price30 = "";?>
                                    {{ Form::number('price30', $price30, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total30', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total30', (int)$price30 * (int)$quantity30, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product31', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(30, $productArr)) ? $product31 = $productArr[30] : $product31 = "";?>
                                    {{ Form::select('product31', $productslist, $product31, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity31', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(30, $productQtyArr)) ? $quantity31 = $productQtyArr[30] : $quantity31 = "";?>
                                    {{ Form::number('quantity31', $quantity31, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price31', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(30, $productPriceArr)) ? $price31 = $productPriceArr[30] : $price31 = "";?>
                                    {{ Form::number('price31', $price31, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total31', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total31', (int)$price31 * (int)$quantity31, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product32', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(31, $productArr)) ? $product32 = $productArr[31] : $product32 = "";?>
                                    {{ Form::select('product32', $productslist, $product32, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity32', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(31, $productQtyArr)) ? $quantity32 = $productQtyArr[31] : $quantity32 = "";?>
                                    {{ Form::number('quantity32', $quantity32, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price32', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(31, $productPriceArr)) ? $price32 = $productPriceArr[31] : $price32 = "";?>
                                    {{ Form::number('price32', $price32, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total32', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total32', (int)$price32 * (int)$quantity32, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product33', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(32, $productArr)) ? $product33 = $productArr[32] : $product33 = "";?>
                                    {{ Form::select('product33', $productslist, $product33, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity33', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(32, $productQtyArr)) ? $quantity33 = $productQtyArr[32] : $quantity33 = "";?>
                                    {{ Form::number('quantity33', $quantity33, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price33', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(32, $productPriceArr)) ? $price33 = $productPriceArr[32] : $price33 = "";?>
                                    {{ Form::number('price33', $price33, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total33', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total33', (int)$price33 * (int)$quantity33, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product34', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(33, $productArr)) ? $product34 = $productArr[33] : $product34 = "";?>
                                    {{ Form::select('product34', $productslist, $product34, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity34', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(33, $productQtyArr)) ? $quantity34 = $productQtyArr[33] : $quantity34 = "";?>
                                    {{ Form::number('quantity34', $quantity34, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price34', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(33, $productPriceArr)) ? $price34 = $productPriceArr[33] : $price34 = "";?>
                                    {{ Form::number('price34', $price34, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total34', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total34', (int)$price34 * (int)$quantity34, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product35', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(34, $productArr)) ? $product35 = $productArr[34] : $product35 = "";?>
                                    {{ Form::select('product35', $productslist, $product35, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity35', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(34, $productQtyArr)) ? $quantity35 = $productQtyArr[34] : $quantity35 = "";?>
                                    {{ Form::number('quantity35', $quantity35, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price35', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(34, $productPriceArr)) ? $price35 = $productPriceArr[34] : $price35 = "";?>
                                    {{ Form::number('price35', $price35, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total35', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total35', (int)$price35 * (int)$quantity35, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product36', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(35, $productArr)) ? $product36 = $productArr[35] : $product36 = "";?>
                                    {{ Form::select('product36', $productslist, $product36, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity36', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(35, $productQtyArr)) ? $quantity36 = $productQtyArr[35] : $quantity36 ="";?>
                                    {{ Form::number('quantity36', $quantity36, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price36', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(35, $productPriceArr)) ? $price36 = $productPriceArr[35] : $price36 = "";?>
                                    {{ Form::number('price36', $price36, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total36', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total36', (int)$price36 * (int)$quantity36, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product37', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(36, $productArr)) ? $product37 = $productArr[36] : $product37 = "";?>
                                    {{ Form::select('product37', $productslist, $product37, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity37', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(36, $productQtyArr)) ? $quantity37 = $productQtyArr[36] : $quantity37 = "";?>
                                    {{ Form::number('quantity37', $quantity37, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price37', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(36, $productPriceArr)) ? $price37 = $productPriceArr[36] : $price37 = "";?>
                                    {{ Form::number('price37', $price37, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total37', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total37', (int)$price37 * (int)$quantity37, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product38', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(37, $productArr)) ? $product38 = $productArr[37] : $product38 = "";?>
                                    {{ Form::select('product38', $productslist, $product38, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity38', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(37, $productQtyArr)) ? $quantity38 = $productQtyArr[37] : $quantity38 = "";?>
                                    {{ Form::number('quantity38', $quantity38, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price38', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(37, $productPriceArr)) ? $price38 = $productPriceArr[37] : $price38 = "";?>
                                    {{ Form::number('price38', $price38, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total38', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total38', (int)$price38 * (int)$quantity38, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product39', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(38, $productArr)) ? $product39 = $productArr[38] : $product39 = "";?>
                                    {{ Form::select('product39', $productslist, $product39, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity39', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(38, $productQtyArr)) ? $quantity39 = $productQtyArr[38] : $quantity39 = "";?>
                                    {{ Form::number('quantity39', $quantity39, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price39', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(38, $productPriceArr)) ? $price39 = $productPriceArr[38] : $price39 = "";?>
                                    {{ Form::number('price39', $price39, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total39', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total39', (int)$price39 * (int)$quantity39, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product40', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(39, $productArr)) ? $product40 = $productArr[39] : $product40 = "";?>
                                    {{ Form::select('product40', $productslist, $product40, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity40', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(39, $productQtyArr)) ? $quantity40 = $productQtyArr[39] : $quantity40 = "";?>
                                    {{ Form::number('quantity40', $quantity40, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price40', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(39, $productPriceArr)) ? $price40 = $productPriceArr[39] : $price40 = "";?>
                                    {{ Form::number('price40', $price40, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total40', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total40', (int)$price40 * (int)$quantity40, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product41', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(40, $productArr)) ? $product41 = $productArr[40] : $product41 = "";?>
                                    {{ Form::select('product41', $productslist, $product41, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity41', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(40, $productQtyArr)) ? $quantity41 = $productQtyArr[40] : $quantity41 = "";?>
                                    {{ Form::number('quantity41',$quantity41, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price41', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(40, $productPriceArr)) ? $price41 = $productPriceArr[40] : $price41 = "";?>
                                    {{ Form::number('price41', $price41, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total41', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total41', (int)$price41 * (int)$quantity41, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product42', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(41, $productArr)) ? $product42 = $productArr[41] : $product42 = "";?>
                                    {{ Form::select('product42', $productslist, $product42, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity42', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(41, $productQtyArr)) ? $quantity42 = $productQtyArr[41] : $quantity42 = "";?>
                                    {{ Form::number('quantity42', $quantity42, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price42', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(41, $productPriceArr)) ? $price42 = $productPriceArr[41] : $price42 = "";?>
                                    {{ Form::number('price42', $price42, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total42', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total42', (int)$price42 * (int)$quantity42, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product43', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(42, $productArr)) ? $product43 = $productArr[42] : $product43 = "";?>
                                    {{ Form::select('product43', $productslist, $product43, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity43', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(42, $productQtyArr)) ? $quantity43 = $productQtyArr[42] : $quantity43 = "";?>
                                    {{ Form::number('quantity43',$quantity43, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price43', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(42, $productPriceArr)) ? $price43 = $productPriceArr[42] : $price43 = "";?>
                                    {{ Form::number('price43', $price43, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total43', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total43', (int)$price43 * (int)$quantity43, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product44', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(43, $productArr)) ? $product44 = $productArr[43] : $product44 = "";?>
                                    {{ Form::select('product44', $productslist, $product44, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity44', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(43, $productQtyArr)) ? $quantity44 = $productQtyArr[43] : $quantity44 = "";?>
                                    {{ Form::number('quantity44', $quantity44, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price44', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(43, $productPriceArr)) ? $price44 = $productPriceArr[43] : $price44 = "";?>
                                    {{ Form::number('price44', $price44, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total44', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total44', (int)$price44 * (int)$quantity44, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product45', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(44, $productArr)) ? $product45 = $productArr[44] : $product45 = "";?>
                                    {{ Form::select('product45', $productslist, $product45, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity45', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(44, $productQtyArr)) ? $quantity45 = $productQtyArr[44] : $quantity45 = "";?>
                                    {{ Form::number('quantity45', $quantity45, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price45', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(44, $productPriceArr)) ? $price45 = $productPriceArr[44] : $price45 = "";?>
                                    {{ Form::number('price45', $price45, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total45', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total45', (int)$price45 * (int)$quantity45, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product46', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(45, $productArr)) ? $product46 = $productArr[45] : $product46 = "";?>
                                    {{ Form::select('product46', $productslist, $product46, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity46', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(45, $productQtyArr)) ? $quantity46 = $productQtyArr[45] : $quantity46 = "";?>
                                    {{ Form::number('quantity46', $quantity46, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price46', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(45, $productPriceArr)) ? $price46 = $productPriceArr[45] : $price46 = "";?>
                                    {{ Form::number('price46', $price46, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total46', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total46', (int)$price46 * (int)$quantity46, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product47', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(46, $productArr)) ? $product47 = $productArr[46] : $product47 = "";?>
                                    {{ Form::select('product47', $productslist, $product47, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity47', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(46, $productQtyArr)) ? $quantity47 = $productQtyArr[46] : $quantity47 = "";?>
                                    {{ Form::number('quantity47', $quantity47, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price47', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(46, $productPriceArr)) ? $price47 = $productPriceArr[46] : $price47 = "";?>
                                    {{ Form::number('price47', $price47, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total47', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total47', (int)$price47 * (int)$quantity47, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product48', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(47, $productArr)) ? $product48 = $productArr[47] : $product48 = "";?>
                                    {{ Form::select('product48', $productslist, $product48, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity48', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(47, $productQtyArr)) ? $quantity48 = $productQtyArr[47] : $quantity48 = "";?>
                                    {{ Form::number('quantity48', $quantity48, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price48', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(47, $productPriceArr)) ? $price48 = $productPriceArr[47] : $price48 = "";?>
                                    {{ Form::number('price48', $price48, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total48', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total48', (int)$price48 * (int)$quantity48, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product49', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(48, $productArr)) ? $product49 = $productArr[48] : $product49 = "";?>
                                    {{ Form::select('product49', $productslist,$product49, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity49', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(48, $productQtyArr)) ? $quantity49 = $productQtyArr[48] : $quantity49 = "";?>
                                    {{ Form::number('quantity49', $quantity49, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price49', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(48, $productPriceArr)) ? $price49 = $productPriceArr[48] : $price49 = "";?>
                                    {{ Form::number('price49', $price49, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total49', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total49', (int)$price49 * (int)$quantity49, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('add', 'اضافه کن')}}
                                <div class="input-group">
                                    <div class="addmore btn btn-primary">+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('product50', ' محصول')}}
                                <div class="input-group">
                                    <?php (array_key_exists(49, $productArr)) ? $product50 = $productArr[49] : $product50 = "";?>
                                    {{ Form::select('product50', $productslist, $product50, ['class' => 'form-control'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('quantity50', ' تعداد ')}}
                                <div class="input-group">
                                    <?php (array_key_exists(49, $productQtyArr)) ? $quantity50 = $productQtyArr[49] : $quantity50 = "";?>
                                    {{ Form::number('quantity50', $quantity50, ['class' => 'form-control', 'placeholder' => 'تعداد'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('price50', ' قیمت ')}}
                                <div class="input-group">
                                  <?php (array_key_exists(49, $productPriceArr)) ? $price50 = $productPriceArr[49] : $price50 = "";?>
                                    {{ Form::number('price50', $price50, ['class' => 'form-control', 'placeholder' => 'قیمت'] )}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('total50', ' قیمت کل ')}}
                                <div class="input-group">
                                    {{ Form::number('total50', (int)$price50 * (int)$quantity50, ['class' => 'form-control', 'placeholder' => 'قیمت کل'] )}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">
                                {{ Form::textarea('description', $sale->description, ['class' => 'form-control', 'placeholder' => 'Sale Descripiton', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('saledate', 'تاریخ فروش/تحویل')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::text('saledate', $sale->saledate, ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ فروش/تحویل', 'required'] )}}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('merchant', 'انتخاب بازرگان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">
                                {{ Form::select('merchant', $merchantslist, $sale->merchant, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('commission', 'کمیسیون')}}
                            <div class="input-group">
                                {{ Form::number('commission', $sale->commission, ['class' => 'form-control commission', 'placeholder' => 'کمیسیون فروش'] )}}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
