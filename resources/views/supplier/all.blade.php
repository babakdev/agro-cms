@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">تامین کنندگان</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtSupplier text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>ID</th>
                                <th>تامین کننده</th>
                                <th>تلفن</th>
                                <th>ایمیل</th>
                                <th>نام کارخانه</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($suppliers) > 0 )

                            @foreach($suppliers as $supplier)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $supplier->supplierid }}</td>
                                    <td>{{ $supplier->fname }} {{ $supplier->lname }}</td>
                                    <td>{{ $supplier->phone }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ $supplier->company }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('supplier', $supplier->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('supplier', $supplier->id) }}/edit" title="ویرایش"><i class="material-icons md-12">create</i></a></span>

                                        <form action="{{ route('supplier.destroy', $supplier->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
