@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="widget-user-header bg-primary">
                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username">{{ $supplier->fname }} {{ $supplier->lname }}</h3>
                    <h5 class="widget-user-desc">Supplier | {{ $supplier->company }}</h5>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom" dir="rtl">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">درباره</a></li>
                                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">آدرس</a></li>
                                <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">تراکنشها</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">

                                        <?php if($supplier->photo){ ?>
                                            <img src="{{ URL::asset('images/supplier/' . $supplier->photo)}}" alt="User Avatar">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="User Avatar">
                                        <?php } ?>

                                    </div>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>آیدی/سریال</td>
                                                <td>{{ $supplier->supplierid }}</td>
                                            </tr>
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $supplier->fname }} {{ $supplier->lname }}</td>
                                            </tr>
                                            <tr>
                                                <td>تلفن</td>
                                                <td>{{ $supplier->phone }}</td>
                                            </tr>
                                            <tr>
                                                <td>فکس</td>
                                                <td>{{ $supplier->fax }}</td>
                                            </tr>
                                            <tr>
                                                <td>ایمیل</td>
                                                <td>{{ $supplier->email }}</td>
                                            </tr>
                                            <tr>
                                                <td>نام کارخانه</td>
                                                <td>{{ $supplier->company }}</td>
                                            </tr>
                                            {{-- <tr>
                                                <td>Cheque Issue Name</td>
                                                <td>{{ $supplier->cheque }}</td>
                                            </tr> --}}
                                            <tr>
                                                <td>نام بانک</td>
                                                <td>{{ $supplier->bank }}</td>
                                            </tr>
                                            <tr>
                                                <td>شماره حساب بانکی</td>
                                                <td>{{ $supplier->account }}</td>
                                            </tr>
                                            <tr>
                                                <td>Paypal اکانت</td>
                                                <td>{{ $supplier->paypal }}</td>
                                            </tr>
                                            <tr>
                                                <td>روش دیگر پرداخت</td>
                                                <td>{{ $supplier->other }}</td>
                                            </tr>
                                            <tr>
                                                <td>Social Media Links</td>
                                                <td>
                                                    <a class="btn btn-social-icon btn-facebook" href="{{ $supplier->facebook }}"><i class="fa fa-facebook"></i></a>
                                                    <a class="btn btn-social-icon btn-twitter" href="{{ $supplier->twitter }}"><i class="fa fa-twitter"></i></a>
                                                    <a class="btn btn-social-icon btn-google" href="{{ $supplier->youtube }}"><i class="fa fa-youtube"></i></a>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $supplier->about !!}
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    {!! $supplier->address !!}
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_4">
                                    <table class="table table-bordered table-hover dtProduct text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>تاریخ</th>
                                <th>محصول</th>
                                <th>تعداد</th>
                                <th>قیمت</th>
                                <th>کمیسیون</th>
                                <th>قیمت کل</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($purchases) > 0 )

                            @foreach($purchases as $purchase)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($purchase->purchasedate)->format('j M Y') }}</td>
                                    <td>{{ $purchase->title }}</td>
                                    <td>{{ number_format($purchase->quantity) }}</td>
                                    <td>{{ number_format($purchase->price) }}</td>
                                    <td>{{ number_format($purchase->commission) }}</td>
                                    <td>{{ number_format($purchase->total) }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('purchase', $purchase->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('purchase', $purchase->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('purchase.destroy', $purchase->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection
