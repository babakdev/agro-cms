@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title right">تامین کننده جدید </h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['action' => 'SupplierController@store', 'files' => true, 'method' => 'POST']) !!}
                    
                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('supplierid', 'آیدی/سریال')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('supplierid', '', ['class' => 'form-control', 'placeholder' => 'آیدی/سریال'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('fname', 'نام ')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('fname', '', ['class' => 'form-control', 'placeholder' => 'نام '] )}}
                            </div>
                        </div>
                    </div>
{{-- 
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('lname', 'نام خانوادگی')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('lname', '', ['class' => 'form-control', 'placeholder' => 'نام خانوادگی'] )}}
                            </div>
                        </div>
                    </div> --}}

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'تلفن'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('fax', 'فکس')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('fax', '', ['class' => 'form-control', 'placeholder' => 'فکس'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('email', 'ایمیل')}}
                            <div class="input-group">                                                                      
                                {{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'ایمیل'] )}}
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('company', 'نام کارخانه')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('company', '', ['class' => 'form-control', 'placeholder' => 'نام کارخانه'] )}}
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('cheque', 'Cheque Name')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('cheque', '', ['class' => 'form-control', 'placeholder' => 'Cheque Issue Name'] )}}
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('bank', 'نام بانک')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('bank', '', ['class' => 'form-control', 'placeholder' => 'نام بانک'] )}}
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('account', 'شماره حساب بانکی')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('account', '', ['class' => 'form-control', 'placeholder' => 'شماره حساب بانکی'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('paypal', 'Paypal')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('paypal', '', ['class' => 'form-control', 'placeholder' => 'اکانت Paypal'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('other', 'روش دیگر پرداخت')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('other', '', ['class' => 'form-control', 'placeholder' => 'روش دیگر پرداخت'] )}}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('address', 'آدرس')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('address', '', ['class' => 'form-control', 'placeholder' => 'آدرس', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>  


                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('about', 'درباره')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('about', '', ['class' => 'form-control', 'placeholder' => 'درباره', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div> 


                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('facebook', 'Facebook')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('facebook', '', ['class' => 'form-control', 'placeholder' => 'Facebook Page/Profile Link'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('twitter', 'Twitter')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('twitter', '', ['class' => 'form-control', 'placeholder' => 'Twitter Page/Profile Link'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('youtube', 'Youtube')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('youtube', '', ['class' => 'form-control', 'placeholder' => 'Youtube Channel Link'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('map', 'Google Map')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('map', '', ['class' => 'form-control', 'placeholder' => 'Google Map Location'] )}}
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

