@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Invoices</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtInvoice">
                        <thead>
                            <tr>
                                <th style="width: 10px">SL</th>
                                <th>Order ID</th>
                                <th>Sale Date</th>
                                <th>Merchant</th>
                                <th>Due Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($invoices) > 0 )

                            @foreach($invoices as $invoice)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $invoice->orderid }}</td>
                                    <td>{{ Carbon::parse($invoice->invoicedate)->format('j M Y') }}</td>
                                    <td>{{ Helper::getNameByID($invoice->merchant, 'merchants') }}</td>
                                    <td>{{ Carbon::parse($invoice->paymentdue)->format('j M Y') }}</td>
                                    <td>{{ $invoice->paymentstatus }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('users/invoices/view', $invoice->id) }}">View</a></span>                                        
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

