@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Purchases</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtPurchase">
                        <thead>
                            <tr>
                                <th style="width: 10px">SL</th>
                                <th>Date</th>
                                <th>Item/Title</th>
                                <th>Category</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead> 
                        <tbody
                            @if(count($purchases) > 0 )
                            
                            @foreach($purchases as $purchase)
                            
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($purchase->purchasedate)->format('j M Y') }}</td>
                                    <td>{{ Helper::getProductTitle($purchase->product) }}</td>
                                    <td>{{ $purchase->category }}</td>
                                    <td>{{ number_format($purchase->quantity, 2) }}</td>                                    
                                    <td>{{ Helper::getCurrency(). " " . number_format($purchase->total, 2) }}</td>
                                    
                                    <?php if( Helper::invoiceBySale($purchase->id)){                                        
                                        if(isset(Helper::invoiceBySale($purchase->id)->paidamount) && Helper::invoiceBySale($purchase->id)->paidamount == $purchase->total){ ?>
                                            <td><span class="badge bg-green">Paid</span></td>
                                            <td><?php if( Helper::invoiceBySale($purchase->id)){ echo Helper::getCurrency(). " " . number_format(Helper::invoiceBySale($purchase->id)->paidamount, 2); } ?></td>
                                        <?php }elseif(isset(Helper::invoiceBySale($purchase->id)->paidamount) && Helper::invoiceBySale($purchase->id)->paidamount !== $purchase->total ){ ?>
                                            <td><span class="badge bg-yellow">Due</span></td>
                                            <td><?php if( Helper::invoiceBySale($purchase->id)){ echo Helper::getCurrency(). " " . number_format($purchase->total - Helper::invoiceBySale($purchase->id)->paidamount , 2); } ?></td>
                                        <?php }else{ ?>
                                            <td><span class="badge bg-red">Unpaid</span></td>
                                            <td>{{ Helper::getCurrency(). " " . number_format($purchase->total, 2) }}</td>
                                        <?php } ?>                                            
                                    <?php }else{ ?>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                    <?php } ?>
                                            
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('users/purchases/view', $purchase->id) }}">View</a></span>                                        			  
                                    </td>
                                </tr>

                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

