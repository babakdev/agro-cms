@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">
                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ Helper::getProductTitle($purchase->product) }}</h3>
                    <h5 class="widget-user-desc">{{ $purchase->category }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Primary</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        <img src="{{ URL::asset('images/product/' . Helper::getProduct($purchase->product)->photo )}}" alt="User Avatar">
                                    </div>
                                    <!-- /.widget-user-image -->
                                    
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>Purchase Title</td>
                                                <td>{{ Helper::getProductTitle($purchase->product) }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>Purchase Date</td>
                                                <td>{{ Carbon::parse($purchase->purchasedate)->format('j M Y') }}</td>                                                
                                            </tr>                                            
                                            <tr>
                                                <td>Category</td>
                                                <td>{{ $purchase->category }}</td>                                                
                                            </tr> 
                                            <tr>
                                                <td>Unit/Set/Carton Quantity</td>
                                                <td>{{ $purchase->quantity }} {{ $purchase->unit }}</td>
                                            </tr>
                                            <tr>
                                                <td>Company</td>
                                                <td>{{ $purchase->company }}</td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td>(+) {{ Helper::getCurrency(). " " . number_format($purchase->price, 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Commission</td>
                                                <td>(-) {{ Helper::getCurrency(). " " . number_format($purchase->commission, 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Total Price</strong></td>
                                                <td><strong>(=) {{ Helper::getCurrency(). " " . number_format($purchase->total, 2) }}</strong></td>
                                            </tr>
                                            
                                                                                   
                                        </tbody></table>
                                </div>
                                
                                
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection