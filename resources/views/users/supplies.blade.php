@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Supplies</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtPurchase">
                        <thead>
                            <tr>
                                <th style="width: 10px">SL</th>
                                <th>Date</th>
                                <th>Item/Title</th>
                                <th>Category</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead> 
                        <tbody
                            @if(count($supplies) > 0 )
                            
                            @foreach($supplies as $supplie)
                            
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon::parse($supplie->purchasedate)->format('j M Y') }}</td>
                                    <td>{{ $supplie->title }}</td>
                                    <td>{{ $supplie->category }}</td>
                                    <td>{{ number_format($supplie->quantity, 2) }}</td>                                    
                                    <td>{{ Helper::getCurrency(). " " . number_format($supplie->total, 2) }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('users/supplies/view', $supplie->id) }}">View</a></span>                                        			  
                                    </td>
                                </tr>

                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

