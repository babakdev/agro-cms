       
@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  Edit Purchase</h3>
                    <h3 class="box-title right">
                        <button type="button" class="btn btn-primary photo_input float-right">Choose Photo - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['PurchaseController@update', $purchase->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}
                    
                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('title', 'Purchase Item/Purchase/Title')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('title', $purchase->title, ['class' => 'form-control', 'placeholder' => 'Purchase Title', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'Purchase Description')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('description', $purchase->description, ['class' => 'form-control', 'placeholder' => 'Purchase Descripiton', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div>  

                    @php $arrayCatLists = array(' ' => 'Select Category', $purchase->category => $purchase->category ); @endphp
                    @if(count($categorieslist) > 0 )                            
                    @foreach($categorieslist as $category)
                    @php                                 
                    $arrayCatLists[$category->category] = $category->category;
                    @endphp
                    @endforeach                        
                    @endif

                    @php $supplierslist = array(' ' => 'Select Supplier', $purchase->supplier => $purchase->supplier); @endphp
                    @if(count($suppliers) > 0 )                            
                    @foreach($suppliers as $supplier)
                    @php 
                    $supplierslist[$supplier->id] = $supplier->fname;
                    @endphp
                    @endforeach                        
                    @endif   

                                        
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('purchasedate', 'Purchase Date')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('purchasedate', $purchase->purchasedate, ['class' => 'form-control datepicker', 'placeholder' => 'Purchase Date'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('', 'Purchase Category')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('category', $arrayCatLists, $purchase->category, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('supplier', 'Purchase Supplier')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierslist,  $purchase->supplier, ['class' => 'form-control', 'required'] )}}
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('unit', 'Unit/Set/Carton')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('unit', $purchase->unit, ['class' => 'form-control', 'placeholder' => 'Unit/Set/Carton'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('quantity', 'Unit/Set/Carton Quantity')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('quantity', $purchase->quantity, ['class' => 'form-control quantity', 'placeholder' => 'Unit/Set/Carton Quantity', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('company', 'Company/Brand')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('company', $purchase->company, ['class' => 'form-control', 'placeholder' => 'Company/Brand'] )}}
                            </div>
                        </div>
                    </div>                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('price', 'Individual Price')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('price', $purchase->price, ['class' => 'form-control price', 'placeholder' => 'Individual Price', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('commission', 'Commission')}}
                            <div class="input-group">                                                                  
                                {{ Form::number('commission', $purchase->commission, ['class' => 'form-control commission', 'placeholder' => 'Purchase Commission'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('total', 'Total Price')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::number('total', $purchase->total, ['class' => 'form-control total', 'placeholder' => 'Total', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('Submit', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>                        



                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection 