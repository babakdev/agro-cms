@extends('layouts.app')

@section('content')


<style>
    @media print {    
        .main-footer{
            display: none;
        }
    }
</style>

<!-- Main content -->

<?php
    $totalSubtotal = 0;
    if ($invoice->product1) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product1) * $invoice->quantity1;
    }

    if ($invoice->product2) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product2) * $invoice->quantity2;
    }

    if ($invoice->product3) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product3) * $invoice->quantity3;
    }
    if ($invoice->product4) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product4) * $invoice->quantity4;
    }
    if ($invoice->product5) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product5) * $invoice->quantity5;
    }
    if ($invoice->product6) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product6) * $invoice->quantity6;
    }
    if ($invoice->product7) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product7) * $invoice->quantity7;
    }
    if ($invoice->product8) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product8) * $invoice->quantity8;
    }
    if ($invoice->product9) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product9) * $invoice->quantity9;
    }
    if ($invoice->product10) {
        $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product10) * $invoice->quantity10;
    }


    $discount = $totalSubtotal * $invoice->discount / 100;
    $lessdiscount = $totalSubtotal - $discount;
    $tax = $lessdiscount * $invoice->tax / 100;
    $total = $totalSubtotal - $discount + $tax + $invoice->other;
?>


<section class="invoice" id="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <img style="width:40px;" src="{{ URL::asset('images/' . Helper::getSettings()->photo)}}" alt="{{ Helper::getSettings()->title }}"> {{ Helper::getSettings()->title }}
                <small class="pull-right">Date: {{ $invoice->invoicedate }}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            From
            <address>
                <strong>{{ Helper::getSettings()->title }}</strong><br>
                {{ Helper::getSettings()->address }}<br>
                {{ Helper::getSettings()->city }}, {{ Helper::getSettings()->country }}, {{ Helper::getSettings()->postal }}<br>
                Phone: {{ Helper::getSettings()->phone }}<br>
                Email: {{ Helper::getSettings()->email }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            To
            <address>
                @if( Helper::getProfileByID($invoice->merchant, 'merchants') !== 0 )
                    <strong>{{ Helper::getProfileByID($invoice->merchant, 'merchants')->fname . " " . Helper::getProfileByID($invoice->merchant, 'merchants')->lname .  " ( " . Helper::getProfileByID($invoice->merchant, 'merchants')->company . " ) " }}</strong><br>
                    @if( Helper::getProfileByID($invoice->merchant, 'merchants')->address )
                    {{ strip_tags(Helper::getProfileByID($invoice->merchant, 'merchants')->address) }}<br>
                    @endif      
                    Phone: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->phone }}<br>
                    Email: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->email }}<br>
                    @if( Helper::getProfileByID($invoice->merchant, 'merchants')->fax )
                    Fax: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->fax }}
                    @endif
                @endif
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Invoice #{{ $invoice->id }}</b><br>
            <b>Order ID:</b> {{ $invoice->orderid }}<br>
            <b>Payment Due Date:</b> {{ $invoice->paymentdue }}<br>
            <b>Payment Method:</b> {{ $invoice->paymentmethod }}<br>
            <b>Account:</b> {{ $invoice->paymentaccount }}<br>
            <b>Status:</b> 
            <span class="
            <?php if( $invoice->paymentstatus == "Paid"  && $total == $invoice->paidamount ){ 
                echo "text-green";                
            } elseif($invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount){ 
                echo "text-yellow"; 
            } else{ 
                echo "text-red";                 
            } ?>">
                
            <?php if( $invoice->paymentstatus == "Paid"  && $total == $invoice->paidamount ){ 
                echo $invoice->paymentstatus;                
            } elseif( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount){
                echo "Partial Paid";                 
            }else{ 
                echo "Unpaid";             
            } ?></span><br>
            
            <b>Due:</b> 
            <span class="
            <?php if( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount ){ 
                echo "text-yellow";                
            } ?>">
                
            <?php if( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount ){ 
                echo Helper::getCurrency() . " " . number_format($total - $invoice->paidamount, 2);              
            }?></span>
            
            <hr>
            <?php if($invoice->paymentstatus == "Paid"){?>
            <b>Paid Amount:</b> {{ $invoice->paidamount }}<br>
            <b>Paid Method:</b> {{ $invoice->paidmethod }}<br>
            <b>Paid Acc. Info:</b> {{ $invoice->paidinfo }}<br>
            <br><br>
            <?php }?>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th># Code</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>

                    @if(count($invoice->product1) > 0)
                        @if(Helper::getProduct($invoice->product1) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product1)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product1)->title }}</td>
                                <td>{{ number_format($invoice->quantity1,2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product1), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product1) * $invoice->quantity1, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product2) > 0)
                        @if(Helper::getProduct($invoice->product2) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product2)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product2)->title }}</td>
                                <td>{{ number_format($invoice->quantity2, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product2), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product2) * $invoice->quantity2, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product3) > 0)
                        @if(Helper::getProduct($invoice->product3) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product3)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product3)->title }}</td>
                                <td>{{ number_format($invoice->quantity3, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product3), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product3) * $invoice->quantity3, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product4) > 0)
                        @if(Helper::getProduct($invoice->product4) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product4)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product4)->title }}</td>
                                <td>{{ number_format($invoice->quantity4, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product4), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product4) * $invoice->quantity4, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product5) > 0)
                        @if(Helper::getProduct($invoice->product5) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product5)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product5)->title }}</td>
                                <td>{{ number_format($invoice->quantity5, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product5), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product5) * $invoice->quantity5, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product6) > 0)
                        @if(Helper::getProduct($invoice->product6) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product6)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product6)->title }}</td>
                                <td>{{ number_format($invoice->quantity6, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product6), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product6) * $invoice->quantity6, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product7) > 0)
                        @if(Helper::getProduct($invoice->product7) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product7)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product7)->title }}</td>
                                <td>{{ number_format($invoice->quantity7, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product7), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product7) * $invoice->quantity7, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product8) > 0)
                        @if(Helper::getProduct($invoice->product8) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product8)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product8)->title }}</td>
                                <td>{{ number_format($invoice->quantity8, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product8), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product8) * $invoice->quantity8, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product9) > 0)
                        @if(Helper::getProduct($invoice->product9) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product9)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product9)->title }}</td>
                                <td>{{ number_format($invoice->quantity9, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product9), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product9) * $invoice->quantity9, 2) }}</td>
                            </tr>
                        @endif
                    @endif
                    @if(count($invoice->product10) > 0)
                        @if(Helper::getProduct($invoice->product10) !== 0)
                            <tr>
                                <td>{{ Helper::getProduct($invoice->product10)->id }}</td>
                                <td>{{ Helper::getProduct($invoice->product10)->title }}</td>
                                <td>{{ number_format($invoice->quantity10, 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product10), 2) }}</td>
                                <td>{{ Helper::getCurrency(). " " . number_format(Helper::getProductCost($invoice->product10) * $invoice->quantity10, 2) }}</td>
                            </tr>
                        @endif
                    @endif

                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <!-- <p class="lead">Payment Methods:</p>
          <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

            @if($invoice->note)
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                {{ strip_tags($invoice->note) }}
            </p>
            @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!-- <p class="lead">Amount Due {{ $invoice->paymentdue }}</p> -->

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                            <th style="width:50%">Subtotal:</th>
                            
                            <td>{{ Helper::getCurrency(). " " . number_format( $totalSubtotal , 2) }}</td>
                        </tr>
                        <tr>
                            <th>Discount {{ $invoice->discount }}%</th>
                            <td>(-) {{ Helper::getCurrency(). " " . number_format( $discount, 2 ) }}</td>
                        </tr>
                        <tr>
                            <th>Tax {{ $invoice->tax }}%</th>
                            <td>(+) {{ Helper::getCurrency(). " " . number_format( $tax, 2) }}</td>
                        </tr>
                        <tr>
                            <th>Other:</th>
                            <td>(+) {{ Helper::getCurrency(). " " . number_format( $invoice->other, 2 ) }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>(=) {{ Helper::getCurrency(). " " . number_format( $total, 2 ) }}</td>
                        </tr>
                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a id="print" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
            
            <a href="{{ url('invoice', $invoice->id) }}/pdf" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Generate PDF
            </a>
                        
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-payment-paypal">
                <i class="fa fa-money"></i> Pay Now
            </button>
            
        </div>
    </div>
</section>

<div class="modal fade" id="modal-payment-paypal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">
                
                <div class="col-md-8 col-md-offset-2 btmMediumSpace">
                    <h1 class="text-center"><img class="payment-logo"src="{{ asset('images/paypal.jpg')}}" alt="Paypal Logo"></h1>
                    {!! Form::open(['action' => 'PaypalController@store', 'method' => 'POST']) !!}
                
                    <div class="col-md-12 btmSmallSpace"> 
                        
                        <div class="form-group">                        
                            {{ Form::label('', 'Payment Title')}}  <span class="text-red"> (*)</span>
                            <div class="input-group">
                                <input class="form-control" type="hidden" name="id" value="{{ $invoice->id }}" >
                                <input class="form-control" type="hidden" name="title" value="Invoice #{{ $invoice->id }}" >
                                <input class="form-control" type="text" value="Invoice #{{ $invoice->id }}" disabled="">
                            </div>
                        </div>
                        <div class="form-group">                        
                            {{ Form::label('', 'Payment Amount')}}  <span class="text-red"> (*)</span>
                            <div class="input-group">
                                <input class="form-control" type="hidden" name="amount" value="{{ $total }}" >
                                <input class="form-control" type="text" value="{{ $total }}" disabled="">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12 btmSmallSpace">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('Pay Now ( ' . Helper::getCurrency(). " " . number_format( $total, 2) . ' )', ['class' => 'btn btn-primary col-md-offset-3 col-md-6'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    
                </div>
                
                <div class="col-md-8 col-md-offset-2 btmSmallSpace">
                    
                    <h1 class="text-center"><img class="payment-logo"src="{{ asset('images/stripe.png')}}" alt="Stripe Logo"></h1>
                    
                    <!-- stripe payment form -->
                    {!! Form::open(['action' => 'StripeController@stripe', 'method' => 'POST', 'id' => 'payment-form']) !!}
                    
                        <input type="hidden" name="invoiceid" value="{{ $invoice->id }}">
                        <div class="form-row btmMediumSpace">
                          <p for="card-element">Credit or Debit Card</p>
                          <div id="card-element">
                            <!-- a Stripe Element will be inserted here. -->
                          </div>
                          <!-- Used to display form errors -->
                          <div id="card-errors"></div>
                        </div>
                        <button type="submit" class="btn btn-primary col-md-offset-3 col-md-6 payNowBtn">Pay Now ( {{Helper::getCurrency(). " " . number_format( $total, 2)}} )</button>
                    {!! Form::close() !!}
                    
                </div>
                
                </div>
            
            <div style="border:0" class="modal-footer"></div>
        </div>
    </div>
</div>


@endsection
