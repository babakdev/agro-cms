<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ Helper::getSettings()->title }} | {{ Helper::getSettings()->tag }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link rel="shortcut icon" href="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" type="image/x-icon">

        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/font-awesome/css/font-awesome.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css')}}">
        <!-- Event Calendar -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.2/fullcalendar.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/select2/dist/css/select2.min.css')}}">
        <!-- Plugin CSS -->
        <link type="text/css" href="{{ asset('css/OverlayScrollbars.min.css')}}" rel="stylesheet"/>
        <!-- Custom Style -->
        <link rel="stylesheet" href="{{ URL::asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('css/responsive.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">

        <!-- jQuery 3 -->
        <script src="{{ URL::asset('adminlte/components/jquery/dist/jquery.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::asset('adminlte/components/jquery-ui/jquery-ui.min.js')}}"></script>
        <!-- Chart.Js -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.2/fullcalendar.min.js"></script>


    </head>
    <body class="hold-transition skin-blue sidebar-mini pos">

        <div class="loading" id="loading" style="display:none;">
            <img src="{{ asset('images/loading.svg')}}" alt="Loading">
        </div>


        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    
                    <h4 class="company-title"><img class="customLogo" src="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" alt="Logo">  {{ Helper::getSettings()->title }} <?php if(Helper::getSettings()->tag !== " "){?> | {{ Helper::getSettings()->tag }} <?php } ?></h4>

                    <div class="navbar-custom-menu">
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                            <li><a href="{{ route('login') }}"><i class="material-icons">lock</i> ورود</a></li>
                            <li><a href="{{ route('register') }}"><i class="material-icons">lock_outline</i> عضویت</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <i class="material-icons">apps</i>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('user/' . Auth::user()->id )}}"><i class="material-icons">person</i> پروفایل</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('user/' . Auth::user()->id . '/edit' )}}"><i class="material-icons">person</i> ویرایش پروفایل</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            <i class="material-icons">close</i> خروج
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                            @endguest
                        </ul>

                        <ul class="nav navbar-nav navbar-right pos">
                            <li><a href="{{ url('/') }}" target="_blank">داشبورد <i class="material-icons md-18">dashboard</i></a></li>
                        </ul>

                    </div>

                </nav>
            </header>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <div class="col-md-offset-9 col-md-3">
                        @include('inc.messages')
                    </div>
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
        </div>
        <!-- ./wrapper -->


        <!-- Scripts -->
        <!--<script src="{{ asset('js/app.js') }}"></script>-->
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
            var baseurl = "<?php echo url('/'); ?>";
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('adminlte/components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <!-- Select2 -->
        <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js')}}"></script>

        <!-- datepicker -->
        <script src="{{ asset('adminlte/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('adminlte/dist/js/adminlte.min.js')}}"></script>
        <!-- Chart.Js -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
        <!-- Plugin JS -->
        <script type="text/javascript" src="{{ asset('js/jquery.overlayScrollbars.min.js')}}"></script>
        <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/r-2.2.1/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/r-2.2.1/datatables.min.js"></script>
        <script src="{{ asset('laravel-ckeditor/ckeditor.js')}}"></script>
        <script src="{{ asset('laravel-ckeditor/adapters/jquery.js')}}"></script>
        <script src="{{ asset('js/iniDatatables.js')}}"></script>
        <script src="{{ asset('js/mainScripts.js')}}"></script>

        <!-- Stripe JS -->
        <script src="https://js.stripe.com/v3/"></script>
        <!-- Your JS File -->
        <!-- <script src="{{ asset('js/stripecharge.js')}}"></script> -->

        <script>
        <?php if(Request::is('pos')){ ?>

        $(".pos select.paymentMethod").on('change', function() {
            var paymentMethod = $(".pos select.paymentMethod").val();
            console.log(paymentMethod);
            if(paymentMethod == 1){
              $("#payment-form #card-element").attr('id', 'xxcard-element');
            }else if(paymentMethod == 2){
                $("#payment-form #xxcard-element").attr('id', 'card-element');
            }else{
               $("#payment-form #card-element").attr('id', 'xxcard-element');
            }
        });

        // Stripe API Key
        // var stripe = Stripe('pk_test_uavQVvB1ueyXPdPQTTi6LeDp');
        var stripe = Stripe('{{ env('STRIPE_KEY') }}');
        var elements = stripe.elements();
        // Custom Styling
        var style = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        // Create an instance of the card Element
        var card = elements.create('card', {style: style});
        // Add an instance of the card Element into the `card-element` <div>
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
        if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {

            var paymentMethod = $(".pos select.paymentMethod").val();
            if(paymentMethod == 2){
                event.preventDefault();
                // event.preventDefault();
                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        stripeTokenHandler(result.token);
                    }
                });
            }

        });

        // Send Stripe Token to Server
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
        // Add Stripe Token to hidden input
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
        // Submit form
            form.submit();
        }

        <?php } ?>

        </script>

        <script>

        $("#photo").on('change', function () {

            $(".image-holder-col").css({"visibility": "visible", "height": "auto"});

            //Get count of selected files
            var countFiles = $(this)[0].files.length;

            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();

            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {

                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);
                        }

                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }

                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });

</script>



    </body>
</html>
