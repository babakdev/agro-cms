<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ Helper::getSettings()->title }} | {{ Helper::getSettings()->tag }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link rel="shortcut icon" href="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('images/' . Helper::getSettings()->photo )}}" type="image/x-icon">

        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/components/font-awesome/css/font-awesome.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css')}}">
        <!-- Custom Style -->
        <link rel="stylesheet" href="{{ URL::asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('css/responsive.css')}}">

        <!-- Styles -->
        <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <div class="content-wrapper access-page">
                <section class="content">
                    <div class="col-md-offset-9 col-md-3">
                        @include('inc.messages')
                    </div>
                    @yield('content')
                </section>
            </div>
            <div class="control-sidebar-bg"></div>
        </div>
        <script src="{{ URL::asset('adminlte/components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('adminlte/components/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{ URL::asset('adminlte/components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('adminlte/dist/js/adminlte.min.js')}}"></script>
    </body>
</html>
