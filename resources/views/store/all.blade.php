@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">نمایندگی ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtStore text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام</th>
                                <th>تلفن</th>
                                <th>شهر</th>
                                {{-- <th>Time</th> --}}
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($stores) > 0 )

                            @foreach($stores as $store)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $store->name }}</td>
                                    <td>{{ $store->phone }}</td>
                                    <td>{{ $store->location }}</td>
                                    {{-- <td>{{ $store->time }}</td> --}}
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('store', $store->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('store', $store->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('store.destroy', $store->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
