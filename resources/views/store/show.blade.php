@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">                
                <div class="widget-user-header bg-primary">                    
                    <h3 class="widget-user-username">{{ $store->name }}</h3>
                    <h5 class="widget-user-desc">{{ $store->location }}</h5>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">توضیحات</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        <?php if($store->photo){ ?>
                                            <img src="{{ URL::asset('images/staff/' . $store->photo)}}" alt="Store Photo">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="Staff Photo">
                                        <?php } ?>
                                    </div>
                                    <!-- /.widget-user-image -->
                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>نام فروشگاه</td>
                                                <td>{{ $store->name }}</td>                                                
                                            </tr> 
                                            {{-- <tr>
                                                <td>Store Slogan</td>
                                                <td>{{ $store->slogan }}</td>                                                
                                            </tr>  --}}
                                            <tr>
                                                <td>تلفن</td>
                                                <td>{{ $store->phone }}</td>                                                
                                            </tr> 
                                            {{-- <tr>
                                                <td>Store Open & Close Time</td>
                                                <td>{{ $store->time }}</td>                                                
                                            </tr>  --}}
                                            <tr>
                                                <td>استان و شهر</td>
                                                <td>{{ $store->location }}</td>                                                
                                            </tr> 
                                            <tr>
                                                <td>آدرس </td>
                                                <td>{{ $store->address }}</td>                                                
                                            </tr> 
                                            <tr>
                                                <td>مدیر فروشگاه</td>
                                                <td><a title="Manager Details" href="{{ url('staff', $store->manager) }}">{{ Helper::getNameByID($store->manager, 'staff') }}</a></td>                                                
                                            </tr> 
                                            <tr>
                                                <td>شماره تماس مدیر فروشگاه</td>
                                                <td>{{ $store->managerphone }}</td>                                                
                                            </tr>                                             
                                            {{-- <tr>
                                                <td>Total Staffs</td>
                                                <td>{{ $store->staff }}</td>                                                
                                            </tr>  --}}
                                            {{-- <tr>
                                                <td>Store Social Media</td>
                                                <td>
                                                    <a class="btn btn-social-icon btn-primary" href="{{ $store->website }}"><i class="fa fa-globe"></i></a>
                                                    <a class="btn btn-social-icon btn-facebook" href="{{ $store->facebook }}"><i class="fa fa-facebook"></i></a>
                                                    <a class="btn btn-social-icon btn-twitter" href="{{ $store->twitter }}"><i class="fa fa-twitter"></i></a>
                                                    <a class="btn btn-social-icon btn-google" href="{{ $store->youtube }}"><i class="fa fa-youtube"></i></a>
                                                    <a class="btn btn-social-icon btn-google" href="{{ $store->map }}"><i class="fa fa-map-marker"></i></a>
                                                </td>
                                            </tr>    --}}
                                            
                                        </tbody></table>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    {!! $store->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection