@extends('layouts.app')

@section('content')

@php $staffslist = array(' ' => 'انتخاب مدیر فروشگاه'); @endphp
@if(count($staffs) > 0 )                            
@foreach($staffs as $staff)
@php 
$staffslist[$staff->id] = $staff->fname . " (" . $staff->position .  ")" ;
@endphp
@endforeach                        
@endif   

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  نمایندگی جدید</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'StoreController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('name', 'نام فروشگاه')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'نام فروشگاه', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('slogan', 'Store Slogan/Tag Line')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('slogan', '', ['class' => 'form-control', 'placeholder' => 'Store Slogan'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'تلفن', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('time', 'Store Open & Close Time')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('time', '', ['class' => 'form-control', 'placeholder' => 'Store Opening & Closing Time', 'required'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('location', 'استان و شهر')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('location', '', ['class' => 'form-control', 'placeholder' => 'استان و شهر', 'rquired'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('address', 'آدرس')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'آدرس'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('manager', 'مدیر فروشگاه')}}
                            <div class="input-group">                                                                  
                                {{ Form::select('manager', $staffslist, null, ['class' => 'form-control'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('managerphone', 'شماره تماس مدیر فروشگاه')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('managerphone', '', ['class' => 'form-control', 'placeholder' => 'شماره تماس مدیر فروشگاه'] )}}
                            </div>
                        </div>
                    </div>   
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('staff', 'Store Total Staff')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('staff', '', ['class' => 'form-control', 'placeholder' => 'Store Total Staff'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('website', 'وبسایت')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('website', '', ['class' => 'form-control', 'placeholder' => 'وبسایت'] )}}
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('facebook', 'Store Facebook Page')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('facebook', '', ['class' => 'form-control', 'placeholder' => 'Store Facebook Page'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('youtube', 'Store Youtube Channel')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('youtube', '', ['class' => 'form-control', 'placeholder' => 'Store Youtube'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('twitter', 'Store Twitter Page')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('twitter', '', ['class' => 'form-control', 'placeholder' => 'Store Twitter'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('map', 'Store Google Map')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('map', '', ['class' => 'form-control', 'placeholder' => 'Store Google Map URL'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('description', 'توضیحات')}}
                            <div class="input-group">                                                                  
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'توضیحات'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>  
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

