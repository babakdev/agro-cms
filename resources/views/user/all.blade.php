@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">کاربران</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtStaff text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام</th>
                                <th>ایمیل</th>
                                <th>تلفن</th>
                                <th>جنسیت</th>
                                <th>نقش</th>
                                <th>مربوط به</th>
                                <th style = "width:15%">عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($users) > 0 )

                            @foreach($users as $user)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td><?php if($user->supplier){ ?> <a title="جزئیات تامین کننده" href="/supplier/{{ $user->supplier }}">{{ Helper::getNameByID($user->supplier, 'suppliers') }}</a>,<?php } ?> <?php if($user->merchant){ ?><a title="جزئیات بازرگان" href="/merchant/{{ $user->merchant }}">{{ Helper::getNameByID($user->merchant, 'merchants') }}</a><?php } ?></td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('user', $user->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('user', $user->id) }}/edit" title="ویرایش"><i class="material-icons md-12">create</i></a></span>

                                        <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
