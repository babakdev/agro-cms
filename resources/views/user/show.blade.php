@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="widget-user-header bg-primary">

                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username">{{ $user->name }}</h3>
                    <h5 class="widget-user-desc">نقش | {{ $user->role }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        
                                        <?php if($user->photo){ ?>
                                            <img src="{{ URL::asset('images/user/' . $user->photo)}}" alt="User Avatar">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="User Avatar">
                                        <?php } ?>
                                            
                                    </div>

                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $user->name }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>ایمیل</td>
                                                <td>{{ $user->email }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تلفن</td>
                                                <td>{{ $user->phone }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تاریخ تولد</td>
                                                <td>{{ $user->dob }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>جنسیت</td>
                                                <td>{{ $user->gender }}</td>
                                            </tr>
                                            <tr>
                                                <td>پروفایل تامین کننده و وارد کننده</td>
                                                <td><?php if($user->supplier){ ?> <a title="جزئیات تامین کننده" href="{{ url('supplier', $user->supplier) }}">{{ Helper::getNameByID($user->supplier, 'suppliers') }}</a><?php } ?> <?php if($user->merchant){ ?> | <a title="جزئیات بازرگان" href="{{ url('merchant', $user->merchant) }}">{{ Helper::getNameByID($user->merchant, 'merchants') }}</a><?php } ?></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection