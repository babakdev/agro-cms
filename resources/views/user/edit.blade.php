@extends('layouts.app')

@section('content')

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )                            
@foreach($merchants as $merchant)
@php 
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach                        
@endif

@php $supplierslist = array(' ' => 'انتخاب تامین کننده'); @endphp
@if(count($suppliers) > 0 )                            
@foreach($suppliers as $supplier)
@php 
$supplierslist[$supplier->id] = $supplier->fname;
@endphp
@endforeach                        
@endif

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  ویرایش کاربر</h3>
                    <h3 class="box-title left col-sm-8">
                        <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#modal-email">
                            تغییر ایمیل
                        </button>
                        <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#modal-password">
                            تغییر رمزعبور
                        </button>
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['UserController@update', $user->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}

                    <input type="hidden" name="email" value="<?php echo $user->email; ?>">
                    <input type="hidden" name="password" value="<?php echo $user->password; ?>">
                    
                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    
                    <hr>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('name', 'نام')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('email', 'ایمیل')}}
                            <div class="input-group">                                                                      
                                {{ Form::email('xxemail', $user->email, ['class' => 'form-control', 'placeholder' => $user->email, 'disabled'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('password', 'رمزعبور')}}
                            <div class="input-group">                                                                      
                                {{ Form::password('xxpassword', ['class' => 'form-control', 'placeholder' => 'رمزعبور', 'disabled'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('phone', $user->phone, ['class' => 'form-control', 'placeholder' => 'تلفن', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('dob', 'تاریخ تولد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('dob', $user->dob, ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ تولد', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('gender', 'جنسیت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('gender', $user->gender, ['class' => 'form-control', 'placeholder' => 'جنسیت', 'required'] )}}
                            </div>
                        </div>
                    </div>

                    <?php if(Auth::user()->role == "admin"){ ?>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('role', 'انتخاب نقش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('role', ['' => 'انتخاب نقش', 'admin' => 'مدیر', 'supplier' => 'تامین کننده', 'merchant' => 'بازرگان', 'merchant-supplier' => 'تامین کننده و وارد کننده', 'staff' => 'کارمند'], $user->role, ['class' => 'form-control role', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 supplierField" <?php if ($user->supplier) { ?>  style="display: block;" <?php } ?> >
                        <div class="form-group">
                            {{ Form::label('supplier', 'انتخاب تامین کننده')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierslist, $user->supplier, ['class' => 'form-control supplier', 'rquired'] )}}
                            </div>
                        </div>
                    </div> 

                    <div class="col-md-4 merchantField" <?php if ($user->merchant) { ?>  style="display: block;" <?php } ?> >
                        <div class="form-group">
                            {{ Form::label('merchant', 'انتخاب بازرگان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('merchant', $merchantslist, $user->merchant, ['class' => 'form-control merchant', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                    
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div> 

                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-email" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">تغییر ایمیل</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['action' => ['UserController@updateemail', $user->id ], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'PUT')}}

                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('email', 'ایمیل')}}
                                <div class="input-group">                                                                      
                                    {{ Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => $user->email] )}}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">                                
                                <div class="input-group">                                                                      
                                    {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                                </div>
                            </div>
                        </div> 

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        
        <div class="modal fade" id="modal-password" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">تغییر رمزعبور</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['action' => ['UserController@updatepassword', $user->id ], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'PUT')}}
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('password', 'رمزعبور')}}
                                <div class="input-group">                                                                      
                                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'رمزعبور'] )}}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">                                
                                <div class="input-group">                                                                      
                                    {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                                </div>
                            </div>
                        </div> 

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>







    </div>
</section>

<script>

    $(document).ready(function () {

        var role = '';
        $('.role').on('change', function () {
            role = $(".role").val();
            if (role == "supplier") {
                $(".supplierField").show('slow');
                $(".merchantField").hide('slow');
                $(".merchant").val(' ');
            } else if (role == "merchant") {
                $(".supplierField").hide('slow');
                $(".merchantField").show('slow');
                $(".supplier").val(' ');
            } else if (role == "merchant-supplier") {
                $(".supplierField").show('slow');
                $(".merchantField").show('slow');
            }
        });


    });

</script>


@endsection

