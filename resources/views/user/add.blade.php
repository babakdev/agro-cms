@extends('layouts.app')

@section('content')

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )                            
@foreach($merchants as $merchant)
@php 
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach                        
@endif

@php $supplierslist = array(' ' => 'انتخاب تامین کننده'); @endphp
@if(count($suppliers) > 0 )                            
@foreach($suppliers as $supplier)
@php 
$supplierslist[$supplier->id] = $supplier->fname;
@endphp
@endforeach                        
@endif

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  کاربر جدید</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'UserController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('name', 'نام')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('email', 'ایمیل')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'ایمیل', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('password', 'رمزعبور')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'رمزعبور', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'تلفن', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('dob', 'تاریخ تولد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('dob', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ تولد', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('gender', 'جنسیت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('gender', '', ['class' => 'form-control', 'placeholder' => 'جنسیت', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('role', 'انتخاب نقش')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('role', ['' => 'انتخاب نقش', 'admin' => 'مدیر', 'supplier' => 'تامین کننده', 'merchant' => 'بازرگان', 'merchant-supplier' => 'تامین کننده و وارد کننده', 'staff' => 'کارمند'], null, ['class' => 'form-control role', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 supplierField" style="display: none;">
                        <div class="form-group">
                            {{ Form::label('supplier', 'انتخاب تامین کننده')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('supplier', $supplierslist, null, ['class' => 'form-control supplier', 'rquired'] )}}
                            </div>
                        </div>
                    </div> 
                    
                    <div class="col-md-4 merchantField" style="display: none;">
                        <div class="form-group">
                            {{ Form::label('merchant', 'انتخاب بازرگان')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::select('merchant', $merchantslist, null, ['class' => 'form-control merchant', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div> 
                    
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>

<script>

    $(document).ready(function () {
        
        var role = ''; 
        $('.role').on('change', function() {
            role = $(".role").val();
            if(role == "supplier"){
                $(".supplierField").show('slow');
                $(".merchantField").hide('slow');
                $(".merchant").val(' ');
            }else if(role == "merchant"){
                $(".supplierField").hide('slow');
                $(".merchantField").show('slow');
                $(".supplier").val(' ');
            }else if(role == "merchant-supplier"){
                $(".supplierField").show('slow');
                $(".merchantField").show('slow');
            }
        });

        
    });

</script>


@endsection

