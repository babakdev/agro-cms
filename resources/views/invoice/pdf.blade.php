
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>



            .table{
                width:100%
            }
            
            .table tbody tr:nth-of-type(odd) {
                background-color: #f9f9f9;
            }

            th{
                padding: 5px 0;
                font-size: 18px;
                color:#333;
            }

            td{
                padding: 5px 0;
                margin: 0 0 5px 0;
                font-size: 16px;
                color:#333;
            }
            
            p{
                margin: 0 0 5px 0;
                font-size: 16px;
                color:#333;
            }



            .tabletwo{
                width:100%;
            }

            .wrapper{
                margin: 0;
                padding: 25px;
                width: 100%;
            }

            .header{
                margin: 10px 0;
                padding: 0;
                width: 100%;
            }

            .header .left{
                margin: 0;
                padding: 0;
                width: 100%;
                float: left;
            }

            .header .right{
                margin: 0;
                padding: 0;
                width: 100%;
                float: left;
            }

            .upper{
                margin: 10px 0;
                padding: 0;
                width: 100%;
            }

            .upper .one{
                margin: 0;
                padding: 0;
                width: 33%;

            }

            .upper .two{
                margin: 0;
                padding: 0;
                width: 33%;  
            }

            .upper .three{
                margin: 0;
                padding: 0;
                width: 33%;
            }

            .page-break {
                page-break-after: always;
            }

        </style>

    </head>
    <body>

        <div class="wrapper">
            <div class="header"> 
                <h1><u>{{ $title }} | {{ $tag }}</u></h1><br>
                <h1>شماره فاکتور #{{ $invoiceid }}</h1>
                <strong>تاریخ: {{ $date }}</strong><br>
            </div>

            <div class="upper">
                <div class="one">
                    <h3><strong>از طرف</strong></h3>
                    <p>{{ $title }}</p>
                    <p>{{ $address }}</p>
                    <p>{{ $city }}, {{ $country }}, {{ $postal }}</p>
                    <p>تلفن: {{ $phone }}</p>
                    <p>ایمیل: {{ $email }}</p>
                </div><br>
                <div class="two">
                    <h3><strong>به</strong></h3>
                    <p>{{ $merchant }}</p>
                    <p>{{ $maddress }}</p>
                    <p>تلفن: {{ $mphone }}</p>
                    <p>ایمیل: {{ $memail }}</p>
                    <p>فکس: {{ $mfax }}</p>
                </div><br>
                <div class="three">
                    <h3><strong>شماره فاکتور #{{ $invoiceid }}</strong></h3> 
                    <p>شماره سفارش: {{ $orderid }}</p>
                    <p>دلیل پرداخت: {{ $paymentdue }}</p>
                    <p>روش پرداخت: {{ $paymentmethod }}</p>
                    <p>از حساب: {{ $paymentaccount }}</p>
                    <p>وضعیت: <span style="color:red;">پرداخت نشده</span></p>
                </div><br>
            </div>
            <div class="page-break"></div>
            <table class="table">
                <thead>
                    <tr>
                        <th># کد</th>
                        <th>محصول</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
                        <th>کل</th>
                    </tr>
                </thead>
                <tbody>
                    @if($product1 !== " ")
                        {!! html_entity_decode($product1) !!}
                    @endif
                    
                    @if($product2 !== " ")
                    {!! html_entity_decode($product2) !!}
                    @endif
                    
                    @if($product3 !== " ")
                        {!! html_entity_decode($product3) !!}
                    @endif
                    
                    @if($product4 !== " ")
                        {!! html_entity_decode($product4) !!}
                    @endif
                    
                    @if($product5 !== " ")
                        {!! html_entity_decode($product5) !!}
                    @endif
                    
                    @if($product6 !== " ")
                        {!! html_entity_decode($product6) !!}
                    @endif
                    
                    @if($product7 !== " ")
                        {!! html_entity_decode($product7) !!}
                    @endif
                    
                    @if($product8 !== " ")
                        {!! html_entity_decode($product8) !!}
                    @endif
                    
                    @if($product9 !== " ")
                        {!! html_entity_decode($product9) !!}
                    @endif
                    
                    @if($product10 !== " ")
                        {!! html_entity_decode($product10) !!}
                    @endif
                    
                </tbody>
            </table><br>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width: 15%"> </td>
                        <td> </td>
                        <td> </td>
                        <th>کل:</th>
                        <td>{{ $totalfootersubtotal }}</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <th>تخفیف {{ $discountpercent }}%</th>
                        <td>(-) {{ $totaldiscount }}</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <th>مالیات {{ $taxpercent }}%</th>
                        <td>(+) {{ $totaltax }}</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <th>جانبی:</th>
                        <td>(+) {{ $totalother }}</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <th>مبلغ نهایی:</th>
                        <td>(=) {{ $nettotal }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>