@extends('layouts.app')

@section('content')


<style>
    @media print {
        .main-footer{
            display: none;
        }
    }
</style>

<!-- Main content -->

<?php

    $totalSubtotal = array();
    $productIDs = $saleData->product; //Product IDs
    $productQtys = $saleData->quantity; //Product Quantity
    $productPrices = $saleData->price; //Product price

    //Product IDs
    $productArr = explode(",", $productIDs);
    foreach ($productArr as $prokey => $provalue){
        if ($provalue == "") {
            unset($productArr[$prokey]);
        }
    }

    //Product Quantity
    $productQtyArr = explode(",", $productQtys);
    foreach ($productQtyArr as $proQtykey => $proQtyvalue){
        if ($proQtyvalue == "") {
            unset($productQtyArr[$proQtykey]);
        }
    }

    //Product Price
    $productPriceArr = explode(",", $productPrices);
    foreach ($productPriceArr as $proPrikey => $proPrivalue){
        if ($proPrivalue == "") {
            unset($productPriceArr[$proPrikey]);
        }
    }

    foreach ($productArr as $productkey => $product){
        $total[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
    }

    $total = array_sum($total) - $saleData->commission;

?>


<section class="invoice" id="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <img style="width:40px;" src="{{ URL::asset('images/' . Helper::getSettings()->photo)}}" alt="{{ Helper::getSettings()->title }}"> {{ Helper::getSettings()->title }}
                <small class="pull-right">Date: {{ $invoice->invoicedate }}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            From
            <address>
                <strong>{{ Helper::getSettings()->title }}</strong><br>
                {{ Helper::getSettings()->address }}<br>
                {{ Helper::getSettings()->city }}, {{ Helper::getSettings()->country }}, {{ Helper::getSettings()->postal }}<br>
                Phone: {{ Helper::getSettings()->phone }}<br>
                Email: {{ Helper::getSettings()->email }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            To
            <address>
                @if( Helper::getProfileByID($invoice->merchant, 'merchants') !== 0 )
                    <strong>{{ Helper::getProfileByID($invoice->merchant, 'merchants')->fname . " " . Helper::getProfileByID($invoice->merchant, 'merchants')->lname .  " ( " . Helper::getProfileByID($invoice->merchant, 'merchants')->company . " ) " }}</strong><br>
                    @if( Helper::getProfileByID($invoice->merchant, 'merchants')->address )
                    {{ strip_tags(Helper::getProfileByID($invoice->merchant, 'merchants')->address) }}<br>
                    @endif
                    Phone: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->phone }}<br>
                    Email: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->email }}<br>
                    @if( Helper::getProfileByID($invoice->merchant, 'merchants')->fax )
                    Fax: {{ Helper::getProfileByID($invoice->merchant, 'merchants')->fax }}
                    @endif
                @else
                <strong>{{ $invoice->customer }}</strong><br>
                {{ $invoice->customer_phone }}
                @endif

            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Invoice #{{ $invoice->id }}</b><br>
            <b>Order ID:</b> {{ $invoice->orderid }}<br>
            <b>Payment Due Date:</b> {{ $invoice->paymentdue }}<br>
            <b>Payment Method:</b> {{ $invoice->paymentmethod }}<br>
            <b>Account:</b> {{ $invoice->paymentaccount }}<br>
            <b>Status:</b>
            <span class="
            <?php if( $invoice->paymentstatus == "Paid"  && $total == $invoice->paidamount ){
                echo "text-green";
            } elseif($invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount){
                echo "text-yellow";
            } else{
                echo "text-red";
            } ?>">

            <?php if( $invoice->paymentstatus == "Paid"  && $total == $invoice->paidamount ){
                echo $invoice->paymentstatus;
            } elseif( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount){
                echo "Partial Paid";
            }else{
                echo "Unpaid";
            } ?></span><br>

            <b>Due:</b>
            <span class="
            <?php if( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount ){
                echo "text-yellow";
            } ?>">

            <?php if( $invoice->paymentstatus == "Paid"  && $total !== $invoice->paidamount ){
                echo Helper::getCurrency() . " " . number_format($total - $invoice->paidamount, 2);
            }?></span>

            <hr>
            <?php if($invoice->paymentstatus == "Paid"){?>
            <b>Paid Amount:</b> {{ $invoice->paidamount }}<br>
            <b>Paid Method:</b> {{ $invoice->paidmethod }}<br>
            <b>Paid Acc. Info:</b> {{ $invoice->paidinfo }}<br>
            <br><br>
            <?php }?>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th># Code</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

                    <?php



                        // echo "<pre>";
                        //
                        // var_dump($productArr);
                        //
                        // var_dump($productQtyArr);
                        //
                        // var_dump($productPriceArr);
                        //
                        // echo "</pre>";
                        //
                        // echo "<br>";
                        // echo "<br>";


                        foreach ($productArr as $productkey => $product){

                            // echo $productkey . " Product Key";
                            // echo "<br>";
                            // echo "<br>";
                            //
                            // echo $productQtyArr[$productkey]  . " Product Quantity";
                            // echo "<br>";
                            // echo "<br>";
                            //
                            // echo $productPriceArr[$productkey]  . " Product Price";
                            // echo "<br>";
                            // echo "<br>";


                    ?>
                        @if(!empty($product))
                            @if(Helper::getProduct($product) != false)
                                <tr>
                                    <td>{{ Helper::getProduct($product)->id }}</td>
                                    <td>{{ Helper::getProduct($product)->title }}</td>
                                    <td>{{ number_format($productQtyArr[$productkey],2) }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($productPriceArr[$productkey], 2) }}</td>
                                    <td>{{ Helper::getCurrency(). " " . number_format($productPriceArr[$productkey]  * $productQtyArr[$productkey], 2) }}</td>
                                </tr>
                            @endif
                        @endif


                    <?php $totalSubtotal[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey]; } ?>

                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <!-- <p class="lead">Payment Methods:</p>
          <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

            @if($invoice->note)
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                {{ strip_tags($invoice->note) }}
            </p>
            @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!-- <p class="lead">Amount Due {{ $invoice->paymentdue }}</p> -->
          <?php
            $discount = $saleData->commission;
            $lessdiscount = array_sum($totalSubtotal) - $discount;
            $tax = $lessdiscount * $invoice->tax / 100;
            $total = array_sum($totalSubtotal) - $discount + $tax + $invoice->other;
          ?>

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                            <th style="width:50%">Subtotal:</th>

                            <td>{{ Helper::getCurrency(). " " . number_format( array_sum($totalSubtotal) , 2) }}</td>
                        </tr>
                        <tr>
                            <th>Discount {{ $invoice->discount }}</th>
                            <td>(-) {{ Helper::getCurrency(). " " . number_format( $discount, 2 ) }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>(=) {{ Helper::getCurrency(). " " . number_format( $total, 2 ) }}</td>
                        </tr>
                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a id="print" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>

            <!-- <a href="{{ url('invoice', $invoice->id) }}/pdf" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Generate PDF
            </a>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-email">
                <i class="fa fa-envelope"></i> Send Invoice
            </button> -->

            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-payment">
                <i class="fa fa-money"></i> Payment
            </button>

            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-payment-paypal">
                <i class="fa fa-money"></i> Pay Now
            </button>

        </div>
    </div>
</section>


<div class="modal fade" id="modal-payment-paypal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">

                <div class="col-md-8 col-md-offset-2 btmMediumSpace">
                    <h1 class="text-center"><img class="payment-logo"src="{{ asset('images/paypal.jpg')}}" alt="Paypal Logo"></h1>
                    {!! Form::open(['action' => 'PaypalController@store', 'method' => 'POST']) !!}

                    <div class="col-md-12 btmSmallSpace">

                        <div class="form-group">
                            {{ Form::label('', 'Payment Title')}}  <span class="text-red"> (*)</span>
                            <div class="input-group">
                                <input class="form-control" type="hidden" name="id" value="{{ $invoice->id }}" >
                                <input class="form-control" type="hidden" name="title" value="Invoice #{{ $invoice->id }}" >
                                <input class="form-control" type="text" value="Invoice #{{ $invoice->id }}" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('', 'Payment Amount')}}  <span class="text-red"> (*)</span>
                            <div class="input-group">
                                <input class="form-control" type="hidden" name="amount" value="{{ $total }}" >
                                <input class="form-control" type="text" value="{{ $total }}" disabled="">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 btmSmallSpace">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('Pay Now ( ' . Helper::getCurrency(). " " . number_format( $total, 2) . ' )', ['class' => 'btn btn-primary col-md-offset-3 col-md-6'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>

                <div class="col-md-8 col-md-offset-2 btmSmallSpace">

                    <h1 class="text-center"><img class="payment-logo"src="{{ asset('images/stripe.png')}}" alt="Stripe Logo"></h1>

                    <!-- stripe payment form -->
                    {!! Form::open(['action' => 'StripeController@stripe', 'method' => 'POST', 'id' => 'payment-form']) !!}

                        <input type="hidden" name="invoiceid" value="{{ $invoice->id }}">
                        <input type="hidden" name="amount" value="{{ $total }}">
                        <div class="form-row btmMediumSpace">
                          <p for="card-element">Credit or Debit Card</p>
                          <div id="card-element">
                            <!-- a Stripe Element will be inserted here. -->
                          </div>
                          <!-- Used to display form errors -->
                          <div id="card-errors"></div>
                        </div>
                        <button type="submit" class="btn btn-primary col-md-offset-3 col-md-6 payNowBtn">Pay Now ( {{Helper::getCurrency(). " " . number_format( $total, 2)}} )</button>
                    {!! Form::close() !!}

                </div>

                </div>

            <div style="border:0" class="modal-footer"></div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-email">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send PDF To Email</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['action' => 'InvoiceController@sendInvoiceToEmail', 'files' => true, 'method' => 'POST']) !!}
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" name="id" value="{{$invoice->id}}">
                        {{ Form::label('', 'Email Address')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            {{ Form::text('emails', '', ['class' => 'form-control', 'placeholder' => 'Email Address', 'required'] )}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('', 'Email Message')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Email Message', 'required'] )}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-primary'] )}}
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-payment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Manual Payment</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['action' => ['InvoiceController@invoicepayment', $invoice->id ], 'files' => true, 'method' => 'POST']) !!}
                {{Form::hidden('_method', 'PUT')}}

                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('', 'Payment Date')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            {{ Form::date('paymentdate', '', ['class' => 'form-control', 'placeholder' => 'Payment Date', 'required'] )}}
                        </div>
                    </div>

                    <div class="form-group" style="display:none">
                        {{ Form::label('', 'Payment Status')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            <select class="form-control" name="status" required="">
                                <option value=" ">Select Payment Status</option>
                                <option selected value="Paid">Paid</option>
                                <option value="Unpaid">Unpaid</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('', 'Payment Amount')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            {{ Form::text('amount', '', ['class' => 'form-control', 'placeholder' => 'Amount', 'required'] )}}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('', 'Payment Method')}}  <span class="text-red"> (*)</span>
                        <div class="input-group">
                            {{ Form::text('method', '', ['class' => 'form-control', 'placeholder' => 'Payment Method', 'required'] )}}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('', 'Payment Method Info')}}</span>
                        <div class="input-group">
                            {{ Form::text('info', '', ['class' => 'form-control', 'placeholder' => 'Payment Method Info (Acc Number)' ] )}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-primary'] )}}
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

@endsection
