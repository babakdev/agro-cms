@extends('layouts.app')

@section('content')

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )
@foreach($merchants as $merchant)
@php
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach
@endif

@php $productslist = array(' ' => 'انتخاب محصول'); @endphp
@if(count($products) > 0 )
@foreach($products as $product)
@php
$productslist[$product->id] = $product->title;
@endphp
@endforeach
@endif

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  فاکتور جدید</h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => 'InvoiceController@store', 'files' => true, 'method' => 'POST']) !!}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('merchant', 'انتخاب مشتری/بازرگان')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::select('merchant', $merchantslist, null, ['class' => 'form-control', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('invoicedate', 'تاریخ فروش')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::text('invoicedate', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ فروش', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('orderid', 'شماره فاکتور')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::text('orderid', '', ['class' => 'form-control', 'placeholder' => 'شماره فاکتور', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('paymentdue', 'تاریخ پرداخت')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::text('paymentdue', '', ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ پرداخت', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('paymentmethod', 'نحوه پرداخت')}}
                                    <div class="input-group">
                                        {{ Form::text('paymentmethod', '', ['class' => 'form-control', 'placeholder' => 'نحوه پرداخت'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('paymentaccount', 'پرداخت از محل')}}
                                    <div class="input-group">
                                        {{ Form::text('paymentaccount', '', ['class' => 'form-control', 'placeholder' => 'پرداخت از محل'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('tax', 'مالیات %')}}
                                    <div class="input-group">
                                        {{ Form::number('tax', '', ['class' => 'form-control', 'placeholder' => 'مالیات %'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('other', 'هزینه های جانبی')}}
                                    <div class="input-group">
                                        {{ Form::number('other', '', ['class' => 'form-control', 'placeholder' => 'هزینه های جانبی'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('discount', 'تخفیف %')}}
                                    <div class="input-group">
                                        {{ Form::number('discount', '', ['class' => 'form-control', 'placeholder' => 'تخفیف %'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('note', 'توضیح')}}
                                    <div class="input-group">
                                        {{ Form::textarea('note', '', ['class' => 'form-control', 'placeholder' => 'توضیح', 'rows' => 5] )}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product1', 'انتخاب محصول 01')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::select('product1', $productslist, null, ['class' => 'form-control', 'required'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity1', 'تعداد 01')}} <span class="text-red"> (*)</span>
                                    <div class="input-group">
                                        {{ Form::number('quantity1', '', ['class' => 'form-control', 'placeholder' => 'Quantity', 'required'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product2', 'انتخاب محصول 02')}}
                                    <div class="input-group">
                                        {{ Form::select('product2', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity2', 'تعداد 02')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity2', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product3', 'انتخاب محصول 03')}}
                                    <div class="input-group">
                                        {{ Form::select('product3', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity3', 'تعداد 03')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity3', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product4', 'انتخاب محصول 04')}}
                                    <div class="input-group">
                                        {{ Form::select('product4', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity4', 'تعداد 04')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity4', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product5', 'انتخاب محصول 05')}}
                                    <div class="input-group">
                                        {{ Form::select('product5', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity5', 'تعداد 05')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity5', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product6', 'انتخاب محصول 06')}}
                                    <div class="input-group">
                                        {{ Form::select('product6', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity6', 'تعداد 06')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity6', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product7', 'انتخاب محصول 07')}}
                                    <div class="input-group">
                                        {{ Form::select('product7', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity7', 'تعداد 07')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity7', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product8', 'انتخاب محصول 08')}}
                                    <div class="input-group">
                                        {{ Form::select('product8', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity8', 'تعداد 08')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity8', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product9', 'انتخاب محصول 09')}}
                                    <div class="input-group">
                                        {{ Form::select('product9', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity9', 'تعداد 09')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity9', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('add', 'اضافه کن')}}
                                    <div class="input-group">
                                        <div class="addmore btn btn-primary">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="display:none">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('product10', 'انتخاب محصول 10')}}
                                    <div class="input-group">
                                        {{ Form::select('product10', $productslist, null, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ Form::label('quantity10', 'تعداد 10')}}
                                    <div class="input-group">
                                        {{ Form::number('quantity10', '', ['class' => 'form-control', 'placeholder' => 'Quantity'] )}}
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::submit('ثبت', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection
