@extends('layouts.app')

@section('content')

    <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format(Helper::getInvTotalPaid(), 2) }}</h3>
              <p>جمع کل صورتحساب</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format(Helper::getInvTotalDue(), 2) }}</h3>
              <p>صورتحساب موقت جزئی</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box counter-box">
            <div class="inner">
              <h3>{{ Helper::getCurrency(). " " . number_format(Helper::getInvTotalUnpaid()  + Helper::getInvTotalDue(), 2) }}</h3>
              <p>صورتحسابهای پرداخت نشده</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
          </div>
        </div>

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه فاکتورها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtInvoice text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>شماره فاکتور</th>
                                <th>تاریخ</th>
                                <th>بازرگان</th>
                                <th>موعد مقرر</th>
                                <th>وضعیت</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($invoices) > 0 )

                            @foreach($invoices as $invoice)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $invoice->orderid }}</td>
                                    @php
                                    $invoicedate = preg_replace('/\s+/', '', $invoice->invoicedate);
                                    @endphp

                                    @if(strlen($invoicedate) > 1)
                                    <td>{{ Carbon::parse($invoicedate)->format('j M Y') }}</td>
                                    @else
                                    <td>N/A</td>
                                    @endif

                                    <td><a title="Merchant Details" href="{{ url('merchant', $invoice->merchant) }}">{{ Helper::getNameByID($invoice->merchant, 'merchants') }}</a></td>


                                    @php
                                    $paymentdue = preg_replace('/\s+/', '', $invoice->paymentdue);
                                    @endphp
                                    @if(strlen($paymentdue) > 1)
                                    <td>{{ Carbon::parse($paymentdue)->format('j M Y') }}</td>
                                    @else
                                    <td>N/A</td>
                                    @endif


                                    <td>{{ $invoice->paymentstatus }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('invoice', $invoice->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('invoice', $invoice->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('invoice.destroy', $invoice->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
