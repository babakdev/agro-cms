@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="widget-user-header bg-primary">
                    <h3 class="widget-user-username">{{ $merchant->fname }} {{ $merchant->lname }}</h3>
                    <h5 class="widget-user-desc">بازرگان | {{ $merchant->company }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">درباره</a></li>
                                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">آدرس</a></li>
                                <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">تراکنش ها</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">

                                        <?php if($merchant->photo){ ?>
                                            <img src="{{ URL::asset('images/merchant/' . $merchant->photo)}}" alt="User Avatar">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="User Avatar">
                                        <?php } ?>

                                    </div>
                                    <!-- /.widget-user-image -->
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>آی دی/سریال</td>
                                                <td>{{ $merchant->merchantid }}</td>
                                            </tr>
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $merchant->fname }} {{ $merchant->lname }}</td>
                                            </tr>
                                            <tr>
                                                <td>تلفن</td>
                                                <td>{{ $merchant->phone }}</td>
                                            </tr>
                                            <tr>
                                                <td>فکس</td>
                                                <td>{{ $merchant->fax }}</td>
                                            </tr>
                                            <tr>
                                                <td>ایمیل</td>
                                                <td>{{ $merchant->email }}</td>
                                            </tr>
                                            <tr>
                                                <td>نام کارخانه</td>
                                                <td>{{ $merchant->company }}</td>
                                            </tr>
                                            <tr>
                                                <td>مجوز تجارت</td>
                                                <td>{{ $merchant->tradelicense }}</td>
                                            </tr>
                                            <tr>
                                                <td>لایسنس های دیگر</td>
                                                <td>{{ $merchant->otherlicense }}</td>
                                            </tr>
                                            <tr>
                                                <td>شبکه های اجتماعی</td>
                                                <td>
                                                    <a class="btn btn-social-icon btn-facebook" href="{{ $merchant->facebook }}"><i class="fa fa-facebook"></i></a>
                                                    <a class="btn btn-social-icon btn-twitter" href="{{ $merchant->twitter }}"><i class="fa fa-twitter"></i></a>
                                                    <a class="btn btn-social-icon btn-google" href="{{ $merchant->youtube }}"><i class="fa fa-youtube"></i></a>
                                                </td>
                                            </tr>

                                        </tbody></table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {!! $merchant->about !!}
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    {!! $merchant->address !!}
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_4">
                                    <table class="table table-bordered table-hover dtProduct text-center">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>تاریخ</th>
                                                <th>محصول</th>
                                                <th>بازرگان</th>
                                                <th>تعداد</th>
                                                <th>کل مبلغ</th>
                                                <th>پرداختی</th>
                                                <th>ناشی از</th>
                                                <th style="width: 20%">عملیات</th>
                                            </tr>
                                        </thead>
                                        <tbody
                                            @if(count($sales) > 0 )

                                            @foreach($sales as $sale)

                                            @php
                                            $totalSubtotal = array();
                                            $productIDs = $sale->product; //Product IDs
                                            $productQtys = $sale->quantity; //Product Quantity
                                            $productPrices = $sale->price; //Product price

                                            //Product IDs
                                            $productArr = explode(",", $productIDs);
                                            foreach ($productArr as $prokey => $provalue){
                                                if ($provalue == "") {
                                                    unset($productArr[$prokey]);
                                                }
                                            }

                                            //Product Quantity
                                            $productQtyArr = explode(",", $productQtys);
                                            foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                                                if ($proQtyvalue == "") {
                                                    unset($productQtyArr[$proQtykey]);
                                                }
                                            }

                                            //Product Price
                                            $productPriceArr = explode(",", $productPrices);
                                            foreach ($productPriceArr as $proPrikey => $proPrivalue){
                                                if ($proPrivalue == "") {
                                                    unset($productPriceArr[$proPrikey]);
                                                }
                                            }

                                            foreach ($productArr as $productkey => $product){
                                                $totalSubtotal[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                                            }

                                            $totalSubtotal = array_sum($totalSubtotal);


                                            @endphp

                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ Carbon::parse($sale->saledate)->format('j M Y') }}</td>
                                                    <td>

                                                            <?php for($sq=0; count($productArr) > $sq; $sq++){
                                                                if(!empty($productArr[$sq])){ ?>
                                                                    <a title="جزئیات محصول" href="{{ url('product', $productArr[$sq]) }}">{{ Helper::getProductTitle( $productArr[$sq] ) }}</a><br>
                                                                <?php }
                                                                if($sq === 1){ ?>
                                                                    <a title="محصولات بیشتر" href="{{ url('sale', $sale->id) }}">...</a><br>
                                                                <?php break;
                                                                }
                                                            } ?>

                                                    </td>
                                                    <td><a title="Merchant Details" href="{{ url('merchant', $sale->merchant) }}">{{ Helper::getNameByID($sale->merchant, 'merchants') }}</a></td>
                                                    <td>{{ number_format(array_sum($productQtyArr), 2) }}</td>
                                                    <td><strong>{{ number_format($totalSubtotal - $sale->commission, 2)  }}</strong></td>

                                                    <?php if( Helper::invoiceBySale($sale->id) != false ){ ?>
                                                        <td><strong> <?php if( Helper::invoiceBySale($sale->id)){ echo number_format(Helper::invoiceBySale($sale->id)->paidamount); } ?></strong></td>
                                                    <?php if( Helper::invoiceBySale($sale->id)->paidamount < ($totalSubtotal - $sale->commission) ){ ?>
                                                        <td><strong> {{ number_format( Helper::invoiceBySale($sale->id)->paidamount - ($totalSubtotal - $sale->commission)) }}</strong></td>
                                                    <?php }else{ ?>
                                                        <td>N/A</td>
                                                    <?php } }else{ ?>
                                                            <td>N/A</td>
                                                            <td>N/A</td>
                                                    <?php } ?>
                                                    
                                                    <td>
                                                        <span class="badge bg-blue"><a href="{{ url('purchase', $sale->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                                        <span class="badge bg-yellow"><a href="{{ url('purchase', $sale->id) }}/edit/" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                                        <form action="{{ route('purchase.destroy', $sale->id) }}" method="POST">
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                                        </form>
                                                    </td>
                                                </tr>

                                            @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection
