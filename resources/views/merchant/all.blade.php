@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">همه بازرگانان/واردکنندگان</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtMerchant text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>آی دی</th>
                                <th>نام</th>
                                <th>تلفن</th>
                                <th>ایمیل</th>
                                <th>کارخانه</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($merchants) > 0 )

                            @foreach($merchants as $merchant)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $merchant->merchantid }}</td>
                                    <td>{{ $merchant->fname }} {{ $merchant->lname }}</td>
                                    <td>{{ $merchant->phone }}</td>
                                    <td>{{ $merchant->email }}</td>
                                    <td>{{ $merchant->company }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('merchant', $merchant->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('merchant', $merchant->id) }}/edit" title="ویرایش"><i class="material-icons md-12">create</i></a></span>
                                        <form action="{{ route('merchant.destroy', $merchant->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
