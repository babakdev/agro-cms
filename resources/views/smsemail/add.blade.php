@extends('layouts.app')

@section('content')

@php $merchantslist = array(' ' => 'انتخاب بازرگان'); @endphp
@if(count($merchants) > 0 )                            
@foreach($merchants as $merchant)
@php 
$merchantslist[$merchant->id] = $merchant->fname;
@endphp
@endforeach                        
@endif

@php $supplierslist = array(' ' => 'انتخاب تامین کننده'); @endphp
@if(count($suppliers) > 0 )                            
@foreach($suppliers as $supplier)
@php 
$supplierslist[$supplier->id] = $supplier->fname;
@endphp
@endforeach                        
@endif

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"> ارسال پیامک / ایمیل</h3>
                </div>
                <div class="box-body">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#singlesms" data-toggle="tab" aria-expanded="false">پیامک با شماره</a></li>
                            <li><a href="#multiplesms" data-toggle="tab" aria-expanded="false">پیامک گروهی</a></li>
                            <li><a href="#singleemail" data-toggle="tab" aria-expanded="false">ایمیل به</a></li>
                            <li><a href="#multipleemail" data-toggle="tab" aria-expanded="false">ایمیل گروهی</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="singlesms">
                                <div class="box-body">
                                    
                                    {!! Form::open(['action' => 'SmsemailController@smsbynumbers', 'method' => 'POST']) !!}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('phone', 'شماره موبایل')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'شماره موبایل', 'title' => 'Phone Numbers With Country Code', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('message', 'پیامک')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'پیامک', 'maxlength' => '160', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">                                
                                            <div class="input-group">                                                                      
                                                {{ Form::submit('ارسال', ['class' => 'btn btn-primary'] )}}
                                            </div>
                                        </div>
                                    </div> 

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            
                            <div class="tab-pane" id="multiplesms">
                                <div class="box-body">
                                    
                                    {!! Form::open(['action' => 'SmsemailController@smsbygroup', 'method' => 'POST']) !!}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('', 'یک مدل را برای ارسال انتخاب نمایید')}} <span class="text-red"> (*)</span><br><br>
                                            <label class="radio-inline"><input checked class="multismstype" type="radio" name="multismstype" value="Supplier">تامین کننده</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multismstype" value="Merchant">بازرگان</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multismstype" value="Staff">کارمند</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multismstype" value="User">کاربر</label>
                                        </div>
                                    </div><br><br>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('message', 'پیامک')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'متن پیامک', 'maxlength' => '160', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">                                
                                            <div class="input-group">                                                                      
                                                {{ Form::submit('ارسال', ['class' => 'btn btn-primary'] )}}
                                            </div>
                                        </div>
                                    </div> 

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            
                            
                             <div class="tab-pane" id="singleemail">
                                <div class="box-body">
                                    
                                    {!! Form::open(['action' => 'SmsemailController@emailbyemails', 'method' => 'POST']) !!}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('email', 'ایمیل')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'ایمیل', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('message', 'متن ایمیل')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'متن ایمیل', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">                                
                                            <div class="input-group">                                                                      
                                                {{ Form::submit('ارسال', ['class' => 'btn btn-primary'] )}}
                                            </div>
                                        </div>
                                    </div> 

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            
                            <div class="tab-pane" id="multipleemail">
                                <div class="box-body">
                                    
                                    {!! Form::open(['action' => 'SmsemailController@emailbygroups', 'method' => 'POST']) !!}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('', 'یک مدل را برای ارسال انتخاب نمایید')}} <span class="text-red"> (*)</span><br><br>
                                            <label class="radio-inline"><input checked class="multismstype" type="radio" name="multiemailtype" value="Supplier">تامین کننده</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multiemailtype" value="Merchant">بازرگان</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multiemailtype" value="Staff">کارمند</label>
                                            <label class="radio-inline"><input class="multismstype" type="radio" name="multiemailtype" value="User">کاربر</label>
                                        </div>
                                    </div><br><br>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::label('message', 'پیامک')}} <span class="text-red"> (*)</span>
                                            <div class="input-group">                                                                      
                                                {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'متن', 'maxlength' => '160', 'required'] )}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">                                
                                            <div class="input-group">                                                                      
                                                {{ Form::submit('ارسال', ['class' => 'btn btn-primary'] )}}
                                            </div>
                                        </div>
                                    </div> 

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            
                            
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>

    $(document).ready(function () {

        var role = '';
        $('.role').on('change', function () {
            role = $(".role").val();
            if (role == "supplier") {
                $(".supplierField").show('slow');
                $(".merchantField").hide('slow');
                $(".merchant").val(' ');
            } else if (role == "merchant") {
                $(".supplierField").hide('slow');
                $(".merchantField").show('slow');
                $(".supplier").val(' ');
            } else if (role == "merchant-supplier") {
                $(".supplierField").show('slow');
                $(".merchantField").show('slow');
            }
        });


    });
    
    
    $(document).ready(function () {
        
        var role = ''; 
        $('.role').on('change', function() {
            role = $(".role").val();
            if(role == "supplier"){
                $(".supplierField").show('slow');
                $(".merchantField").hide('slow');
                $(".merchant").val(' ');
            }else if(role == "merchant"){
                $(".supplierField").hide('slow');
                $(".merchantField").show('slow');
                $(".supplier").val(' ');
            }else if(role == "merchant-supplier"){
                $(".supplierField").show('slow');
                $(".merchantField").show('slow');
            }
        });

        
    });

</script>


@endsection

