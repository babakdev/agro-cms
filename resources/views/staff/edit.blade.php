
@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">            
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">  ویرایش اطلاعات کارمند</h3>
                    <h3 class="box-title left col-sm-2">
                        <button type="button" class="btn btn-primary photo_input float-left">انتخاب تصویر - <i class="material-icons md-18">add_a_photo</i></button>
                    </h3>
                </div>
                <div class="box-body">

                    {!! Form::open(['action' => ['StaffController@update', $staff->id ], 'files' => true, 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'PUT')}}
                    
                    <div class="col-md-12 image-holder-col">
                        <div class="form-group">                            
                            <div class="input-group"> 
                                <div id="image-holder"></div>
                                {{Form::file('photo', ['id' => 'photo', 'class' => 'form-control-file'])}}   
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('fname', 'نام')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('fname', $staff->fname, ['class' => 'form-control', 'placeholder' => 'نام', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('lname', 'نام خانوادگی')}}
                            <div class="input-group">                                                                      
                                {{ Form::text('lname', $staff->lname, ['class' => 'form-control', 'placeholder' => 'نام خانوادگی'] )}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('phone', 'تلفن')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('phone', $staff->phone, ['class' => 'form-control', 'placeholder' => 'تلفن', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('position', 'عنوان شغلی')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                      
                                {{ Form::text('position', $staff->position, ['class' => 'form-control', 'placeholder' => 'عنوان شغلی', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('dob', 'تاریخ تولد')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('dob', $staff->dob, ['class' => 'form-control datepicker', 'placeholder' => 'تاریخ تولد', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('gender', 'جنسیت')}} <span class="text-red"> (*)</span>
                            <div class="input-group">                                                                  
                                {{ Form::text('gender', $staff->gender, ['class' => 'form-control', 'placeholder' => 'جنسیت', 'required'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('education', 'سطح تحصیلات')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('education', $staff->education, ['class' => 'form-control', 'placeholder' => 'سطح تحصیلات'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('height', 'شماره بیمه')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('height', $staff->height, ['class' => 'form-control', 'placeholder' => 'شماره بیمه'] )}}
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('weight', 'Weight')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('weight', $staff->weight, ['class' => 'form-control', 'placeholder' => 'Weight'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('nationality', 'Nationality')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('nationality', $staff->nationality, ['class' => 'form-control', 'placeholder' => 'Nationality'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('bloood', 'Blood Group')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('blood', $staff->blood, ['class' => 'form-control', 'placeholder' => 'Blood Group'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('religion', 'Religion')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('religion', $staff->religion, ['class' => 'form-control', 'placeholder' => 'Religion'] )}}
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('marital', 'وضعیت تاهل')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('marital', $staff->marital, ['class' => 'form-control', 'placeholder' => 'وضعیت تاهل'] )}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('nid', 'کدملی')}}
                            <div class="input-group">                                                                  
                                {{ Form::text('nid', $staff->nid, ['class' => 'form-control', 'placeholder' => 'کدملی'] )}}
                            </div>
                        </div>
                    </div>
                    
                                        
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('address', 'آدرس')}}
                            <div class="input-group">                                                                      
                                {{ Form::textarea('address', $staff->address, ['class' => 'form-control', 'placeholder' => 'آدرس', 'rows' => 5] )}}
                            </div>
                        </div>
                    </div> 
                    
                    <hr>

                    <div class="col-md-12">
                        <div class="form-group">                                
                            <div class="input-group">                                                                      
                                {{ Form::submit('ویرایش', ['class' => 'btn btn-primary'] )}}
                            </div>
                        </div>
                    </div>                        



                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

