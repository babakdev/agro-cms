@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 box-success">

                <div class="widget-user-header bg-primary">

                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username">{{ $staff->fname }} {{ $staff->lname }}</h3>
                    <h5 class="widget-user-desc">کارمند | {{ $staff->position }}</h5>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">اصلی</a></li>
                                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">آدرس</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="profile widget-user-image">
                                        
                                        <?php if($staff->photo){ ?>
                                            <img src="{{ URL::asset('images/staff/' . $staff->photo)}}" alt="Staff Photo">
                                        <?php } else{ ?>
                                            <img src="{{ URL::asset('images/default.png')}}" alt="Staff Photo">
                                        <?php } ?>
                                            
                                    </div>

                                    <table class="table table-bordered">
                                        <tbody>                                            
                                            <tr>
                                                <td>نام</td>
                                                <td>{{ $staff->fname . " " . $staff->lname }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تلفن</td>
                                                <td>{{ $staff->phone }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>تاریخ تولد</td>
                                                <td>{{ $staff->dob }}</td>                                                
                                            </tr>
                                            <tr>
                                                <td>جنسیت</td>
                                                <td>{{ $staff->gender }}</td>
                                            </tr>
                                            <tr>
                                                <td>سطح تحصیلات</td>
                                                <td>{{ $staff->education }}</td>
                                            </tr>
                                            <tr>
                                                <td>شماره بیمه</td>
                                                <td>{{ $staff->height }}</td>
                                            </tr>
                                            {{-- <tr>
                                                <td>Weight</td>
                                                <td>{{ $staff->weight }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nationality</td>
                                                <td>{{ $staff->nationality }}</td>
                                            </tr>
                                            <tr>
                                                <td>Blood</td>
                                                <td>{{ $staff->blood }}</td>
                                            </tr>                                            
                                            <tr>
                                                <td>Religion</td>
                                                <td>{{ $staff->religion }}</td>
                                            </tr> --}}
                                            <tr>
                                                <td>وضعیت تاهل</td>
                                                <td>{{ $staff->marital }}</td>
                                            </tr>
                                            <tr>
                                                <td>کدملی</td>
                                                <td>{{ $staff->nid }}</td>
                                            </tr>                         
                                        </tbody></table>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    {!! $staff->address !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection