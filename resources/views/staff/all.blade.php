@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">کارمندان</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dtStaff text-center">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام</th>
                                <th>تلفن</th>
                                <th>جنسیت</th>
                                <th>تحصیلات</th>
                                <th>شغل</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody
                            @if(count($staffs) > 0 )

                            @foreach($staffs as $staff)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $staff->fname . " " . $staff->lname }}</td>
                                    <td>{{ $staff->phone }}</td>
                                    <td>{{ $staff->gender }}</td>
                                    <td>{{ $staff->education }}</td>
                                    <td>{{ $staff->position }}</td>
                                    <td>
                                        <span class="badge bg-blue"><a href="{{ url('staff', $staff->id) }}" title="نمایش"><i class="material-icons md-12">call_made</i></a></span>
                                        <span class="badge bg-yellow"><a href="{{ url('staff', $staff->id) }}/edit" title="ویرایش"><i class="material-icons md-12">create</i></a></span>

                                        <form action="{{ route('staff.destroy', $staff->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <span><button class="badge bg-red delete" title="حذف"><i class="material-icons md-12">close</i></button></span>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
