<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use App\Payments;
use App\Invoices;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller {

    private $_api_context;

    public function __construct() {
        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    public function store(Request $request) {
        
        $invoicetitle =  $request->title;
        $invoiceAmount = $request->amount;        
        $invoiceID = $request->id;        
        $systemCurrency = \App\Helpers\Helper::getSettings()->currency;        
//        $convertedAmount = $this->convertCurrency((float)$invoiceAmount, $systemCurrency, "USD");
        $convertedAmount = $invoiceAmount;
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName($invoicetitle)// item name
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($convertedAmount);
        $item_list = new ItemList();
        $item_list->setItems([$item]);

        $amount = new Amount();
        $amount->setCurrency('USD')
                ->setTotal($convertedAmount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription($invoicetitle);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url('/payment/paypal/status'))
                ->setCancelUrl(url('/payment/paypal/status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $ex) {
            return redirect('/')->with('error', 'Something Went wrong, funds could not be loaded');
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        
        \Session()->put('paypal_payment_id', $payment->getId());
        \Session()->put('invoice_id', $invoiceID);

        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }
        
        return redirect('/')->with('error', 'Unknown error occurred');
        
    }

    
    public function getPaymentStatus(Request $request) {
        $payment_id = Session()->get('paypal_payment_id');
        $invoice_id = Session()->get('invoice_id');
        Session()->forget('paypal_payment_id');

        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            return redirect('/')->with('error', 'Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);        
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') { // payment made
                        
            $paymentID = $result->getId();
            $paymentMethod = $result->getPayer()->getPaymentMethod();
            $payerEmail = $result->getPayer()->getPayerInfo()->getEmail();
            $payerFName = $result->getPayer()->getPayerInfo()->getFirstName();
            $payerLName = $result->getPayer()->getPayerInfo()->getLastName();
            $payerID = $result->getPayer()->getPayerInfo()->getPayerId();
            
            $payAmount = $result->getTransactions()[0]->getAmount()->getTotal();
            $payCurrency = $result->getTransactions()[0]->getAmount()->getCurrency();
            
            $paymentsDB = new Payments();
            $paymentsDB->invoiceid = $invoice_id;
            $paymentsDB->paymentid = $paymentID;
            $paymentsDB->paymentmethod = $paymentMethod;
            $paymentsDB->payeremail = $payerEmail;
            $paymentsDB->payerfname = $payerFName;
            $paymentsDB->payerlname = $payerLName;
            $paymentsDB->payerid = $payerID;
            $paymentsDB->amount = $payAmount;
            $paymentsDB->currency = $payCurrency;
            $paymentsDB->Save();
                       
            $invoice = Invoices::find($invoice_id);
            $invoice->paymentstatus = "Paid";
            $invoice->paidamount = $payAmount;
            $invoice->paidmethod = $paymentMethod;
            $invoice->paidinfo = $payerEmail . " (" . $payerID .  ")";
            $invoice->Save();
            
            return redirect('/')->with('success', 'Payment Successfully');
        }
        
        return redirect('/')->with('error', 'Unexpected error occurred & payment has been failed.');
        
    }
    
    public function convertCurrency($amount, $from, $to){
        $data = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");
        preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return number_format(round($converted, 3),2);
    }

}
