<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use App\Merchant;
use App\Pcategories;
use App\Product;
use App\Sale;
use App\Invoices;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SaleController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        return view('sale.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        return view('sale.add')->with($data);
    }

    public function all() {
        $data['sales'] = Sale::orderBy('id', 'desc')->get();
        $data['products'] = Product::all();
        return view('sale.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'product1' => 'required',
            'saledate' => 'required',
            'merchant' => 'required',
        ]);

        $sale = new Sale;

        // All PRODUCT Start

        $product1 = $request->product1;
        $product2 = $request->product2;
        $product3 = $request->product3;
        $product4 = $request->product4;
        $product5 = $request->product5;
        $product6 = $request->product6;
        $product7 = $request->product7;
        $product8 = $request->product8;
        $product9 = $request->product9;
        $product10 = $request->product10;
        $product11 = $request->product11;
        $product12 = $request->product12;
        $product13 = $request->product13;
        $product14 = $request->product14;
        $product15 = $request->product15;
        $product16 = $request->product16;
        $product17 = $request->product17;
        $product18 = $request->product18;
        $product19 = $request->product19;
        $product20 = $request->product20;
        $product21 = $request->product21;
        $product22 = $request->product22;
        $product23 = $request->product23;
        $product24 = $request->product24;
        $product25 = $request->product25;
        $product26 = $request->product26;
        $product27 = $request->product27;
        $product28 = $request->product28;
        $product29 = $request->product29;
        $product30 = $request->product30;
        $product31 = $request->product31;
        $product32 = $request->product32;
        $product33 = $request->product33;
        $product34 = $request->product34;
        $product35 = $request->product35;
        $product36 = $request->product36;
        $product37 = $request->product37;
        $product38 = $request->product38;
        $product39 = $request->product39;
        $product40 = $request->product40;
        $product41 = $request->product41;
        $product42 = $request->product42;
        $product43 = $request->product43;
        $product44 = $request->product44;
        $product45 = $request->product45;
        $product46 = $request->product46;
        $product47 = $request->product47;
        $product48 = $request->product48;
        $product49 = $request->product49;
        $product50 = $request->product50;

         // State All Quantity

        $quantity1 = $request->quantity1;
        $quantity2 = $request->quantity2;
        $quantity3 = $request->quantity3;
        $quantity4 = $request->quantity4;
        $quantity5 = $request->quantity5;
        $quantity6 = $request->quantity6;
        $quantity7 = $request->quantity7;
        $quantity8 = $request->quantity8;
        $quantity9 = $request->quantity9;
        $quantity10 = $request->quantity10;
        $quantity11 = $request->quantity11;
        $quantity12 = $request->quantity12;
        $quantity13 = $request->quantity13;
        $quantity14 = $request->quantity14;
        $quantity15 = $request->quantity15;
        $quantity16 = $request->quantity16;
        $quantity17 = $request->quantity17;
        $quantity18 = $request->quantity18;
        $quantity19 = $request->quantity19;
        $quantity20 = $request->quantity20;
        $quantity21 = $request->quantity21;
        $quantity22 = $request->quantity22;
        $quantity23 = $request->quantity23;
        $quantity24 = $request->quantity24;
        $quantity25 = $request->quantity25;
        $quantity26 = $request->quantity26;
        $quantity27 = $request->quantity27;
        $quantity28 = $request->quantity28;
        $quantity29 = $request->quantity29;
        $quantity30 = $request->quantity30;
        $quantity31 = $request->quantity31;
        $quantity32 = $request->quantity32;
        $quantity33 = $request->quantity33;
        $quantity34 = $request->quantity34;
        $quantity35 = $request->quantity35;
        $quantity36 = $request->quantity36;
        $quantity37 = $request->quantity37;
        $quantity38 = $request->quantity38;
        $quantity39 = $request->quantity39;
        $quantity40 = $request->quantity40;
        $quantity41 = $request->quantity41;
        $quantity42 = $request->quantity42;
        $quantity43 = $request->quantity43;
        $quantity44 = $request->quantity44;
        $quantity45 = $request->quantity45;
        $quantity46 = $request->quantity46;
        $quantity47 = $request->quantity47;
        $quantity48 = $request->quantity48;
        $quantity49 = $request->quantity49;
        $quantity50 = $request->quantity50;

         // Start All Price

        $price1 = $request->price1;
        $price2 = $request->price2;
        $price3 = $request->price3;
        $price4 = $request->price4;
        $price5 = $request->price5;
        $price6 = $request->price6;
        $price7 = $request->price7;
        $price8 = $request->price8;
        $price9 = $request->price9;
        $price10 = $request->price10;
        $price11 = $request->price11;
        $price12 = $request->price12;
        $price13 = $request->price13;
        $price14 = $request->price14;
        $price15 = $request->price15;
        $price16 = $request->price16;
        $price17 = $request->price17;
        $price18 = $request->price18;
        $price19 = $request->price19;
        $price20 = $request->price20;
        $price21 = $request->price21;
        $price22 = $request->price22;
        $price23 = $request->price23;
        $price24 = $request->price24;
        $price25 = $request->price25;
        $price26 = $request->price26;
        $price27 = $request->price27;
        $price28 = $request->price28;
        $price29 = $request->price29;
        $price30 = $request->price30;
        $price31 = $request->price31;
        $price32 = $request->price32;
        $price33 = $request->price33;
        $price34 = $request->price34;
        $price35 = $request->price35;
        $price36 = $request->price36;
        $price37 = $request->price37;
        $price38 = $request->price38;
        $price39 = $request->price39;
        $price40 = $request->price40;
        $price41 = $request->price41;
        $price42 = $request->price42;
        $price43 = $request->price43;
        $price44 = $request->price44;
        $price45 = $request->price45;
        $price46 = $request->price46;
        $price47 = $request->price47;
        $price48 = $request->price48;
        $price49 = $request->price49;
        $price50 = $request->price50;

         // Start All Total

        $total1 = $request->total1;
        $total2 = $request->total2;
        $total3 = $request->total3;
        $total4 = $request->total4;
        $total5 = $request->total5;
        $total6 = $request->total6;
        $total7 = $request->total7;
        $total8 = $request->total8;
        $total9 = $request->total9;
        $total10 = $request->total10;
        $total11 = $request->total11;
        $total12 = $request->total12;
        $total13 = $request->total13;
        $total14 = $request->total14;
        $total15 = $request->total15;
        $total16 = $request->total16;
        $total17 = $request->total17;
        $total18 = $request->total18;
        $total19 = $request->total19;
        $total20 = $request->total20;
        $total21 = $request->total21;
        $total22 = $request->total22;
        $total23 = $request->total23;
        $total24 = $request->total24;
        $total25 = $request->total25;
        $total26 = $request->total26;
        $total27 = $request->total27;
        $total28 = $request->total28;
        $total29 = $request->total29;
        $total30 = $request->total30;
        $total31 = $request->total31;
        $total32 = $request->total32;
        $total33 = $request->total33;
        $total34 = $request->total34;
        $total35 = $request->total35;
        $total36 = $request->total36;
        $total37 = $request->total37;
        $total38 = $request->total38;
        $total39 = $request->total39;
        $total40 = $request->total40;
        $total41 = $request->total41;
        $total42 = $request->total42;
        $total43 = $request->total43;
        $total44 = $request->total44;
        $total45 = $request->total45;
        $total46 = $request->total46;
        $total47 = $request->total47;
        $total48 = $request->total48;
        $total49 = $request->total49;
        $total50 = $request->total50;

        $productIDs = $product1 . "," . $product2 . "," . $product3 . "," . $product4 . "," . $product5 . "," . $product6 . "," . $product7 . "," . $product8 . "," . $product9 . "," . $product10 . "," . $product11 . "," . $product12 . "," . $product13 . "," . $product14 . "," . $product15 . "," . $product16 . "," . $product17 . "," . $product18 . "," . $product19 . "," . $product20. "," . $product21 . "," . $product22 . "," . $product23 . "," . $product24 . "," . $product25 . "," . $product26 . "," . $product27 . "," . $product28 . "," . $product29 . "," . $product31 . "," . $product32 . "," . $product33 . "," . $product34 . "," . $product35 . "," . $product36 . "," . $product37 . "," . $product38 . "," . $product39 . "," . $product40 . "," . $product41 . "," . $product42 . "," . $product43 . "," . $product44 . "," . $product45 . "," . $product46 . "," . $product47 . "," . $product48 . "," . $product49 . "," . $product50 ;
        $quantity = $quantity1 . "," . $quantity2 . "," . $quantity3 . "," . $quantity4 . "," . $quantity5 . "," . $quantity6 . "," . $quantity7 . "," . $quantity8 . "," . $quantity9 . "," . $quantity10 . "," . $quantity11 . "," . $quantity12 . "," . $quantity13 . "," . $quantity14 . "," . $quantity15 . "," . $quantity16 . "," . $quantity17 . "," . $quantity18 . "," . $quantity19 . "," . $quantity20 . "," . $quantity21 . "," . $quantity22 . "," . $quantity23 . "," . $quantity24 . "," . $quantity25 . "," . $quantity26 . "," . $quantity27 . "," . $quantity28 . "," . $quantity29 . "," . $quantity30 . "," . $quantity31 . "," . $quantity32 . "," . $quantity33 . "," . $quantity34 . "," . $quantity35 . "," . $quantity36 . "," . $quantity37 . "," . $quantity38 . "," . $quantity39 . "," . $quantity40 . "," . $quantity41 . "," . $quantity42 . "," . $quantity43 . "," . $quantity44 . "," . $quantity45 . "," . $quantity46 . "," . $quantity47 . "," . $quantity48 . "," . $quantity49 . "," . $quantity50 ;

        $price = $price1 . "," . $price2 . "," . $price3 . "," . $price4 . "," . $price5 . "," . $price6 . "," . $price7 . "," . $price8 . "," . $price9 . "," . $price10 . "," . $price11 . "," . $price12 . "," . $price13 . "," . $price14 . "," . $price15 . "," . $price16 . "," . $price17 . "," . $price18 . "," . $price19 . "," . $price20 . "," . $price21 . "," . $price22 . "," . $price23 . "," . $price24 . "," . $price25 . "," . $price26 . "," . $price27 . "," . $price28 . "," . $price29 . "," . $price30 . "," . $price31 . "," . $price32 . "," . $price33 . "," . $price36 . "," . $price37 . "," . $price38 . "," . $price39 . "," . $price40 . "," . $price41 . "," . $price42 . "," . $price43 . "," . $price44 . "," . $price45 . "," . $price46 . "," . $price47 . "," . $price48 . "," . $price49 . "," . $price50 ;

        $total = $total1 . "," . $total2 . "," . $total3 . "," . $total4 . "," . $total5 . "," . $total6 . "," . $total7 . "," . $total8 . "," . $total9 . "," . $total10 . "," . $total11 . "," . $total12 . "," . $total13 . "," . $total14 . "," . $total15 . "," . $total16 . "," . $total17 . "," . $total18 . "," . $total19 . "," . $total20 . "," . $total21 . "," . $total22 . "," . $total23 . "," . $total24 . "," . $total25 . "," . $total26 . "," . $total27 . "," . $total28 . "," . $total29 . "," . $total30 . "," . $total31 . "," . $total32 . "," . $total33 . "," . $total34 . "," . $total35 . "," . $total36 . "," . $total37 . "," . $total38 . "," . $total39 . "," . $total40 . "," . $total41 . "," . $total42 . "," . $total43 . "," . $total44 . "," . $total45 . "," . $total46 . "," . $total47 . "," . $total48 . "," . $total49 . "," . $total50 ;


        $sale->product = $productIDs;
        $sale->description = $request->description;
        $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
        $sale->category = "";
        $sale->merchant = $request->merchant;
        $sale->unit = 0;
        $sale->quantity = $quantity;
        $sale->price = $price;
        $sale->commission = $request->commission;
        $sale->total = 0;
        $sale->save();

        $latestSale = Sale::latest('id')->first();

        $invoice = new Invoices;
        $invoice->saleid = $latestSale->id;
        $invoice->merchant = $request->merchant;
        $invoice->invoicedate = $request->saledate;
        $invoice->orderid = Carbon::parse($request->saledate)->format('Y-m-d') . "-" . rand(10, 99);
        $invoice->paymentdue = "";
        $invoice->product1 = "";
        $invoice->quantity1 = 0;
        $invoice->save();

        return redirect('/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['sale'] = Sale::findOrFail($id);
        $data['invoice'] = Invoices::where('saleid', $id)->value('id');
//        $data['invoice'] = Invoices::findOrFail($id);
        return view('sale.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['sale'] = Sale::findOrFail($id);
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        return view('sale.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'product1' => 'required',
            'saledate' => 'required',
            'merchant' => 'required',
        ]);

        $sale = Sale::find($id);

        // All PRODUCT Start

        $product1 = $request->product1;
        $product2 = $request->product2;
        $product3 = $request->product3;
        $product4 = $request->product4;
        $product5 = $request->product5;
        $product6 = $request->product6;
        $product7 = $request->product7;
        $product8 = $request->product8;
        $product9 = $request->product9;
        $product10 = $request->product10;
        $product11 = $request->product11;
        $product12 = $request->product12;
        $product13 = $request->product13;
        $product14 = $request->product14;
        $product15 = $request->product15;
        $product16 = $request->product16;
        $product17 = $request->product17;
        $product18 = $request->product18;
        $product19 = $request->product19;
        $product20 = $request->product20;
        $product21 = $request->product21;
        $product22 = $request->product22;
        $product23 = $request->product23;
        $product24 = $request->product24;
        $product25 = $request->product25;
        $product26 = $request->product26;
        $product27 = $request->product27;
        $product28 = $request->product28;
        $product29 = $request->product29;
        $product30 = $request->product30;
        $product31 = $request->product31;
        $product32 = $request->product32;
        $product33 = $request->product33;
        $product34 = $request->product34;
        $product35 = $request->product35;
        $product36 = $request->product36;
        $product37 = $request->product37;
        $product38 = $request->product38;
        $product39 = $request->product39;
        $product40 = $request->product40;
        $product41 = $request->product41;
        $product42 = $request->product42;
        $product43 = $request->product43;
        $product44 = $request->product44;
        $product45 = $request->product45;
        $product46 = $request->product46;
        $product47 = $request->product47;
        $product48 = $request->product48;
        $product49 = $request->product49;
        $product50 = $request->product50;

         // State All Quantity

        $quantity1 = $request->quantity1;
        $quantity2 = $request->quantity2;
        $quantity3 = $request->quantity3;
        $quantity4 = $request->quantity4;
        $quantity5 = $request->quantity5;
        $quantity6 = $request->quantity6;
        $quantity7 = $request->quantity7;
        $quantity8 = $request->quantity8;
        $quantity9 = $request->quantity9;
        $quantity10 = $request->quantity10;
        $quantity11 = $request->quantity11;
        $quantity12 = $request->quantity12;
        $quantity13 = $request->quantity13;
        $quantity14 = $request->quantity14;
        $quantity15 = $request->quantity15;
        $quantity16 = $request->quantity16;
        $quantity17 = $request->quantity17;
        $quantity18 = $request->quantity18;
        $quantity19 = $request->quantity19;
        $quantity20 = $request->quantity20;
        $quantity21 = $request->quantity21;
        $quantity22 = $request->quantity22;
        $quantity23 = $request->quantity23;
        $quantity24 = $request->quantity24;
        $quantity25 = $request->quantity25;
        $quantity26 = $request->quantity26;
        $quantity27 = $request->quantity27;
        $quantity28 = $request->quantity28;
        $quantity29 = $request->quantity29;
        $quantity30 = $request->quantity30;
        $quantity31 = $request->quantity31;
        $quantity32 = $request->quantity32;
        $quantity33 = $request->quantity33;
        $quantity34 = $request->quantity34;
        $quantity35 = $request->quantity35;
        $quantity36 = $request->quantity36;
        $quantity37 = $request->quantity37;
        $quantity38 = $request->quantity38;
        $quantity39 = $request->quantity39;
        $quantity40 = $request->quantity40;
        $quantity41 = $request->quantity41;
        $quantity42 = $request->quantity42;
        $quantity43 = $request->quantity43;
        $quantity44 = $request->quantity44;
        $quantity45 = $request->quantity45;
        $quantity46 = $request->quantity46;
        $quantity47 = $request->quantity47;
        $quantity48 = $request->quantity48;
        $quantity49 = $request->quantity49;
        $quantity50 = $request->quantity50;

         // Start All Price

        $price1 = $request->price1;
        $price2 = $request->price2;
        $price3 = $request->price3;
        $price4 = $request->price4;
        $price5 = $request->price5;
        $price6 = $request->price6;
        $price7 = $request->price7;
        $price8 = $request->price8;
        $price9 = $request->price9;
        $price10 = $request->price10;
        $price11 = $request->price11;
        $price12 = $request->price12;
        $price13 = $request->price13;
        $price14 = $request->price14;
        $price15 = $request->price15;
        $price16 = $request->price16;
        $price17 = $request->price17;
        $price18 = $request->price18;
        $price19 = $request->price19;
        $price20 = $request->price20;
        $price21 = $request->price21;
        $price22 = $request->price22;
        $price23 = $request->price23;
        $price24 = $request->price24;
        $price25 = $request->price25;
        $price26 = $request->price26;
        $price27 = $request->price27;
        $price28 = $request->price28;
        $price29 = $request->price29;
        $price30 = $request->price30;
        $price31 = $request->price31;
        $price32 = $request->price32;
        $price33 = $request->price33;
        $price34 = $request->price34;
        $price35 = $request->price35;
        $price36 = $request->price36;
        $price37 = $request->price37;
        $price38 = $request->price38;
        $price39 = $request->price39;
        $price40 = $request->price40;
        $price41 = $request->price41;
        $price42 = $request->price42;
        $price43 = $request->price43;
        $price44 = $request->price44;
        $price45 = $request->price45;
        $price46 = $request->price46;
        $price47 = $request->price47;
        $price48 = $request->price48;
        $price49 = $request->price49;
        $price50 = $request->price50;

         // Start All Total

        $total1 = $request->total1;
        $total2 = $request->total2;
        $total3 = $request->total3;
        $total4 = $request->total4;
        $total5 = $request->total5;
        $total6 = $request->total6;
        $total7 = $request->total7;
        $total8 = $request->total8;
        $total9 = $request->total9;
        $total10 = $request->total10;
        $total11 = $request->total11;
        $total12 = $request->total12;
        $total13 = $request->total13;
        $total14 = $request->total14;
        $total15 = $request->total15;
        $total16 = $request->total16;
        $total17 = $request->total17;
        $total18 = $request->total18;
        $total19 = $request->total19;
        $total20 = $request->total20;
        $total21 = $request->total21;
        $total22 = $request->total22;
        $total23 = $request->total23;
        $total24 = $request->total24;
        $total25 = $request->total25;
        $total26 = $request->total26;
        $total27 = $request->total27;
        $total28 = $request->total28;
        $total29 = $request->total29;
        $total30 = $request->total30;
        $total31 = $request->total31;
        $total32 = $request->total32;
        $total33 = $request->total33;
        $total34 = $request->total34;
        $total35 = $request->total35;
        $total36 = $request->total36;
        $total37 = $request->total37;
        $total38 = $request->total38;
        $total39 = $request->total39;
        $total40 = $request->total40;
        $total41 = $request->total41;
        $total42 = $request->total42;
        $total43 = $request->total43;
        $total44 = $request->total44;
        $total45 = $request->total45;
        $total46 = $request->total46;
        $total47 = $request->total47;
        $total48 = $request->total48;
        $total49 = $request->total49;
        $total50 = $request->total50;

        $productIDs = $product1 . "," . $product2 . "," . $product3 . "," . $product4 . "," . $product5 . "," . $product6 . "," . $product7 . "," . $product8 . "," . $product9 . "," . $product10 . "," . $product11 . "," . $product12 . "," . $product13 . "," . $product14 . "," . $product15 . "," . $product16 . "," . $product17 . "," . $product18 . "," . $product19 . "," . $product20. "," . $product21 . "," . $product22 . "," . $product23 . "," . $product24 . "," . $product25 . "," . $product26 . "," . $product27 . "," . $product28 . "," . $product29 . "," . $product31 . "," . $product32 . "," . $product33 . "," . $product34 . "," . $product35 . "," . $product36 . "," . $product37 . "," . $product38 . "," . $product39 . "," . $product40 . "," . $product41 . "," . $product42 . "," . $product43 . "," . $product44 . "," . $product45 . "," . $product46 . "," . $product47 . "," . $product48 . "," . $product49 . "," . $product50 ;
        $quantity = $quantity1 . "," . $quantity2 . "," . $quantity3 . "," . $quantity4 . "," . $quantity5 . "," . $quantity6 . "," . $quantity7 . "," . $quantity8 . "," . $quantity9 . "," . $quantity10 . "," . $quantity11 . "," . $quantity12 . "," . $quantity13 . "," . $quantity14 . "," . $quantity15 . "," . $quantity16 . "," . $quantity17 . "," . $quantity18 . "," . $quantity19 . "," . $quantity20 . "," . $quantity21 . "," . $quantity22 . "," . $quantity23 . "," . $quantity24 . "," . $quantity25 . "," . $quantity26 . "," . $quantity27 . "," . $quantity28 . "," . $quantity29 . "," . $quantity30 . "," . $quantity31 . "," . $quantity32 . "," . $quantity33 . "," . $quantity34 . "," . $quantity35 . "," . $quantity36 . "," . $quantity37 . "," . $quantity38 . "," . $quantity39 . "," . $quantity40 . "," . $quantity41 . "," . $quantity42 . "," . $quantity43 . "," . $quantity44 . "," . $quantity45 . "," . $quantity46 . "," . $quantity47 . "," . $quantity48 . "," . $quantity49 . "," . $quantity50 ;

        $price = $price1 . "," . $price2 . "," . $price3 . "," . $price4 . "," . $price5 . "," . $price6 . "," . $price7 . "," . $price8 . "," . $price9 . "," . $price10 . "," . $price11 . "," . $price12 . "," . $price13 . "," . $price14 . "," . $price15 . "," . $price16 . "," . $price17 . "," . $price18 . "," . $price19 . "," . $price20 . "," . $price21 . "," . $price22 . "," . $price23 . "," . $price24 . "," . $price25 . "," . $price26 . "," . $price27 . "," . $price28 . "," . $price29 . "," . $price30 . "," . $price31 . "," . $price32 . "," . $price33 . "," . $price36 . "," . $price37 . "," . $price38 . "," . $price39 . "," . $price40 . "," . $price41 . "," . $price42 . "," . $price43 . "," . $price44 . "," . $price45 . "," . $price46 . "," . $price47 . "," . $price48 . "," . $price49 . "," . $price50 ;

        $total = $total1 . "," . $total2 . "," . $total3 . "," . $total4 . "," . $total5 . "," . $total6 . "," . $total7 . "," . $total8 . "," . $total9 . "," . $total10 . "," . $total11 . "," . $total12 . "," . $total13 . "," . $total14 . "," . $total15 . "," . $total16 . "," . $total17 . "," . $total18 . "," . $total19 . "," . $total20 . "," . $total21 . "," . $total22 . "," . $total23 . "," . $total24 . "," . $total25 . "," . $total26 . "," . $total27 . "," . $total28 . "," . $total29 . "," . $total30 . "," . $total31 . "," . $total32 . "," . $total33 . "," . $total34 . "," . $total35 . "," . $total36 . "," . $total37 . "," . $total38 . "," . $total39 . "," . $total40 . "," . $total41 . "," . $total42 . "," . $total43 . "," . $total44 . "," . $total45 . "," . $total46 . "," . $total47 . "," . $total48 . "," . $total49 . "," . $total50 ;


        $sale->product = $productIDs;
        $sale->description = $request->description;
        $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
        $sale->category = "";
        $sale->merchant = $request->merchant;
        $sale->unit = 0;
        $sale->quantity = $quantity;
        $sale->price = $price;
        $sale->commission = $request->commission;
        $sale->total = 0;
        $sale->save();

        return redirect('/sale/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Sale::find($id)->delete();
        return redirect('/sale/all')->with('success', 'Successfully Deleted');
    }

}
