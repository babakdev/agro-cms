<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\Pcategories;
use App\Supplier;
use Image;

class ProductController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('product.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('product.add')->with($data);
    }

    public function all() {
        $data['products'] = Product::all();
        return view('product.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'producttitle' => 'required',
            'price' => 'required',
            'unit' => 'required',
            'unitquantity' => 'required',
            'category' => 'required',
            'supplier' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $product = new Product;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $product->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/product');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $product->photo);
        }

        $product->title = $request->input('producttitle');
        $product->description = $request->input('description');
        $product->code = $request->input('code');
        $product->category = $request->input('category');
        $product->supplier = $request->input('supplier');
        $product->model = $request->input('model');
        $product->unit = $request->input('unit');
        $product->unitquantity = $request->input('unitquantity');
        $product->materials = $request->input('materials');
        $product->weight = $request->input('weight');
        $product->dimension = $request->input('dimension');
        $product->color = $request->input('color');
        $product->design = $request->input('design');
        $product->capacity = $request->input('capacity');
        $product->quality = $request->input('quality');
        $product->origin = $request->input('origin');
        $product->warehouse = $request->input('warehouse');
        $product->manufacturedate = $request->input('manufacturedate');
        $product->expiredate = $request->input('expiredate');
        $product->link = $request->input('link');
        $product->price = $request->input('price');
        $product->profit = $request->input('profit');
        $product->cost1 = $request->input('cost1');
        $product->cost2 = $request->input('cost2');
        $product->cost3 = $request->input('cost3');
        $product->cost4 = $request->input('cost4');
        $product->cost5 = $request->input('cost5');
        $product->cost1 = $request->input('cost1');
        $product->cost2 = $request->input('cost2');
        $product->cost3 = $request->input('cost3');
        $product->cost4 = $request->input('cost4');
        $product->cost5 = $request->input('cost5');
        $product->save();
        return redirect('/product/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $product = Product::findOrFail($id);
        return view('product.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['product'] = Product::findOrFail($id);
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('product.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'producttitle' => 'required',
            'price' => 'required',
            'unit' => 'required',
            'unitquantity' => 'required',
            'category' => 'required',
            'supplier' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $product = Product::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $product->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/product');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $product->photo);
        }

        $product->title = $request->input('producttitle');
        $product->description = $request->input('description');
        $product->code = $request->input('code');
        $product->category = $request->input('category');
        $product->supplier = $request->input('supplier');
        $product->model = $request->input('model');
        $product->unit = $request->input('unit');
        $product->unitquantity = $request->input('unitquantity');
        $product->materials = $request->input('materials');
        $product->weight = $request->input('weight');
        $product->dimension = $request->input('dimension');
        $product->color = $request->input('color');
        $product->design = $request->input('design');
        $product->capacity = $request->input('capacity');
        $product->quality = $request->input('quality');
        $product->origin = $request->input('origin');
        $product->warehouse = $request->input('warehouse');
        $product->manufacturedate = $request->input('manufacturedate');
        $product->expiredate = $request->input('expiredate');
        $product->link = $request->input('link');
        $product->price = $request->input('price');
        $product->profit = $request->input('profit');
        $product->costtitle1 = $request->input('costtitle1');
        $product->costtitle2 = $request->input('costtitle2');
        $product->costtitle3 = $request->input('costtitle3');
        $product->costtitle4 = $request->input('costtitle4');
        $product->costtitle5 = $request->input('costtitle5');
        $product->cost1 = $request->input('cost1');
        $product->cost2 = $request->input('cost2');
        $product->cost3 = $request->input('cost3');
        $product->cost4 = $request->input('cost4');
        $product->cost5 = $request->input('cost5');
        $product->save();
        return redirect('/product/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Product::find($id)->delete();
        return redirect('/product/all')->with('success', 'Successfully Deleted');
    }

    public function category() {
        $data['categories'] = Pcategories::all();
        return view('product.category')->with($data);
    }

    public function storeCategory(Request $request) {
        $this->validate($request, [
            'category' => 'required',
        ]);

        $pcategories = new Pcategories();
        $pcategories->category = $request->category;
        $pcategories->save();
        return redirect('/product/category')->with('success', 'Successfully Added');
    }

    public function categoryDestroy($id) {
        Pcategories::find($id)->delete();
        return redirect('product/category/')->with('success', 'Successfully Deleted');
    }

}
