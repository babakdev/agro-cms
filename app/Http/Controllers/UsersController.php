<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use App\Pcategories;
use App\Purchase;
use App\Sale;
use App\Supplier;
use App\User;
use App\Invoices;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;

class UsersController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    public function purchases() {
        $id = Auth::id();
        $user = User::where('id', $id)->firstOrFail();
        $merchant = $user->merchant;
        $data['purchases'] = Sale::where('merchant', $merchant)->get();
        return view('users.purchases')->with($data);
    }

    public function purchasesshow($id) {
        $purchase = Sale::findOrFail($id);
        return view('users.purchasesshow')->with('purchase', $purchase);
    }

    public function supplies() {
        $id = Auth::id();
        $user = User::where('id', $id)->firstOrFail();
        $supplier = $user->supplier;
        $data['supplies'] = Purchase::where('supplier', $supplier)->get();
        return view('users.supplies')->with($data);
    }

    public function suppliesshow($id) {
        $data['supplies'] = Purchase::findOrFail($id);
        return view('users.suppliesshow')->with($data);
    }

    public function invoices() {
        $id = Auth::id();
        $user = User::where('id', $id)->firstOrFail();
        $merchant = $user->merchant;
        $data['invoices'] = Invoices::where('merchant', $merchant)->get();
        return view('users.invoices')->with($data);
    }

    public function invoiceshow($id) {
        $data['invoice'] = Invoices::findOrFail($id);
        return view('users.invoiceshow')->with($data);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'category' => 'required',
            'supplier' => 'required',
            'quantity' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $purchase = new Purchase;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $purchase->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/purchase');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $purchase->photo);
        }

        $purchase->title = $request->title;
        $purchase->description = $request->description;
        $purchase->purchasedate = Carbon::parse($request->purchasedate)->format('Y-m-d H:i:s');
        $purchase->category = $request->category;
        $purchase->supplier = $request->supplier;
        $purchase->unit = $request->unit;
        $purchase->quantity = $request->quantity;
        $purchase->company = $request->company;
        $purchase->price = $request->price;
        $purchase->commission = $request->commission;
        $purchase->total = $request->total;
        $purchase->save();
        return redirect('/purchase/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $purchase = Purchase::find($id);
        return view('purchase.show')->with('purchase', $purchase);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['purchase'] = Purchase::find($id);
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('purchase.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $purchase = Purchase::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $purchase->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/purchase');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $purchase->photo);
        }

        $purchase->title = $request->title;
        $purchase->description = $request->description;
        $purchase->purchasedate = Carbon::parse($request->purchasedate)->format('Y-m-d H:i:s');
        $purchase->category = $request->category;
        $purchase->supplier = $request->supplier;
        $purchase->unit = $request->unit;
        $purchase->quantity = $request->quantity;
        $purchase->company = $request->company;
        $purchase->price = $request->price;
        $purchase->commission = $request->commission;
        $purchase->total = $request->total;
        $purchase->save();
        return redirect('/purchase/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Purchase::find($id)->delete();
        return redirect('/purchase/all')->with('success', 'Successfully Deleted');
    }

}
