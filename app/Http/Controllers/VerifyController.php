<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Envato\Envato;
use App\Settings;
use Helper;

class VerifyController extends Controller
{
	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $o = Envato::verifyPurchase(Helper::getSettings()->purchasecode);
        // dd($o);
        if(isset($o['item']['id']) && $o['item']['id'] == "21551447"){
            return redirect('/');
        }else{
            return view('dashboard.verifycode');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePurchasecode(Request $request, $id)
    {
        $settings = Settings::find($id);
        $settings->purchasecode = $request->purchasecode;
        $settings->save();
        return redirect('/')->with('success', 'Purchase Code Successfully Inserted...');
    }

}
