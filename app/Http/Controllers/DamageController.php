<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Damage;
use App\Pcategories;
use App\Product;
use App\Merchant;
use App\Supplier;

class DamageController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['suppliers'] = Supplier::all();
        $data['products'] = Product::all();
        return view('damage.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['suppliers'] = Supplier::all();
        $data['products'] = Product::all();
        return view('damage.add')->with($data);
    }

    public function all() {
        $data['damages'] = Damage::all();
        $data['products'] = Product::all();
        return view('damage.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'product' => 'required',
            'date' => 'required',
            'category' => 'required',
            'merchant' => 'required',
            'supplier' => 'required',
            'quantity' => 'required',
            'total' => 'required',
        ]);

        $damage = new Damage;
        $damage->product = $request->product;
        $damage->description = $request->description;
        $damage->date = $request->date;
        $damage->category = $request->category;
        $damage->merchant = $request->merchant;
        $damage->supplier = $request->supplier;
        $damage->unit = $request->unit;
        $damage->quantity = $request->quantity;
        $damage->total = $request->total;
        $damage->save();
        return redirect('/damage/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $damage = Damage::findOrFail($id);
        return view('damage.show')->with('damage', $damage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['damage'] = Damage::findOrFail($id);
        $data['categorieslist'] = Pcategories::all();
        $data['merchants'] = Merchant::all();
        $data['suppliers'] = Supplier::all();
        $data['products'] = Product::all();
        return view('damage.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'product' => 'required',
            'date' => 'required',
            'category' => 'required',
            'merchant' => 'required',
            'supplier' => 'required',
            'quantity' => 'required',
            'total' => 'required',
        ]);

        $damage = Damage::find($id);

        $damage->product = $request->product;
        $damage->description = $request->description;
        $damage->date = $request->date;
        $damage->category = $request->category;
        $damage->merchant = $request->merchant;
        $damage->supplier = $request->supplier;
        $damage->unit = $request->unit;
        $damage->quantity = $request->quantity;
        $damage->total = $request->total;
        $damage->save();
        return redirect('/damage/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Damage::find($id)->delete();
        return redirect('/damage/all')->with('success', 'Successfully Deleted');
    }

}
