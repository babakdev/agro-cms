<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Purchase;
use Image;

class SupplierController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('supplier.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('supplier.add');
    }

    public function all() {
        $suppliers = Supplier::all();
        $data['suppliers'] = $suppliers;
        return view('supplier.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'supplierid' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $supplier = new Supplier;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $supplier->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/supplier');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $supplier->photo);
        }

        $supplier->supplierid = $request->input('supplierid');
        $supplier->fname = $request->input('fname');
        $supplier->lname = $request->input('lname');
        $supplier->phone = $request->input('phone');
        $supplier->fax = $request->input('fax');
        $supplier->email = $request->input('email');
        $supplier->company = $request->input('company');
        $supplier->cheque = $request->input('cheque');
        $supplier->bank = $request->input('bank');
        $supplier->account = $request->input('account');
        $supplier->paypal = $request->input('paypal');
        $supplier->other = $request->input('other');
        $supplier->address = $request->input('address');
        $supplier->about = $request->input('about');
        $supplier->facebook = $request->input('facebook');
        $supplier->twitter = $request->input('twitter');
        $supplier->youtube = $request->input('youtube');
        $supplier->map = $request->input('map');

        $supplier->save();
        return redirect('/supplier')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['purchases'] = Purchase::where('supplier', $id)->get();
        $data['supplier'] = Supplier::where('id', $id)->firstOrFail();
        return view('supplier.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $supplier = Supplier::where('id', $id)->firstOrFail();
        return view('supplier.edit')->with('supplier', $supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'supplierid' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $supplier = Supplier::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $supplier->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/supplier');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $supplier->photo);
        }

        $supplier->supplierid = $request->input('supplierid');
        $supplier->fname = $request->input('fname');
        $supplier->lname = $request->input('lname');
        $supplier->phone = $request->input('phone');
        $supplier->fax = $request->input('fax');
        $supplier->email = $request->input('email');
        $supplier->company = $request->input('company');
        $supplier->cheque = $request->input('cheque');
        $supplier->bank = $request->input('bank');
        $supplier->account = $request->input('account');
        $supplier->paypal = $request->input('paypal');
        $supplier->other = $request->input('other');
        $supplier->address = $request->input('address');
        $supplier->about = $request->input('about');
        $supplier->facebook = $request->input('facebook');
        $supplier->twitter = $request->input('twitter');
        $supplier->youtube = $request->input('youtube');
        $supplier->map = $request->input('map');
        $supplier->save();

        return redirect('/supplier/' . $id)->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Supplier::find($id)->delete();
        return redirect('/supplier/all')->with('success', 'Successfully Deleted');
    }

}
