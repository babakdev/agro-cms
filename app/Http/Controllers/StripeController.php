<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Payments;
use App\Invoices;

/** All Stripe Trait Details class **/
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class StripeController extends Controller {

    public function __construct() { }

    public function stripe(Request $request) {

        $invoice_id = $request->invoiceid;
        $invoice_amount = $request->amount;
        $stripeToken = $request->stripeToken;
        if(empty($stripeToken)){
            return redirect('/invoice/')->with('error', 'Token is not found! Please try again.');
        }

        Stripe::setApiKey(env('STRIPE_SECRET',''));
        try{
            $charge = Stripe::charges()->create([
                'amount' => $invoice_amount,
                'currency' => 'usd',
                'source' => $stripeToken
            ]);

            $chargeJson = $charge;
            $stripeID = $chargeJson['id'];
            $stripeStatus = $chargeJson['status'];
            $stripeAmount = $chargeJson['amount'];
            $stripeTnxID = $chargeJson['balance_transaction'];
            $stripeCreated = $chargeJson['created'];
            $stripeCurrency = $chargeJson['currency'];
            $stripePaid = $chargeJson['paid'];
            $stripeSourceID = $chargeJson['source']['id'];
            $stripeLastDigit = $chargeJson['source']['last4'];
            if($stripeStatus == "succeeded"){

                $paymentsDB = new Payments();
                $paymentsDB->invoiceid = $invoice_id;
                $paymentsDB->paymentid = $stripeTnxID;
                $paymentsDB->paymentmethod = "Stripe";
                $paymentsDB->payeremail = $stripeID;
                $paymentsDB->payerfname = $stripeID;
                $paymentsDB->payerlname = $stripeID;
                $paymentsDB->payerid = $stripeSourceID;
                $paymentsDB->amount = $stripeAmount;
                $paymentsDB->currency = $stripeCurrency;
                $paymentsDB->Save();

                $invoice = Invoices::find($invoice_id);
                $invoice->paymentstatus = "Paid";
                $invoice->paidamount = $stripeAmount;
                $invoice->paidmethod = "Stripe";
                $invoice->paidinfo = $stripeID . " (" . $stripeLastDigit .  ")";
                $invoice->Save();
                return redirect('/invoice/')->with('success', 'Payment is successful.');
            }else{
                return redirect('/invoice/')->with('error', 'Payment is not successful.');
            }
        } catch(Exception $e){
            return redirect('/invoice/')->with('error', $e->getMessage());
        }
    }
}
