<?php

/*
  | -----------------------------------------------------
  | PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
  | -----------------------------------------------------
  | AUTHOR: ONEZEROART TEAM
  | -----------------------------------------------------
  | EMAIL: support@onezeroart.com
  | -----------------------------------------------------
  | COPYRIGHT: RESERVED BY ONEZEROART.COM
  | -----------------------------------------------------
  | AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
  | -----------------------------------------------------
  | WEBSITE: http://onezeroart.com
  | -----------------------------------------------------
 */

namespace App\Http\Controllers;

//use Illuminate\Support\Facades\URL;


use App\Helpers\Helper;
use App\Invoices;
use App\Mail\NewInvoice;
use App\Merchant;
use App\Product;
use App\Sale;
use App\Balances;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Carbon;

class InvoiceController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        return view('invoice.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        return view('invoice.add')->with($data);
    }

    public function all() {
        $data['invoices'] = Invoices::all();
        return view('invoice.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'merchant' => 'required',
            'invoicedate' => 'required',
            'orderid' => 'required',
            'paymentdue' => 'required',
            'product1' => 'required',
            'quantity1' => 'required',
        ]);

        $invoice = new Invoices;

        $invoice->merchant = $request->merchant;
        $invoice->invoicedate = $request->invoicedate;
        $invoice->orderid = $request->orderid;
        $invoice->paymentdue = $request->paymentdue;
        $invoice->paymentmethod = $request->paymentmethod;
        $invoice->paymentaccount = $request->paymentaccount;
        $invoice->tax = $request->tax;
        $invoice->other = $request->other;
        $invoice->discount = $request->discount;
        $invoice->note = $request->note;

        $invoice->product1 = $request->product1;
        $invoice->product2 = $request->product2;
        $invoice->product3 = $request->product3;
        $invoice->product4 = $request->product4;
        $invoice->product5 = $request->product5;
        $invoice->product6 = $request->product6;
        $invoice->product7 = $request->product7;
        $invoice->product8 = $request->product8;
        $invoice->product9 = $request->product9;
        $invoice->product10 = $request->product10;

        $invoice->quantity1 = $request->quantity1;
        $invoice->quantity2 = $request->quantity2;
        $invoice->quantity3 = $request->quantity3;
        $invoice->quantity4 = $request->quantity4;
        $invoice->quantity5 = $request->quantity5;
        $invoice->quantity6 = $request->quantity6;
        $invoice->quantity7 = $request->quantity7;
        $invoice->quantity8 = $request->quantity8;
        $invoice->quantity9 = $request->quantity9;
        $invoice->quantity10 = $request->quantity10;

        $invoice->save();
        return redirect('/invoice/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $data['invoice'] = Invoices::findOrFail($id);
        $data['saleData'] = Sale::findOrFail($data['invoice']->saleid);
        return view('invoice.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $data['merchants'] = Merchant::all();
        $data['products'] = Product::all();
        $data['invoice'] = Invoices::findOrFail($id);
        return view('invoice.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'merchant' => 'required',
            'invoicedate' => 'required',
            'orderid' => 'required',
            'paymentdue' => 'required',
            'product1' => 'required',
            'quantity1' => 'required',
        ]);

        $invoice = Invoices::find($id);

        $invoice->merchant = $request->merchant;
        $invoice->invoicedate = $request->invoicedate;
        $invoice->orderid = $request->orderid;
        $invoice->paymentdue = $request->paymentdue;
        $invoice->paymentmethod = $request->paymentmethod;
        $invoice->paymentaccount = $request->paymentaccount;
        $invoice->tax = $request->tax;
        $invoice->other = $request->other;
        $invoice->discount = $request->discount;
        $invoice->note = $request->note;

        $invoice->product1 = $request->product1;
        $invoice->product2 = $request->product2;
        $invoice->product3 = $request->product3;
        $invoice->product4 = $request->product4;
        $invoice->product5 = $request->product5;
        $invoice->product6 = $request->product6;
        $invoice->product7 = $request->product7;
        $invoice->product8 = $request->product8;
        $invoice->product9 = $request->product9;
        $invoice->product10 = $request->product10;

        $invoice->quantity1 = $request->quantity1;
        $invoice->quantity2 = $request->quantity2;
        $invoice->quantity3 = $request->quantity3;
        $invoice->quantity4 = $request->quantity4;
        $invoice->quantity5 = $request->quantity5;
        $invoice->quantity6 = $request->quantity6;
        $invoice->quantity7 = $request->quantity7;
        $invoice->quantity8 = $request->quantity8;
        $invoice->quantity9 = $request->quantity9;
        $invoice->quantity10 = $request->quantity10;

        $invoice->save();
        return redirect('/invoice/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Invoices::find($id)->delete();
        return redirect('/invoice/all')->with('success', 'Successfully Deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function pdf($id) {

        $invoice = Invoices::findOrFail($id);

        $product1 = " ";
        $product2 = " ";
        $product3 = " ";
        $product4 = " ";
        $product5 = " ";
        $product6 = " ";
        $product7 = " ";
        $product8 = " ";
        $product9 = " ";
        $product10 = " ";

        if (count($invoice->product1) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product1 = "<tr>
                    <td> " . Helper::getProduct($invoice->product1)->id . " </td>
                    <td>" . Helper::getProduct($invoice->product1)->title . "</td>
                    <td>" . number_format($invoice->quantity1, 2) . "</td>
                    <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product1), 2) . "</td>
                    <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product1) * $invoice->quantity1, 2) . "</td>
                  </tr>";
            }
        } else {
            $product1 = "";
        }

        if (count($invoice->product2) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product2 = "<tr>
                <td> " . Helper::getProduct($invoice->product2)->id . " </td>
                <td>" . Helper::getProduct($invoice->product2)->title . "</td>
                <td>" . number_format($invoice->quantity2, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product2), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product2) * $invoice->quantity2, 2) . "</td>
              </tr>";
            }
        } else {
            $product2 = "";
        }

        if (count($invoice->product3) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product3 = "<tr>
                <td> " . Helper::getProduct($invoice->product3)->id . " </td>
                <td>" . Helper::getProduct($invoice->product3)->title . "</td>
                <td>" . number_format($invoice->quantity3, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product3), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product3) * $invoice->quantity3, 2) . "</td>
              </tr>";
            }
        } else {
            $product3 = "";
        }

        if (count($invoice->product4) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product4 = "<tr>
                <td> " . Helper::getProduct($invoice->product4)->id . " </td>
                <td>" . Helper::getProduct($invoice->product4)->title . "</td>
                <td>" . number_format($invoice->quantity4, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product4), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product4) * $invoice->quantity4, 2) . "</td>
              </tr>";
            }
        } else {
            $product4 = "";
        }

        if (count($invoice->product5) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product5 = "<tr>
                <td> " . Helper::getProduct($invoice->product5)->id . " </td>
                <td>" . Helper::getProduct($invoice->product5)->title . "</td>
                <td>" . number_format($invoice->quantity5, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product5), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product5) * $invoice->quantity5, 2) . "</td>
              </tr>";
            }
        } else {
            $product5 = "";
        }

        if (count($invoice->product6) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product6 = "<tr>
                <td> " . Helper::getProduct($invoice->product6)->id . " </td>
                <td>" . Helper::getProduct($invoice->product6)->title . "</td>
                <td>" . number_format($invoice->quantity6, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product6), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product6) * $invoice->quantity6, 2) . "</td>
              </tr>";
            }
        } else {
            $product6 = "";
        }

        if (count($invoice->product7) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product7 = "<tr>
                <td> " . Helper::getProduct($invoice->product7)->id . " </td>
                <td>" . Helper::getProduct($invoice->product7)->title . "</td>
                <td>" . number_format($invoice->quantity7, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product7), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product7) * $invoice->quantity7, 2) . "</td>
              </tr>";
            }
        } else {
            $product7 = "";
        }

        if (count($invoice->product8) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product8 = "<tr>
                <td> " . Helper::getProduct($invoice->product8)->id . " </td>
                <td>" . Helper::getProduct($invoice->product8)->title . "</td>
                <td>" . number_format($invoice->quantity8, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product8), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product8) * $invoice->quantity8, 2) . "</td>
              </tr>";
            }
        } else {
            $product8 = "";
        }

        if (count($invoice->product9) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product9 = "<tr>
                <td> " . Helper::getProduct($invoice->product9)->id . " </td>
                <td>" . Helper::getProduct($invoice->product9)->title . "</td>
                <td>" . number_format($invoice->quantity9, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product9), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product9) * $invoice->quantity9, 2) . "</td>
              </tr>";
            }
        } else {
            $product9 = "";
        }

        if (count($invoice->product10) > 0) {
            if (Helper::getProduct($invoice->product1) !== 0) {
                $product10 = "<tr>
                <td> " . Helper::getProduct($invoice->product10)->id . " </td>
                <td>" . Helper::getProduct($invoice->product10)->title . "</td>
                <td>" . number_format($invoice->quantity10, 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product10), 2) . "</td>
                <td>" . Helper::getCurrency() . " " . number_format(Helper::getProductCost($invoice->product10) * $invoice->quantity10, 2) . "</td>
              </tr>";
            }
        } else {
            $product10 = "";
        }


        $totalSubtotal = 0;
        if ($invoice->product1) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product1) * $invoice->quantity1;
        }

        if ($invoice->product2) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product2) * $invoice->quantity2;
        }

        if ($invoice->product3) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product3) * $invoice->quantity3;
        }
        if ($invoice->product4) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product4) * $invoice->quantity4;
        }
        if ($invoice->product5) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product5) * $invoice->quantity5;
        }
        if ($invoice->product6) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product6) * $invoice->quantity6;
        }
        if ($invoice->product7) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product7) * $invoice->quantity7;
        }
        if ($invoice->product8) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product8) * $invoice->quantity8;
        }
        if ($invoice->product9) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product9) * $invoice->quantity9;
        }
        if ($invoice->product10) {
            $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product10) * $invoice->quantity10;
        }


        $discount = $totalSubtotal * $invoice->discount / 100;
        $lessdiscount = $totalSubtotal - $discount;
        $tax = $lessdiscount * $invoice->tax / 100;
        $total = $totalSubtotal - $discount + $tax + $invoice->other;

        $totalFooterSubtotal = Helper::getCurrency() . " " . number_format($totalSubtotal, 2);
        $totalDiscount = Helper::getCurrency() . " " . number_format($discount, 2);
        $totalTax = Helper::getCurrency() . " " . number_format($tax, 2);
        $totalOther = Helper::getCurrency() . " " . number_format($invoice->other, 2);
        $netTotal = Helper::getCurrency() . " " . number_format($total, 2);

        $data = array();
//        $data['logo'] = $imageData;
        $data['logo'] = '$imageData';
        $data['date'] = $invoice->invoicedate;
        $data['title'] = Helper::getSettings()->title;
        $data['tag'] = Helper::getSettings()->tag;
        $data['address'] = Helper::getSettings()->address;
        $data['city'] = Helper::getSettings()->city;
        $data['country'] = Helper::getSettings()->country;
        $data['postal'] = Helper::getSettings()->postal;
        $data['phone'] = Helper::getSettings()->phone;
        $data['email'] = Helper::getSettings()->email;

        $data['merchant'] = Helper::getProfileByID($invoice->merchant, 'merchants')->fname . " " . Helper::getProfileByID($invoice->merchant, 'merchants')->lname . " ( " . Helper::getProfileByID($invoice->merchant, 'merchants')->company . " ) ";
        $data['maddress'] = strip_tags(Helper::getProfileByID($invoice->merchant, 'merchants')->address);
        $data['mphone'] = Helper::getProfileByID($invoice->merchant, 'merchants')->phone;
        $data['memail'] = Helper::getProfileByID($invoice->merchant, 'merchants')->email;
        $data['mfax'] = Helper::getProfileByID($invoice->merchant, 'merchants')->fax;

        $data['invoiceid'] = $invoice->id;
        $data['orderid'] = $invoice->orderid;
        $data['paymentdue'] = $invoice->paymentdue;
        $data['paymentmethod'] = $invoice->paymentmethod;
        $data['paymentaccount'] = $invoice->paymentaccount;


        $data['product1'] = $product1;
        $data['product2'] = $product2;
        $data['product3'] = $product3;
        $data['product4'] = $product4;
        $data['product5'] = $product5;
        $data['product6'] = $product6;
        $data['product7'] = $product7;
        $data['product8'] = $product8;
        $data['product9'] = $product9;
        $data['product10'] = $product10;


        $data['totalfootersubtotal'] = $totalFooterSubtotal;
        $data['totaldiscount'] = $totalDiscount;
        $data['totaltax'] = $totalTax;
        $data['totalother'] = $totalOther;
        $data['nettotal'] = $netTotal;
        $data['discountpercent'] = $invoice->discount;
        $data['taxpercent'] = $invoice->tax;

        $pdf = PDF::loadView('invoice.pdf', $data);
        $pdf->save(public_path('files') . '\invoice_' . date('d-m-Y') . "_" . $invoice->id . '.pdf');
        return $pdf->download('invoice' . $invoice->id . '.pdf');
    }

    public function sendInvoiceToEmail(Request $request) {
        $id = $request->id; //Current Invoice ID
        $email = $request->emails;
        $message = strip_tags($request->message);
        $this->pdf($id);
        $invoicePDF = '\invoice_' . date('d-m-Y') . "_" . $id . '.pdf';

        $data['invoicePDF'] = $invoicePDF;
        $data['email'] = $email;
        $data['message'] = $message;

        Mail::to($email)->send(new NewInvoice($data));
        return redirect('invoice/' . $id)->with('success', 'Sucessfully Email Send');
    }

    public function invoicepayment(Request $request, $id) {
        $this->validate($request, [
            'status' => 'required',
            'amount' => 'required',
            'method' => 'required',
        ]);

        $invoice = Invoices::find($id);
        $merchant = Merchant::find($invoice->merchant);

        $invoice->paymentstatus = $request->status;
        $invoice->paidamount = $request->amount + $invoice->paidamount;
        $invoice->paidmethod = $request->method;
        $invoice->paidinfo = $request->info;
        $invoice->save();

        $balance = new Balances;
        $balance->type = 'Income';
        $balance->title = "Payment (" . $merchant->fname . ")";
        $balance->description = "Payment From Merchant (" . $merchant->fname . ")";
        $balance->balancedate = Carbon::parse($request->paymentdate)->format('Y-m-d H:i:s');
        $balance->amount = $request->amount;
        $balance->by = Auth::user()->name;
        $balance->save();

        return redirect('invoice/' . $id)->with('success', 'Sucessfully Paid');
    }

}
