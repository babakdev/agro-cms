<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Balances;
use App\Invoices;
use App\Purchase;
use App\Sale;
use App\Settings;
use App\User;
use App\Envato\Envato;
use Helper;
use Carbon;


class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    public function index(){
        
        $data['balances'] = Balances::all();
        $data['incomes'] = Balances::where('type', 'Income')->get();
        $data['expenses'] = Balances::where('type', 'Expense')->get();
        $data['purchases'] = Purchase::all();
        $data['sales'] = Sale::all();

        $data['todayincome'] = Balances::where('type', 'Income')->whereDay('balancedate', Carbon::today())->sum('amount');
        $data['todayexpense'] = Balances::where('type', 'Expense')->whereDay('balancedate', Carbon::today())->sum('amount');
        $data['todaypurchase'] = Purchase::whereDay('purchasedate', Carbon::today())->sum('total');

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $data['weekincome'] = Balances::where('type', 'Income')->whereBetween('balancedate', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount');
        $data['weekexpense'] = Balances::where('type', 'Expense')->whereBetween('balancedate', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount');
        $data['weekpurchase'] = Purchase::whereBetween('purchasedate', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('total');

        $data['monthincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', Carbon::now()->month)->sum('amount');
        $data['monthexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', Carbon::now()->month)->sum('amount');
        $data['monthpurchase'] = Purchase::whereMonth('purchasedate', Carbon::now()->month)->sum('total');

        $data['yearincome'] = Balances::where('type', 'Income')->whereYear('balancedate', Carbon::now()->year)->sum('amount');
        $data['yearexpense'] = Balances::where('type', 'Expense')->whereYear('balancedate', Carbon::now()->year)->sum('amount');
        $data['yearpurchase'] = Purchase::whereYear('purchasedate', Carbon::now()->year)->sum('total');

        /****** Purchase & Sale Chart Data ***********/

        $data['janpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('January')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['febpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('February')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['marpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('March')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['aprpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('April')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['maypurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('May')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['junpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('June')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['julpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('July')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['augpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('August')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['seppurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('September')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['octpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('October')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['novpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('November')))->whereYear('purchasedate', date('Y'))->sum('total');
        $data['decpurchase'] = Purchase::whereMonth('purchasedate', date('m', strtotime('December')))->whereYear('purchasedate', date('Y'))->sum('total');

        $data['jansale'] = Helper::tpsalesamountbymonth(date('m', strtotime('January')), date('Y'));
        $data['febsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('February')), date('Y'));
        $data['marsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('March')), date('Y'));
        $data['aprsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('April')), date('Y'));
        $data['maysale'] = Helper::tpsalesamountbymonth(date('m', strtotime('May')), date('Y'));
        $data['junsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('June')), date('Y'));
        $data['julsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('July')), date('Y'));
        $data['augsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('August')), date('Y'));
        $data['sepsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('September')), date('Y'));
        $data['octsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('October')), date('Y'));
        $data['novsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('November')), date('Y'));
        $data['decsale'] = Helper::tpsalesamountbymonth(date('m', strtotime('December')), date('Y'));

        /********* Income & Expense Chart Data **********/

        $data['janincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('January')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['febincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('February')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['marincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('March')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['aprincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('April')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['mayincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('May')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['junincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('June')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['julincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('July')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['augincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('August')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['sepincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('September')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['octincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('October')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['novincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('November')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['decincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('December')))->whereYear('balancedate', date('Y'))->sum('amount');

        $data['janexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('January')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['febexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('February')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['marexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('March')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['aprexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('April')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['mayexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('May')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['junexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('June')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['julexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('July')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['augexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('August')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['sepexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('September')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['octexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('October')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['novexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('November')))->whereYear('balancedate', date('Y'))->sum('amount');
        $data['decexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('December')))->whereYear('balancedate', date('Y'))->sum('amount');

        if(Auth::user()->role !== "admin"){
            $id = Auth::id();
            $user = User::find($id);
            $merchant = $user->merchant;
            $supplier = $user->supplier;
            $data['userPurchase'] = Sale::where('merchant', $merchant)->count();
            $data['userSupplie'] = Purchase::where('supplier', $supplier)->count();
            $data['userInvoice'] = Invoices::where('merchant', $merchant)->count();
        }

        return view('dashboard.index')->with($data);

    }

}
