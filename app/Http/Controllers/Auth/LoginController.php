<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($social) {
        return Socialite::driver($social)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($social) {
        $userSocial = Socialite::driver($social)->user();
        $userData = User::where('email', $userSocial->getEmail())->first();

        if ($userData) {
            Auth::login($userData);
            return redirect('/');
        } else {
            $user = new User;
            $user->email = $userSocial->getEmail();
            $user->name = $userSocial->getName();
            $user->role = "merchant-supplier";
            $user->password = bcrypt("SP" . $userSocial->getName() . date("F-j-Y-g-i-a"));
            $user->save();
            Auth::login($user);
            return redirect('/');
        }
    }

}
