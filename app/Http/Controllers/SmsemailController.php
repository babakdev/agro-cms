<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Merchant;
use App\Staff;
use App\Supplier;
use App\User;
use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;

class SmsemailController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $data['suppliers'] = Supplier::all();
        $data['merchants'] = Merchant::all();
        return view('smsemail.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function SMSByNumbers(Request $request) {
        $numbers = $request->phone;
        $message = strip_tags($request->message);
        $numbersArr = explode(',', $numbers);
        for($x=0; count($numbersArr) > $x; $x++){
            $toNumber = preg_replace("/[^0-9]/", "", $numbersArr[$x]);
            Nexmo::message()->send([
                'to' => $toNumber,
                'from' => env('SMS_SEND_NUMBER'),
                'text' => $message,
            ]);
        }

        return redirect('smsemail')->with('success', 'Sucessfully SMS Send');
    }

    public function SMSByGroup(Request $request) {
        $type = $request->multismstype;
        if($type == "Supplier"){
            $phones = Supplier::pluck('phone');
        }else if($type == "Merchant"){
            $phones = Merchant::pluck('phone');
        }else if($type == "Staff"){
            $phones = Staff::pluck('phone');
        }else if($type == "User"){
            $phones = User::pluck('phone');
        }else{
            $phones = "";
        }
        $message = strip_tags($request->message);
        foreach ($phones as $phone) {
            if(!empty($phone) || $phone !== " "){
                $toNumber = preg_replace("/[^0-9]/", "", $phone);
                Nexmo::message()->send([
                    'to' => $toNumber,
                    'from' => env('SMS_SEND_NUMBER'),
                    'text' => $message,
                ]);
            }
        }
        return redirect('smsemail')->with('success', 'Sucessfully SMS Send');
    }


    public function EmailByEmails(Request $request) {
        $emails = $request->email;
        $message = $request->message;
        $emailsArr = explode(',', $emails);
        for($x=0; count($emailsArr) > $x; $x++){
            $toEmail = preg_replace('/\s+/', '', $emailsArr[$x]);
            $data = array('email' => $toEmail, 'messagetext' => $message);
            //$job = (new SendEmail($toEmail, $data))->delay(10);
            //$this->dispatch($job);
            SendEmail::dispatch($toEmail, $data)->delay(15); //Job Dispatching
        }
        return redirect('smsemail')->with('success', 'Sucessfully Email Send');
    }


    public function EmailByGroups(Request $request) {
        $type = $request->multiemailtype;

        if($type == "Supplier"){
            $emails = Supplier::pluck('email');
        }else if($type == "Merchant"){
            $emails = Merchant::pluck('email');
        }else if($type == "Staff"){
            $emails = Staff::pluck('email');
        }else if($type == "User"){
            $emails = User::pluck('email');
        }else{
            $emails = "";
        }
        $message = $request->message;
        foreach ($emails as $email) {
            if(!empty($email) || $email !== " " || isset($email)){
                $toEmail = preg_replace('/\s+/', '', $email);
                $data = array('email' => $toEmail, 'messagetext' => $message);
                //$job = (new SendEmail($toEmail, $data))->delay(5);
                //$this->dispatch($job);

                SendEmail::dispatch($toEmail, $data)->delay(15);
            }
        }

        return redirect('smsemail')->with('success', 'Sucessfully Email Send');
    }



}
