<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Stores;
use App\Staff;
use Image;

class StoreController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['staffs'] = Staff::all();
        return view('store.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['staffs'] = Staff::all();
        return view('store.add')->with($data);
    }

    public function all() {
        $data['stores'] = Stores::all();
        return view('store.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $store = new Stores;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $store->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/store');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $store->photo);
        }

        $store->name = $request->name;
        $store->slogan = $request->slogan;
        $store->phone = $request->phone;
        $store->time = $request->time;
        $store->location = $request->location;
        $store->address = $request->address;
        $store->manager = $request->manager;
        $store->managerphone = $request->managerphone;
        $store->staff = $request->staff;
        $store->website = $request->website;
        $store->facebook = $request->facebook;
        $store->twitter = $request->twitter;
        $store->youtube = $request->youtube;
        $store->map = $request->map;
        $store->description = $request->description;
        $store->save();
        return redirect('/store/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $store = Stores::findOrFail($id);
        return view('store.show')->with('store', $store);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['staffs'] = Staff::all();
        $data['store'] = Stores::findOrFail($id);
        return view('store.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $store = Stores::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $store->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/store');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $store->photo);
        }

        $store->name = $request->name;
        $store->slogan = $request->slogan;
        $store->phone = $request->phone;
        $store->time = $request->time;
        $store->location = $request->location;
        $store->address = $request->address;
        $store->manager = $request->manager;
        $store->managerphone = $request->managerphone;
        $store->staff = $request->staff;
        $store->website = $request->website;
        $store->facebook = $request->facebook;
        $store->twitter = $request->twitter;
        $store->youtube = $request->youtube;
        $store->map = $request->map;
        $store->description = $request->description;
        $store->save();
        return redirect('/store/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Stores::find($id)->delete();
        return redirect('/store/all')->with('success', 'Successfully Deleted');
    }

}
