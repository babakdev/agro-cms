<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Purchase;
use App\Pcategories;
use App\Supplier;
use Image;

class PurchaseController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('purchase.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('purchase.add')->with($data);
    }

    public function all() {
        $data['purchases'] = Purchase::all();
        return view('purchase.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'purchasedate' => 'required',
            'category' => 'required',
            'supplier' => 'required',
            'unit' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'total' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $purchase = new Purchase;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $purchase->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/purchase');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $purchase->photo);
        }

        $purchase->title = $request->title;
        $purchase->description = $request->description;
        $purchase->purchasedate = Carbon::parse($request->purchasedate)->format('Y-m-d H:i:s');
        $purchase->category = $request->category;
        $purchase->supplier = $request->supplier;
        $purchase->unit = $request->unit;
        $purchase->quantity = $request->quantity;
        $purchase->company = $request->company;
        $purchase->price = $request->price;
        $purchase->commission = $request->commission;
        $purchase->total = $request->total;
        $purchase->save();
        return redirect('/purchase/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $purchase = Purchase::findOrFail($id);
        return view('purchase.show')->with('purchase', $purchase);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['purchase'] = Purchase::findOrFail($id);
        $data['categorieslist'] = Pcategories::all();
        $data['suppliers'] = Supplier::all();
        return view('purchase.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'purchasedate' => 'required',
            'category' => 'required',
            'supplier' => 'required',
            'unit' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'total' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $purchase = Purchase::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $purchase->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/purchase');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $purchase->photo);
        }

        $purchase->title = $request->title;
        $purchase->description = $request->description;
        $purchase->purchasedate = Carbon::parse($request->purchasedate)->format('Y-m-d H:i:s');
        $purchase->category = $request->category;
        $purchase->supplier = $request->supplier;
        $purchase->unit = $request->unit;
        $purchase->quantity = $request->quantity;
        $purchase->company = $request->company;
        $purchase->price = $request->price;
        $purchase->commission = $request->commission;
        $purchase->total = $request->total;
        $purchase->save();
        return redirect('/purchase/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Purchase::find($id)->delete();
        return redirect('/purchase/all')->with('success', 'Successfully Deleted');
    }

}
