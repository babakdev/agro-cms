<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Bankaccount;

class TransactionController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['bankaccounts'] = Bankaccount::all();
        return view('bank.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['bankaccounts'] = Bankaccount::all();
        return view('bank.add')->with($data);
    }

    public function all() {
        $data['transactions'] = Transaction::all();
        return view('bank.all')->with($data);
    }

    /**
     * Bank a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'type' => 'required',
            'amount' => 'required',
        ]);

        $bank = new Transaction;

        $bank->date = $request->date;
        $bank->name = $request->name;
        $bank->type = $request->type;
        $bank->amount = $request->amount;
        $bank->note = $request->note;
        $bank->save();
        return redirect('/transaction/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $bank = Transaction::findOrFail($id);
        return view('bank.show')->with('transaction', $bank);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['bankaccounts'] = Bankaccount::all();
        $data['bank'] = Transaction::findOrFail($id);
        return view('bank.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'type' => 'required',
            'amount' => 'required',
        ]);

        $bank = Transaction::find($id);

        $bank->date = $request->date;
        $bank->name = $request->name;
        $bank->type = $request->type;
        $bank->amount = $request->amount;
        $bank->note = $request->note;
        $bank->save();
        return redirect('/transaction/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Transaction::find($id)->delete();
        return redirect('/transaction/all')->with('success', 'Successfully Deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addbank() {
        $data['bankaccounts'] = BankAccount::all();
        return view('bank.addbank')->with($data);
    }

    /**
     * Bank a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addbankstore(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'account' => 'required',
        ]);

        $bankaccount = new BankAccount;

        $bankaccount->name = $request->name;
        $bankaccount->account = $request->account;
        $bankaccount->branch = $request->branch;
        $bankaccount->swift = $request->swift;
        $bankaccount->address = $request->address;
        $bankaccount->save();
        return redirect('/transaction/')->with('success', 'Successfully Added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyaccount($id) {
        Bankaccount::find($id)->delete();
        return redirect('/transaction')->with('success', 'Successfully Deleted');
    }

}
