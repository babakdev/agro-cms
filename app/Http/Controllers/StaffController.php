<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use Image;

class StaffController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('staff.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('staff.add');
    }

    public function all() {
        $data['staffs'] = Staff::all();
        return view('staff.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'fname' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $staff = new Staff;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $staff->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/staff');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $staff->photo);
        }

        $staff->fname = $request->input('fname');
        $staff->lname = $request->input('lname');
        $staff->phone = $request->input('phone');
        $staff->position = $request->input('position');
        $staff->dob = $request->input('dob');
        $staff->gender = $request->input('gender');
        $staff->education = $request->input('education');
        $staff->height = $request->input('height');
        $staff->weight = $request->input('weight');
        $staff->nationality = $request->input('nationality');
        $staff->blood = $request->input('blood');
        $staff->religion = $request->input('religion');
        $staff->marital = $request->input('marital');
        $staff->nid = $request->input('nid');
        $staff->address = $request->input('address');

        $staff->save();
        return redirect('/staff')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $staff = Staff::where('id', $id)->firstOrFail();
        return view('staff.show')->with('staff', $staff);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $staff = Staff::where('id', $id)->firstOrFail();
        return view('staff.edit')->with('staff', $staff);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'fname' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $staff = Staff::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $staff->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/staff');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $staff->photo);
        }

        $staff->fname = $request->input('fname');
        $staff->lname = $request->input('lname');
        $staff->phone = $request->input('phone');
        $staff->position = $request->input('position');
        $staff->dob = $request->input('dob');
        $staff->gender = $request->input('gender');
        $staff->education = $request->input('education');
        $staff->height = $request->input('height');
        $staff->weight = $request->input('weight');
        $staff->nationality = $request->input('nationality');
        $staff->blood = $request->input('blood');
        $staff->religion = $request->input('religion');
        $staff->marital = $request->input('marital');
        $staff->nid = $request->input('nid');
        $staff->address = $request->input('address');

        $staff->save();
        return redirect('/staff')->with('success', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Staff::find($id)->delete();
        return redirect('/staff/all')->with('success', 'Successfully Deleted');
    }

}
