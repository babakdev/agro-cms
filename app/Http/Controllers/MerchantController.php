<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merchant;
use App\Sale;
use Image;

class MerchantController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('merchant.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('merchant.add');
    }

    public function all() {
        $merchants = Merchant::all();
        $data['merchants'] = $merchants;
        return view('merchant.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'merchantid' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $merchant = new Merchant();
        $image = $request->file('photo');
        if ($request->file('photo')) {
            $merchant->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/merchant');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $merchant->photo);
        }

        $merchant->merchantid = $request->input('merchantid');
        $merchant->fname = $request->input('fname');
        $merchant->lname = $request->input('lname');
        $merchant->phone = $request->input('phone');
        $merchant->fax = $request->input('fax');
        $merchant->email = $request->input('email');
        $merchant->company = $request->input('company');
        $merchant->tradelicense = $request->input('tradelicense');
        $merchant->otherlicense = $request->input('otherlicense');
        $merchant->address = $request->input('address');
        $merchant->about = $request->input('about');
        $merchant->facebook = $request->input('facebook');
        $merchant->twitter = $request->input('twitter');
        $merchant->youtube = $request->input('youtube');
        $merchant->map = $request->input('map');
        $merchant->Save();
        return redirect('/merchant')->with('success', 'Merchant Inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['sales'] = Sale::orderBy('id','DESC')->where('merchant', $id)->get();
        $data['merchant'] = Merchant::findOrFail($id);
        return view('merchant.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Merchant::findOrFail($id);
        return view('merchant.edit')->with('merchant', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'merchantid' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $merchant = Merchant::find($id);
        $image = $request->file('photo');
        if ($request->file('photo')) {
            $merchant->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/merchant');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $merchant->photo);
        }

        $merchant->merchantid = $request->input('merchantid');
        $merchant->fname = $request->input('fname');
        $merchant->lname = $request->input('lname');
        $merchant->phone = $request->input('phone');
        $merchant->fax = $request->input('fax');
        $merchant->email = $request->input('email');
        $merchant->company = $request->input('company');
        $merchant->tradelicense = $request->input('tradelicense');
        $merchant->otherlicense = $request->input('otherlicense');
        $merchant->address = $request->input('address');
        $merchant->about = $request->input('about');
        $merchant->facebook = $request->input('facebook');
        $merchant->twitter = $request->input('twitter');
        $merchant->youtube = $request->input('youtube');
        $merchant->map = $request->input('map');
        $merchant->save();
        return redirect('/merchant/' . $id)->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Merchant::find($id)->delete();
        return redirect('/merchant/all')->with('success', 'Successfully Deleted');
    }

}
