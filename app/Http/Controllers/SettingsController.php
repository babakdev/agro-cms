<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Settings;
use Image;

class SettingsController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['settings'] = Settings::findOrFail(1);
        return view('settings.add')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['settings'] = Settings::findOrFail(1);
        return view('settings.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'tag' => 'required',
            'currency' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'postal' => 'required',
        ]);

        $settings = new Settings;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $settings->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $settings->photo);
        }

        $settings->title = $request->title;
        $settings->tag = $request->tag;
        $settings->currency = $request->currency;
        $settings->email = $request->email;
        $settings->phone = $request->phone;
        $settings->address = $request->address;
        $settings->city = $request->city;
        $settings->country = $request->country;
        $settings->postal = $request->postal;
        $settings->license1 = $request->license1;
        $settings->license2 = $request->license2;
        $settings->license3 = $request->license3;
        $settings->save();
        return redirect('/settings/')->with('success', 'Successfully Added');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['settings'] = Settings::findOrFail($id);
        return view('settings.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'tag' => 'required',
            'currency' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'postal' => 'required',
        ]);

        $settings = Settings::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $settings->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $settings->photo);
        }

        $settings->title = $request->title;
        $settings->tag = $request->tag;
        $settings->currency = $request->currency;
        $settings->email = $request->email;
        $settings->phone = $request->phone;
        $settings->address = $request->address;
        $settings->city = $request->city;
        $settings->country = $request->country;
        $settings->postal = $request->postal;
        $settings->license1 = $request->license1;
        $settings->license2 = $request->license2;
        $settings->license3 = $request->license3;
        $settings->save();
        return redirect('/settings')->with('success', 'Successfully Updated');
    }
    
}
