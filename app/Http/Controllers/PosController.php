<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Envato\Envato;
use App\Payments;
use App\Balances;
use App\Invoices;
use App\Purchase;
use App\Sale;
use App\Settings;
use App\User;
use App\Product;
use App\Pcategories;
use App\Supplier;
use Helper;
use Validator;
use Session;
use Carbon;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

/** All Stripe Trait Details class **/
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;


class PosController extends Controller

{

    private $_api_context;

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');

        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(){

         $settings = Settings::find(1);
         $o = Envato::verifyPurchase($settings->purchasecode);

         $data['products'] = Product::all();
         $data['suppliers'] = Supplier::all();
         $data['categorieslist'] = Pcategories::all();

         if(  isset($o['item']['id']) && $o['item']['id'] == "21551447"){
             return view('pos.index')->with($data);
         }else{
             //return view('pos.verifycode');
             return view('pos.index')->with($data);
         }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductsByCat(Request $request)
    {
        $products = Product::where('category', $request->category)->get();
        $html = '';
        foreach($products as $product){
        $html .= '<tr>
                <td>';
                    if($product->photo){
                        $html .= '<img class="img-60" src=" ' . asset("images/product/" . $product->photo) . ' " alt="Product Photo">';
                    } else{
                        $html .= '<img class="img-60" src=" ' . asset("images/default.png") . ' " alt="Product Photo">';
                    }
                $html .= '</td>';
                $html .= '<td> ' . $product->title . '</td>';
                $html .= '<td> ' . Helper::getCurrency() . " " . number_format($product->price, 2) . '</td>';
                $html .= '<td><a class="addToCart" href="#" data-itemID="' . $product->id . '"><i class="material-icons">add_circle</i></a></td>';
            $html .= '</tr>';
        }
        echo json_encode($html);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {

        if(session()->has('posDiscount')){
            $discount = session()->get('posDiscount'); //Item Discount
        }else{
            $discount = 0; //Item Discount
        }

        if(session()->has('posTax')){
            $tax = session()->get('posTax'); //Item Discount
        }else{
            $tax = 0; //Item Discount
        }

        if(session()->has('posShipping')){
            $shipping = session()->get('posShipping'); //Item Discount
        }else{
            $shipping = 0; //Item Discount
        }

        $request->session()->push('productIDs', $request->itemID);
        $itemIDs = $request->session()->get('productIDs'); //Item IDs array
        $totalItmes = count($itemIDs);
        $itemIDs = array_count_values($itemIDs);
        $total = array();
        $html = '';
        foreach ($itemIDs as $key => $item) {
            $product = Product::find($key);
            $total[] = $product->price * $item; //total price
            $html .= '<tr>
                    <td>';
                        if($product->photo){
                            $html .= '<img class="img-60" src=" ' . asset("images/product/" . $product->photo) . ' " alt="Product Photo">';
                        } else{
                            $html .= '<img class="img-60" src=" ' . asset("images/default.png") . ' " alt="Product Photo">';
                        }
                    $html .= '</td>';
                    $html .= '<td> ' . $product->title . '</td>';
                    $html .= '<td class="input-group"><span class="decrement"><i class="material-icons">remove_circle</i></span><input disabled class="form-control text-center adjustToCart" data-itemID="' . $product->id . '" type="text" min="1" value="' . $item . '"><span class="increment"><i class="material-icons">add_circle</i></span></td>';
                    $html .= '<td> ' . Helper::getCurrency() . " " . number_format($product->price, 2) . '</td>';
                    $html .= '<td> ' . Helper::getCurrency() . " " . number_format($product->price * $item, 2) . '</td>';
                    $html .= '<td><a class="text-red" href="#" data-itemID="' . $product->id . '"><i class="material-icons removeToCart">delete</i></a></td>';
                $html .= '</tr>';
        }

        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>QTY <strong>' . number_format($totalItmes, 2) . '</strong></td>';
            $html .= '<td>Total</td>';
            $html .= '<td><strong>' . Helper::getCurrency() . " " . number_format(array_sum($total), 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';

        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Discount</td>';
            $html .= '<td><strong>(-) ' . Helper::getCurrency() . " " . number_format($discount, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Tax</td>';
            $html .= '<td><strong>(+) ' . Helper::getCurrency() . " " . number_format($tax, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Shipping</td>';
            $html .= '<td><strong>(+) ' . Helper::getCurrency() . " " . number_format($shipping, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Net Total</td>';
            $html .= '<td><strong>(=) ' . Helper::getCurrency() . " " . number_format((array_sum($total) - $discount) + ($tax + $shipping), 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';

        $data['html'] = $html;
        $data['total'] = (array_sum($total) - $discount) + ($tax + $shipping);

        echo json_encode($data);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adjustToCart(Request $request)
    {
        $itemIDs = $request->session()->get('productIDs'); //Item IDs array
        if(!empty($request->type) && $request->type == "plus" ){
            $request->session()->push('productIDs', $request->itemID);
        }else if(!empty($request->type) && $request->type == "minus" ){
            $arrayKeyReturned = array_search($request->itemID, $itemIDs); //array key found
            $request->session()->forget('productIDs.' . $arrayKeyReturned);
        }else if(!empty($request->type) && $request->type == "delete" ){
            foreach ($itemIDs as $itemKey => $item){
                if ($request->itemID == $item) {
                    $request->session()->forget('productIDs.' . $itemKey);
                }
            }
        }

        if(session()->has('posDiscount')){
            $discount = session()->get('posDiscount'); //Item Discount
        }else{
            $discount = 0; //Item Discount
        }

        if(session()->has('posTax')){
            $tax = session()->get('posTax'); //Item Discount
        }else{
            $tax = 0; //Item Discount
        }

        if(session()->has('posShipping')){
            $shipping = session()->get('posShipping'); //Item Discount
        }else{
            $shipping = 0; //Item Discount
        }

        $itemIDs = $request->session()->get('productIDs'); //Item IDs array

        $totalItmes = count($itemIDs);
        $itemIDs = array_count_values($itemIDs);

        $total = array();
        $html = '';
        foreach ($itemIDs as $key => $item) {
            $product = Product::find($key);
            $total[] = $product->price * $item; //total price
            $html .= '<tr>
                    <td>';
                        if($product->photo){
                            $html .= '<img class="img-60" src=" ' . asset("images/product/" . $product->photo) . ' " alt="Product Photo">';
                        } else{
                            $html .= '<img class="img-60" src=" ' . asset("images/default.png") . ' " alt="Product Photo">';
                        }
                    $html .= '</td>';
                    $html .= '<td> ' . $product->title . '</td>';
                    $html .= '<td class="input-group"><span class="decrement"><i class="material-icons">remove_circle</i></span><input disabled class="form-control text-center adjustToCart" data-itemID="' . $product->id . '" type="text" min="1" value="' . $item . '"><span class="increment"><i class="material-icons">add_circle</i></span></td>';
                    $html .= '<td> ' . Helper::getCurrency() . " " . number_format($product->price, 2) . '</td>';
                    $html .= '<td> ' . Helper::getCurrency() . " " . number_format($product->price * $item, 2) . '</td>';
                    $html .= '<td><a class="text-red" href="#" data-itemID="' . $product->id . '"><i class="material-icons removeToCart">delete</i></a></td>';
                $html .= '</tr>';
        }

        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>QTY <strong>' . number_format($totalItmes, 2) . '</strong></td>';
            $html .= '<td>Total</td>';
            $html .= '<td><strong>' . Helper::getCurrency() . " " . number_format(array_sum($total), 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';

        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Discount</td>';
            $html .= '<td><strong>(-) ' . Helper::getCurrency() . " " . number_format($discount, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Tax</td>';
            $html .= '<td><strong>(+) ' . Helper::getCurrency() . " " . number_format($tax, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Shipping</td>';
            $html .= '<td><strong>(+) ' . Helper::getCurrency() . " " . number_format($shipping, 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>Net Total</td>';
            $html .= '<td><strong>(=) ' . Helper::getCurrency() . " " . number_format((array_sum($total) - $discount) + ($tax + $shipping), 2) . '</strong></td>';
            $html .= '<td></td>';
        $html .= '</tr>';

        $data['html'] = $html;
        $data['total'] = (array_sum($total) - $discount) + ($tax + $shipping);

        echo json_encode($data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function updateBalance(Request $request)
    {

        $request->session()->put('posDiscount', $request->discount);
        $request->session()->put('posTax', $request->tax);
        $request->session()->put('posShipping', $request->shipping);

        return redirect('/pos/')->with('success', 'POS Successfully Calculated');

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function resetpos(Request $request)
    {
        $request->session()->forget('productIDs');
        $request->session()->forget('posDiscount');
        $request->session()->forget('posTax');
        $request->session()->forget('posShipping');
        $request->session()->forget('posLastInvoice');
        return redirect('/pos/')->with('success', 'POS Successfully Reset');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function payment(Request $request) {

         $paymentMethod =  $request->method; //payment method
         $convertedAmount =  $request->total; //total amount
         $systemCurrency = \App\Helpers\Helper::getSettings()->currency;

         if($paymentMethod == 0){ //Cash

             $itemIDs = $request->session()->get('productIDs'); //Item IDs array
             $itemIDs = array_count_values($itemIDs);

             if(session()->has('posDiscount')){
                 $discount = session()->get('posDiscount'); //Item Discount
             }else{
                 $discount = 0; //Item Discount
             }

             $productIDs = '';
             $quantity = '';
             $price = '';

             foreach ($itemIDs as $key => $item) {
                 $productIDs .= $key . ',';
                 $quantity .= $item . ',';
                 $price .= Product::find($key)->price . ',';
             }

             $sale = new Sale;
             $sale->product = $productIDs;
             $sale->description = strip_tags($request->description);
             $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
             $sale->category = "";
             $sale->merchant = 0;
             $sale->unit = 0;
             $sale->quantity = $quantity;
             $sale->price = $price;
             $sale->commission = $discount;
             $sale->total = 0;
             $sale->save();

             $balance = new Balances;
             $balance->type = "Income";
             $balance->title = "POS Payment ( " . $request->customer . "-" . $request->customer_phone . ")";
             $balance->description = strip_tags($request->description);
             $balance->balancedate = Carbon::now();
             $balance->amount = $convertedAmount;
             $balance->by = Auth::user()->name;
             $balance->save();

             $latestSale = Sale::latest('id')->first();
             $invoice = new Invoices;
             $invoice->saleid = $latestSale->id;
             $invoice->merchant = 0;
             $invoice->invoicedate = Carbon::now();
             $invoice->orderid = Carbon::parse($request->saledate)->format('Y-m-d') . "-" . rand(10, 99);
             $invoice->paymentdue = "";
             $invoice->product1 = "";
             $invoice->quantity1 = 0;
             $invoice->paymentstatus = "Paid";
             $invoice->paidamount = $convertedAmount;
             $invoice->paidmethod = "Cash";
             $invoice->paidinfo = "N/A";
             $invoice->customer = $request->customer;
             $invoice->customer_phone = $request->customer_phone;
             $invoice->save();

             $latestInvoice = Invoices::latest('id')->first();
             $paymentsDB = new Payments();
             $paymentsDB->invoiceid = $latestInvoice->id;
             $paymentsDB->paymentid = "N/A";
             $paymentsDB->paymentmethod = "Cash";
             $paymentsDB->payeremail = "N/A";
             $paymentsDB->payerfname = $request->customer;
             $paymentsDB->payerlname = "N/A";
             $paymentsDB->payerid = "N/A";
             $paymentsDB->amount = $convertedAmount;
             $paymentsDB->currency = $systemCurrency;
             $paymentsDB->Save();

             \session()->forget('productIDs');
             \session()->forget('posShipping');
             \session()->forget('posTax');
             \session()->forget('posDiscount');
             \session()->forget('posLastInvoice');

             \session()->put('posLastInvoice', $latestInvoice->id);

             return redirect('/pos')->with('success', 'Payment Successful');

         }else if($paymentMethod == 1){ //Paypal

             $payer = new Payer();
             $payer->setPaymentMethod('paypal');

             $item = new Item();
             $item->setName('POS Payment (' . $request->customer . '-' . $request->customer_phone . ')')// item name
                     ->setCurrency('USD')
                     ->setQuantity(1)
                     ->setPrice($convertedAmount);
             $item_list = new ItemList();
             $item_list->setItems([$item]);

             $amount = new Amount();
             $amount->setCurrency('USD')
                     ->setTotal($convertedAmount);

             $transaction = new Transaction();
             $transaction->setAmount($amount)
                     ->setItemList($item_list)
                     ->setDescription('POS Payment (' . $request->customer . '-' . $request->customer_phone . ')');

             $redirect_urls = new RedirectUrls();
             $redirect_urls->setReturnUrl(url('/pos/paypal/payment/status'))
                     ->setCancelUrl(url('/pos/paypal/payment/status'));

             $payment = new Payment();
             $payment->setIntent('Sale')
                     ->setPayer($payer)
                     ->setRedirectUrls($redirect_urls)
                     ->setTransactions(array($transaction));

             try {
                 $payment->create($this->_api_context);
             } catch (PayPalConnectionException $ex) {
                 echo $ex->getMessage();
                 return redirect('/pos')->with('error', 'Something Went wrong, funds could not be loaded');
             }

             foreach ($payment->getLinks() as $link) {
                 if ($link->getRel() == 'approval_url') {
                     $redirect_url = $link->getHref();
                     break;
                 }
             }

             \Session()->put('paypal_payment_id', $payment->getId());
             \Session()->put('customer', $request->customer);
             \Session()->put('customer_phone', $request->customer_phone);
             \Session()->put('description', strip_tags($request->description));

             if (isset($redirect_url)) {
                 return redirect($redirect_url);
             }

             return redirect('/pos')->with('error', 'Unknown error occurred');

         }else if($paymentMethod == 2){ //Stripe

             //$invoice_id = $request->invoiceid;
             //$invoice_amount = $request->amount;
             $stripeToken = $request->stripeToken;
             if(empty($stripeToken)){
                 return redirect('/pos')->with('error', 'Token is not found! Please try again.');
             }

             Stripe::setApiKey(env('STRIPE_SECRET',''));
             try{
                 $charge = Stripe::charges()->create([
                     'amount' => intval($convertedAmount),
                     'currency' => $systemCurrency,
                     'source' => $stripeToken
                 ]);

                 $chargeJson = $charge;
                 $stripeID = $chargeJson['id'];
                 $stripeStatus = $chargeJson['status'];
                 $stripeAmount = $chargeJson['amount'];
                 $stripeTnxID = $chargeJson['balance_transaction'];
                 $stripeCreated = $chargeJson['created'];
                 $stripeCurrency = $chargeJson['currency'];
                 $stripePaid = $chargeJson['paid'];
                 $stripeSourceID = $chargeJson['source']['id'];
                 $stripeLastDigit = $chargeJson['source']['last4'];
                 if($stripeStatus == "succeeded"){

                     $itemIDs = $request->session()->get('productIDs'); //Item IDs array
                     $itemIDs = array_count_values($itemIDs);

                     if(session()->has('posDiscount')){
                         $discount = session()->get('posDiscount'); //Item Discount
                     }else{
                         $discount = 0; //Item Discount
                     }

                     $productIDs = '';
                     $quantity = '';
                     $price = '';

                     foreach ($itemIDs as $key => $item) {
                         $productIDs .= $key . ',';
                         $quantity .= $item . ',';
                         $price .= Product::find($key)->price . ',';
                     }

                     $sale = new Sale;
                     $sale->product = $productIDs;
                     $sale->description = strip_tags($request->description);
                     $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
                     $sale->category = "";
                     $sale->merchant = 0;
                     $sale->unit = 0;
                     $sale->quantity = $quantity;
                     $sale->price = $price;
                     $sale->commission = $discount;
                     $sale->total = 0;
                     $sale->save();

                     $balance = new Balances;
                     $balance->type = "Income";
                     $balance->title = "POS Payment ( " . $request->customer . "-" . $request->customer_phone . ")";
                     $balance->description = strip_tags($request->description);
                     $balance->balancedate = Carbon::now();
                     $balance->amount = $convertedAmount;
                     $balance->by = Auth::user()->name;
                     $balance->save();

                     $latestSale = Sale::latest('id')->first();
                     $invoice = new Invoices;
                     $invoice->saleid = $latestSale->id;
                     $invoice->merchant = 0;
                     $invoice->invoicedate = Carbon::now();
                     $invoice->orderid = Carbon::parse($request->saledate)->format('Y-m-d') . "-" . rand(10, 99);
                     $invoice->paymentdue = "";
                     $invoice->product1 = "";
                     $invoice->quantity1 = 0;
                     $invoice->paymentstatus = "Paid";
                     $invoice->paidamount = $convertedAmount;
                     $invoice->paidmethod = "Stripe";
                     $invoice->paidinfo = $stripeID . " (" . $stripeLastDigit .  ")";
                     $invoice->customer = $request->customer;
                     $invoice->customer_phone = $request->customer_phone;
                     $invoice->save();

                     $latestInvoice = Invoices::latest('id')->first();
                     $paymentsDB = new Payments();
                     $paymentsDB->invoiceid = $latestInvoice->id;
                     $paymentsDB->paymentid = $stripeTnxID;
                     $paymentsDB->paymentmethod = "Stripe";
                     $paymentsDB->payeremail = $stripeID;
                     $paymentsDB->payerfname = $stripeID;
                     $paymentsDB->payerlname = $stripeID;
                     $paymentsDB->payerid = $stripeSourceID;
                     $paymentsDB->amount = $convertedAmount;
                     $paymentsDB->currency = $stripeCurrency;
                     $paymentsDB->Save();

                     \session()->forget('productIDs');
                     \session()->forget('posShipping');
                     \session()->forget('posTax');
                     \session()->forget('posDiscount');
                     \session()->forget('posLastInvoice');

                     \session()->put('posLastInvoice', $latestInvoice->id);

                     return redirect('/pos')->with('success', 'Payment Successful');

                 }else{
                     return redirect('/pos')->with('error', 'Payment is not successful.');
                 }
             } catch(Exception $e){
                 return redirect('/pos')->with('error', $e->getMessage());
             }

        }else if($paymentMethod == 3){ //Others

            $itemIDs = $request->session()->get('productIDs'); //Item IDs array
            $itemIDs = array_count_values($itemIDs);

            if(session()->has('posDiscount')){
                $discount = session()->get('posDiscount'); //Item Discount
            }else{
                $discount = 0; //Item Discount
            }

            $productIDs = '';
            $quantity = '';
            $price = '';

            foreach ($itemIDs as $key => $item) {
                $productIDs .= $key . ',';
                $quantity .= $item . ',';
                $price .= Product::find($key)->price . ',';
            }

            $sale = new Sale;
            $sale->product = $productIDs;
            $sale->description = strip_tags($request->description);
            $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
            $sale->category = "";
            $sale->merchant = 0;
            $sale->unit = 0;
            $sale->quantity = $quantity;
            $sale->price = $price;
            $sale->commission = $discount;
            $sale->total = 0;
            $sale->save();

            $balance = new Balances;
            $balance->type = "Income";
            $balance->title = "POS Payment ( " . $request->customer . "-" . $request->customer_phone . ")";
            $balance->description = strip_tags($request->description);
            $balance->balancedate = Carbon::now();
            $balance->amount = $convertedAmount;
            $balance->by = Auth::user()->name;
            $balance->save();

            $latestSale = Sale::latest('id')->first();
            $invoice = new Invoices;
            $invoice->saleid = $latestSale->id;
            $invoice->merchant = 0;
            $invoice->invoicedate = Carbon::now();
            $invoice->orderid = Carbon::parse($request->saledate)->format('Y-m-d') . "-" . rand(10, 99);
            $invoice->paymentdue = "";
            $invoice->product1 = "";
            $invoice->quantity1 = 0;
            $invoice->paymentstatus = "Paid";
            $invoice->paidamount = $convertedAmount;
            $invoice->paidmethod = "Others";
            $invoice->paidinfo = "N/A";
            $invoice->customer = $request->customer;
            $invoice->customer_phone = $request->customer_phone;
            $invoice->save();

            $latestInvoice = Invoices::latest('id')->first();
            $paymentsDB = new Payments();
            $paymentsDB->invoiceid = $latestInvoice->id;
            $paymentsDB->paymentid = "N/A";
            $paymentsDB->paymentmethod = "Others";
            $paymentsDB->payeremail = "N/A";
            $paymentsDB->payerfname = "N/A";
            $paymentsDB->payerlname = "N/A";
            $paymentsDB->payerid = "N/A";
            $paymentsDB->amount = $convertedAmount;
            $paymentsDB->currency = $systemCurrency;
            $paymentsDB->Save();

            \session()->forget('productIDs');
            \session()->forget('posShipping');
            \session()->forget('posTax');
            \session()->forget('posDiscount');
            \session()->forget('posLastInvoice');

            \session()->put('posLastInvoice', $latestInvoice->id);

            return redirect('/pos')->with('success', 'Payment Successful');
        }

     }


    public function paypalStatus(Request $request) {

        $payment_id = Session()->get('paypal_payment_id');
        $customer = Session()->get('customer');
        $customer_phone = Session()->get('customer_phone');
        $description = Session()->get('description');

        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            return redirect('/pos')->with('error', 'Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') { // payment made

            $paymentID = $result->getId();
            $paymentMethod = $result->getPayer()->getPaymentMethod();
            $payerEmail = $result->getPayer()->getPayerInfo()->getEmail();
            $payerFName = $result->getPayer()->getPayerInfo()->getFirstName();
            $payerLName = $result->getPayer()->getPayerInfo()->getLastName();
            $payerID = $result->getPayer()->getPayerInfo()->getPayerId();

            $payAmount = $result->getTransactions()[0]->getAmount()->getTotal();
            $payCurrency = $result->getTransactions()[0]->getAmount()->getCurrency();

            $itemIDs = $request->session()->get('productIDs'); //Item IDs array
            $itemIDs = array_count_values($itemIDs);

            if(session()->has('posDiscount')){
                $discount = session()->get('posDiscount'); //Item Discount
            }else{
                $discount = 0; //Item Discount
            }

            $productIDs = '';
            $quantity = '';
            $price = '';

            foreach ($itemIDs as $key => $item) {
                $productIDs .= $key . ',';
                $quantity .= $item . ',';
                $price .= Product::find($key)->price . ',';
            }

            $sale = new Sale;
            $sale->product = $productIDs;
            $sale->description = $description;
            $sale->saledate = Carbon::parse($request->saledate)->format('Y-m-d H:i:s');
            $sale->category = "";
            $sale->merchant = 0;
            $sale->unit = 0;
            $sale->quantity = $quantity;
            $sale->price = $price;
            $sale->commission = $discount;
            $sale->total = 0;
            $sale->save();

            $balance = new Balances;
            $balance->type = "Income";
            $balance->title = "POS Payment ( " . $customer . "-" . $customer_phone . ")";
            $balance->description = $description;
            $balance->balancedate = Carbon::now();
            $balance->amount = $payAmount;
            $balance->by = Auth::user()->name;
            $balance->save();

            $latestSale = Sale::latest('id')->first();
            $invoice = new Invoices;
            $invoice->saleid = $latestSale->id;
            $invoice->merchant = 0;
            $invoice->invoicedate = Carbon::now();
            $invoice->orderid = Carbon::parse($request->saledate)->format('Y-m-d') . "-" . rand(10, 99);
            $invoice->paymentdue = "";
            $invoice->product1 = "";
            $invoice->quantity1 = 0;
            $invoice->paymentstatus = "Paid";
            $invoice->paidamount = $payAmount;
            $invoice->paidmethod = $paymentMethod;
            $invoice->paidinfo = $payerEmail . " (" . $payerID .  ")";
            $invoice->customer = $customer;
            $invoice->customer_phone = $customer_phone;
            $invoice->save();

            $latestInvoice = Invoices::latest('id')->first();
            $paymentsDB = new Payments();
            $paymentsDB->invoiceid = $latestInvoice->id;
            $paymentsDB->paymentid = $paymentID;
            $paymentsDB->paymentmethod = $paymentMethod;
            $paymentsDB->payeremail = $payerEmail;
            $paymentsDB->payerfname = $payerFName;
            $paymentsDB->payerlname = $payerLName;
            $paymentsDB->payerid = $payerID;
            $paymentsDB->amount = $payAmount;
            $paymentsDB->currency = $payCurrency;
            $paymentsDB->Save();

            \session()->forget('productIDs');
            \session()->forget('posShipping');
            \session()->forget('posTax');
            \session()->forget('posDiscount');
            \session()->forget('posLastInvoice');

            \session()->put('posLastInvoice', $latestInvoice->id);

            return redirect('/pos')->with('success', 'Payment Successful');
        }

        return redirect('/pos')->with('error', 'Unexpected error occurred & payment has been failed.');

    }


}
