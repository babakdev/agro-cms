<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Assets;
use Image;

class AssetController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('asset.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('asset.add');
    }

    public function all() {
        $data['assets'] = Assets::all();
        return view('asset.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'value' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $asset = new Assets;

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $asset->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/asset');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $asset->photo);
        }

        $asset->title = $request->title;
        $asset->value = $request->value;
        $asset->description = $request->description;
        $asset->save();
        return redirect('/asset/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $asset = Assets::findOrFail($id);
        return view('asset.show')->with('asset', $asset);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['asset'] = Assets::findOrFail($id);
        return view('asset.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'value' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $asset = Assets::find($id);

        $image = $request->file('photo');
        if ($request->file('photo')) {
            $asset->photo = time() . '.' . $image->getClientOriginalExtension();
            $imagePath = public_path('/images/asset');
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . '/' . $asset->photo);
        }

        $asset->title = $request->title;
        $asset->value = $request->value;
        $asset->description = $request->description;
        $asset->save();
        return redirect('/asset/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Assets::find($id)->delete();
        return redirect('/asset/all')->with('success', 'Successfully Deleted');
    }

}
