<?php

/*
| -----------------------------------------------------
| PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
| -----------------------------------------------------
| AUTHOR: ONEZEROART TEAM
| -----------------------------------------------------
| EMAIL: support@onezeroart.com
| -----------------------------------------------------
| COPYRIGHT: RESERVED BY ONEZEROART.COM
| -----------------------------------------------------
| AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
| -----------------------------------------------------
| WEBSITE: http://onezeroart.com
| -----------------------------------------------------
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Balances;
use Carbon;

class BalanceController extends Controller {

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkverify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('balance.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('balance.add');
    }

    public function all() {
        $data['todayincome'] = Balances::orderBy('id','DESC')->where('type', 'Income')->whereDay('balancedate', date('d'))->get();
        $data['todayexpense'] = Balances::orderBy('id','DESC')->where('type', 'Expense')->whereDay('balancedate', date('d'))->get();

        $data['monthincome'] = Balances::orderBy('id','DESC')->where('type', 'Income')->whereMonth('balancedate', date('m'))->get();
        $data['monthexpense'] = Balances::orderBy('id','DESC')->where('type', 'Expense')->whereMonth('balancedate', date('m'))->get();

        $data['yearincome'] = Balances::orderBy('id','DESC')->where('type', 'Income')->whereYear('balancedate', date('Y'))->get();
        $data['yearexpense'] = Balances::orderBy('id','DESC')->where('type', 'Expense')->whereYear('balancedate', date('Y'))->get();
        $data['balances'] = Balances::orderBy('id','DESC')->get();
        return view('balance.all')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'type' => 'required',
            'title' => 'required',
            'amount' => 'required',
            'balancedate' => 'required',
        ]);

        $balance = new Balances;
        $balance->type = $request->type;
        $balance->title = $request->title;
        $balance->description = $request->description;
        $balance->balancedate = Carbon::parse($request->balancedate)->format('Y-m-d H:i:s');
        $balance->amount = $request->amount;
        $balance->by = $request->by;
        $balance->save();
        return redirect('/balance/')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $balance = Balances::findOrFail($id);
        return view('balance.show')->with('balance', $balance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['balance'] = Balances::findOrFail($id);
        return view('balance.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'type' => 'required',
            'title' => 'required',
            'amount' => 'required',
            'balancedate' => 'required',
        ]);

        $balance = Balances::find($id);
        $balance->type = $request->type;
        $balance->title = $request->title;
        $balance->description = $request->description;
        $balance->balancedate = date('Y-m-d H:i:s', strtotime($request->balancedate));
        $balance->amount = $request->amount;
        $balance->by = $request->by;
        $balance->save();
        return redirect('/balance/all')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Balances::find($id)->delete();
        return redirect('/balance/all')->with('success', 'Successfully Deleted');
    }


    public function byyear(Request $request) {

        $this->validate($request, [
            'year' => 'required',
        ]);


        $data['janincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('January')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['febincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('February')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['marincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('March')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['aprincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('April')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['mayincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('May')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['junincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('June')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['julincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('July')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['augincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('August')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['sepincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('September')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['octincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('October')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['novincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('November')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['decincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m', strtotime('December')))->whereYear('balancedate', date($request->year))->sum('amount');

        $data['janexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('January')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['febexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('February')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['marexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('March')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['aprexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('April')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['mayexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('May')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['junexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('June')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['julexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('July')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['augexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('August')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['sepexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('September')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['octexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('October')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['novexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('November')))->whereYear('balancedate', date($request->year))->sum('amount');
        $data['decexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m', strtotime('December')))->whereYear('balancedate', date($request->year))->sum('amount');

        $data['todayincome'] = Balances::where('type', 'Income')->whereDay('balancedate', date('d'))->get();
        $data['todayexpense'] = Balances::where('type', 'Expense')->whereDay('balancedate', date('d'))->get();

        $data['monthincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m'))->get();
        $data['monthexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m'))->get();

        $data['yearincome'] = Balances::where('type', 'Income')->whereYear('balancedate', date('Y'))->get();
        $data['yearexpense'] = Balances::where('type', 'Expense')->whereYear('balancedate', date('Y'))->get();

        $data['byyears'] = Balances::all(); //delete this
        $data['balances'] = Balances::all();

        return view('balance.byyear')->with($data);
    }


    public function bymonth(Request $request) {

        $this->validate($request, [
            'month' => 'required',
            'year' => 'required',
        ]);

        $data['bymonths'] = Balances::whereMonth('balancedate', date($request->month))->whereYear('balancedate', date($request->year))->get();

        $data['todayincome'] = Balances::where('type', 'Income')->whereDay('balancedate', date('d'))->get();
        $data['todayexpense'] = Balances::where('type', 'Expense')->whereDay('balancedate', date('d'))->get();

        $data['monthincome'] = Balances::where('type', 'Income')->whereMonth('balancedate', date('m'))->get();
        $data['monthexpense'] = Balances::where('type', 'Expense')->whereMonth('balancedate', date('m'))->get();

        $data['yearincome'] = Balances::where('type', 'Income')->whereYear('balancedate', date('Y'))->get();
        $data['yearexpense'] = Balances::where('type', 'Expense')->whereYear('balancedate', date('Y'))->get();
        $data['balances'] = Balances::all();

        return view('balance.bymonth')->with($data);
    }

}
