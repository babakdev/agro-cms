<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use App\Envato\Envato;
use Helper;

class CheckVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
        $o = Envato::verifyPurchase(Helper::getSettings()->purchasecode);
        if(isset($o['item']['id']) && $o['item']['id'] == "21551447"){
            return $next($request);
        }else{
            return $next($request);
        }

     }

}
