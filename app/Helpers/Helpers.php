<?php

/*
  | -----------------------------------------------------
  | PRODUCT NAME: ABACUS - BUSINESS MANAGEMENT SYSTEM (ABMS)
  | -----------------------------------------------------
  | AUTHOR: ONEZEROART TEAM
  | -----------------------------------------------------
  | EMAIL: support@onezeroart.com
  | -----------------------------------------------------
  | COPYRIGHT: RESERVED BY ONEZEROART.COM
  | -----------------------------------------------------
  | AUTHOR PORTFOLIO: https://codecanyon.net/user/onezeroart/portfolio
  | -----------------------------------------------------
  | WEBSITE: http://onezeroart.com
  | -----------------------------------------------------
 */

//app/Helpers/Envato/User.php

namespace App\Helpers;

use App\Envato\Envato;
use Illuminate\Support\Facades\DB;
use Carbon;

class Helper {

//    Retrive Name By ID
    public static function getNameByID($id, $table) {
        $data = DB::table($table)->where('id', $id)->first();
        if (isset($data->fname)) {
            return $data->fname . " " . $data->lname;
        }
    }

//    Retrive Name By ID
    public static function getProfileByID($id, $table) {
        $data = DB::table($table)->where('id', $id)->get();
        if (isset($data[0])) {
            return $data[0];
        } else {
            return 0;
        }
    }

//    Retrive Product Title By ID
    public static function getProductTitle($id) {
        $data = DB::table('products')->where('id', $id)->first();
        if (isset($data->title)) {
            return $data->title;
        }
    }

//    Retrive Product Details By ID
    public static function getProduct($id) {
        $data = DB::table('products')->where('id', $id)->get();
        if (isset($data[0])) {
            return $data[0];
        } else {
            return false;
        }
    }

//    Retrive Product Total Cost By ID
    public static function getProductCost($id) {
        $data = DB::table('products')->where('id', $id)->get();
        if (isset($data[0])) {
            return $data[0]->price + $data[0]->profit + $data[0]->cost1 + $data[0]->cost2 + $data[0]->cost3 + $data[0]->cost4 + $data[0]->cost5;
        } else {
            return 0;
        }
    }

//    Retrive Total Product Sale
    public static function getProductSale($id) {
        $data = DB::table('sales')->where('product', $id)->sum('quantity');
        return $data;
    }


//    Retrive Total Products Sale
    public static function tpsales() {
        //Product IDs
        $productIDs = DB::table('sales')->pluck('product');
        $totalProducts = 0;
        foreach ($productIDs as $prokey => $provalue){
            $productArr = explode(",", $provalue);
            foreach ($productArr as $prokey2 => $provalue2){
                if ($provalue2 == "") {
                    unset($productArr[$prokey2]);
                }
            }
            $totalProducts += count($productArr);
        }
        return $totalProducts;
    }


//    Retrive Total Products Sale Amount
    public static function tpsalesamount() {

        //All Sales
        $allSales = DB::table('sales')->get();

        $totalItemAmount = array();
        $totalItemCommission = array();

        foreach ($allSales as $key => $value) {

            $productIDs = $value->product;
            $productQtys = $value->quantity;
            $productPrices = $value->price;
            $saleCommission = $value->commission;

            //Product IDs
            $productArr = explode(",", $productIDs);
            foreach ($productArr as $prokey => $provalue){
                if ($provalue == "") {
                    unset($productArr[$prokey]);
                }
            }

            //Product Quantity
            $productQtyArr = explode(",", $productQtys);
            foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                if ($proQtyvalue == "") {
                    unset($productQtyArr[$proQtykey]);
                }
            }

            //Product Price
            $productPriceArr = explode(",", $productPrices);
            foreach ($productPriceArr as $proPrikey => $proPrivalue){
                if ($proPrivalue == "") {
                    unset($productPriceArr[$proPrikey]);
                }
            }

            foreach ($productArr as $productkey => $product){
                $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
            }
            $totalItemCommission[] = $saleCommission;
        }

        return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
    }


//    Retrive Total Products Sale This Month
    public static function tpsalesMonth() {
        //Product IDs
        $productIDs = DB::table('sales')->whereMonth('created_at', date('m'))->pluck('product');
        $totalProducts = 0;
        foreach ($productIDs as $prokey => $provalue){
            $productArr = explode(",", $provalue);
            foreach ($productArr as $prokey2 => $provalue2){
                if ($provalue2 == "") {
                    unset($productArr[$prokey2]);
                }
            }
            $totalProducts += count($productArr);
        }
        return $totalProducts;
    }


//    Retrive Total Products Sale Amount
    public static function tpsalesamountmonth() {

        //All Sales
        $allSales = DB::table('sales')->whereMonth('created_at', date('m'))->get();

        $totalItemAmount = array();
        $totalItemCommission = array();

        foreach ($allSales as $key => $value) {

            $productIDs = $value->product;
            $productQtys = $value->quantity;
            $productPrices = $value->price;
            $saleCommission = $value->commission;

            //Product IDs
            $productArr = explode(",", $productIDs);
            foreach ($productArr as $prokey => $provalue){
                if ($provalue == "") {
                    unset($productArr[$prokey]);
                }
            }

            //Product Quantity
            $productQtyArr = explode(",", $productQtys);
            foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                if ($proQtyvalue == "") {
                    unset($productQtyArr[$proQtykey]);
                }
            }

            //Product Price
            $productPriceArr = explode(",", $productPrices);
            foreach ($productPriceArr as $proPrikey => $proPrivalue){
                if ($proPrivalue == "") {
                    unset($productPriceArr[$proPrikey]);
                }
            }

            foreach ($productArr as $productkey => $product){
                $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
            }
            $totalItemCommission[] = $saleCommission;
        }

        return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
    }



//    Retrive Total Products Sale Amount BY Month & Year
    public static function tpsalesamountbymonth($month, $year) {

        //All Sales
        $allSales = DB::table('sales')->whereMonth('created_at', $month)->whereYear('created_at', $year)->get();

        $totalItemAmount = array();
        $totalItemCommission = array();

        foreach ($allSales as $key => $value) {

            $productIDs = $value->product;
            $productQtys = $value->quantity;
            $productPrices = $value->price;
            $saleCommission = $value->commission;

            //Product IDs
            $productArr = explode(",", $productIDs);
            foreach ($productArr as $prokey => $provalue){
                if ($provalue == "") {
                    unset($productArr[$prokey]);
                }
            }

            //Product Quantity
            $productQtyArr = explode(",", $productQtys);
            foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                if ($proQtyvalue == "") {
                    unset($productQtyArr[$proQtykey]);
                }
            }

            //Product Price
            $productPriceArr = explode(",", $productPrices);
            foreach ($productPriceArr as $proPrikey => $proPrivalue){
                if ($proPrivalue == "") {
                    unset($productPriceArr[$proPrikey]);
                }
            }

            foreach ($productArr as $productkey => $product){
                $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
            }
            $totalItemCommission[] = $saleCommission;
        }

        return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
    }



    //    Retrive Toady Total Sale
        public static function todayTSale() {

            //All Sales
            $allSales = DB::table('sales')->whereDay('created_at', date('d'))->get();

            $totalItemAmount = array();
            $totalItemCommission = array();

            foreach ($allSales as $key => $value) {

                $productIDs = $value->product;
                $productQtys = $value->quantity;
                $productPrices = $value->price;
                $saleCommission = $value->commission;

                //Product IDs
                $productArr = explode(",", $productIDs);
                foreach ($productArr as $prokey => $provalue){
                    if ($provalue == "") {
                        unset($productArr[$prokey]);
                    }
                }

                //Product Quantity
                $productQtyArr = explode(",", $productQtys);
                foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                    if ($proQtyvalue == "") {
                        unset($productQtyArr[$proQtykey]);
                    }
                }

                //Product Price
                $productPriceArr = explode(",", $productPrices);
                foreach ($productPriceArr as $proPrikey => $proPrivalue){
                    if ($proPrivalue == "") {
                        unset($productPriceArr[$proPrikey]);
                    }
                }

                foreach ($productArr as $productkey => $product){
                    $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                }
                $totalItemCommission[] = $saleCommission;
            }

            return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
        }


    //    Retrive Week Total Sale
        public static function weekTSale() {

            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            $allSales = DB::table('sales')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();

            $totalItemAmount = array();
            $totalItemCommission = array();

            foreach ($allSales as $key => $value) {

                $productIDs = $value->product;
                $productQtys = $value->quantity;
                $productPrices = $value->price;
                $saleCommission = $value->commission;

                //Product IDs
                $productArr = explode(",", $productIDs);
                foreach ($productArr as $prokey => $provalue){
                    if ($provalue == "") {
                        unset($productArr[$prokey]);
                    }
                }

                //Product Quantity
                $productQtyArr = explode(",", $productQtys);
                foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                    if ($proQtyvalue == "") {
                        unset($productQtyArr[$proQtykey]);
                    }
                }

                //Product Price
                $productPriceArr = explode(",", $productPrices);
                foreach ($productPriceArr as $proPrikey => $proPrivalue){
                    if ($proPrivalue == "") {
                        unset($productPriceArr[$proPrikey]);
                    }
                }

                foreach ($productArr as $productkey => $product){
                    $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                }
                $totalItemCommission[] = $saleCommission;
            }

            return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
        }


        //    Retrive Month Total Sale
            public static function monthTSale() {

                //All Sales
                $allSales = DB::table('sales')->whereMonth('created_at', date('m'))->get();

                $totalItemAmount = array();
                $totalItemCommission = array();

                foreach ($allSales as $key => $value) {

                    $productIDs = $value->product;
                    $productQtys = $value->quantity;
                    $productPrices = $value->price;
                    $saleCommission = $value->commission;

                    //Product IDs
                    $productArr = explode(",", $productIDs);
                    foreach ($productArr as $prokey => $provalue){
                        if ($provalue == "") {
                            unset($productArr[$prokey]);
                        }
                    }

                    //Product Quantity
                    $productQtyArr = explode(",", $productQtys);
                    foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                        if ($proQtyvalue == "") {
                            unset($productQtyArr[$proQtykey]);
                        }
                    }

                    //Product Price
                    $productPriceArr = explode(",", $productPrices);
                    foreach ($productPriceArr as $proPrikey => $proPrivalue){
                        if ($proPrivalue == "") {
                            unset($productPriceArr[$proPrikey]);
                        }
                    }

                    foreach ($productArr as $productkey => $product){
                        $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                    }
                    $totalItemCommission[] = $saleCommission;
                }

                return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
            }


            //    Retrive Year Total Sale
                public static function yearTSale() {

                    //All Sales
                    $allSales = DB::table('sales')->whereYear('created_at', date('Y'))->get();

                    $totalItemAmount = array();
                    $totalItemCommission = array();

                    foreach ($allSales as $key => $value) {

                        $productIDs = $value->product;
                        $productQtys = $value->quantity;
                        $productPrices = $value->price;
                        $saleCommission = $value->commission;

                        //Product IDs
                        $productArr = explode(",", $productIDs);
                        foreach ($productArr as $prokey => $provalue){
                            if ($provalue == "") {
                                unset($productArr[$prokey]);
                            }
                        }

                        //Product Quantity
                        $productQtyArr = explode(",", $productQtys);
                        foreach ($productQtyArr as $proQtykey => $proQtyvalue){
                            if ($proQtyvalue == "") {
                                unset($productQtyArr[$proQtykey]);
                            }
                        }

                        //Product Price
                        $productPriceArr = explode(",", $productPrices);
                        foreach ($productPriceArr as $proPrikey => $proPrivalue){
                            if ($proPrivalue == "") {
                                unset($productPriceArr[$proPrikey]);
                            }
                        }

                        foreach ($productArr as $productkey => $product){
                            $totalItemAmount[] = $productPriceArr[$productkey]  * $productQtyArr[$productkey];
                        }
                        $totalItemCommission[] = $saleCommission;
                    }

                    return $totalSubtotal = array_sum($totalItemAmount) - array_sum($totalItemCommission);
                }


//    Retrive Total Product Sale
    public static function getProductStock($id) {
        $data = DB::table('products')->where('id', $id)->get();
        return $data[0]->unitquantity;
    }

//    Retrive Total Product Sale
    public static function getSettings() {
        $data = DB::table('settings')->where('id', 1)->get();
        return $data[0];
    }

//    Retrive Currency
    public static function getCurrency() {
        $data = DB::table('settings')->where('id', 1)->get();
        return $data[0]->currency;
    }

//    Retrive Bank Info
    public static function getBank($id) {
        $data = DB::table('bankaccounts')->where('id', $id)->get();
        return $data[0];
    }

//    Retrive Bank Info
    public static function verifyCode() {
        $data = DB::table('settings')->where('id', 1)->get();
        $o = Envato::verifyPurchase($data[0]->purchasecode);
        if (isset($o['item']['id']) && $o['item']['name'] == "Abacus - Business Management System") {
            return true;
        } else {
            return true;
        }
    }

    //    Retrive Invoice Details By Sale ID
    public static function invoiceBySale($id) {
        $data = DB::table('invoices')->where('saleid', $id)->get();
        if (isset($data[0])) {
            return $data[0];
        } else {
            return false;
        }
    }

    //    Retrive Single Invoice Price
    public static function getInvoicePrice($id) {
        $invoice = DB::table('invoices')->where('id', $id)->get();
        if (isset($invoice[0])) {
            $invoice = $invoice[0];

            $totalSubtotal = 0;
            if ($invoice->product1) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product1) * $invoice->quantity1;
            }

            if ($invoice->product2) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product2) * $invoice->quantity2;
            }

            if ($invoice->product3) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product3) * $invoice->quantity3;
            }
            if ($invoice->product4) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product4) * $invoice->quantity4;
            }
            if ($invoice->product5) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product5) * $invoice->quantity5;
            }
            if ($invoice->product6) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product6) * $invoice->quantity6;
            }
            if ($invoice->product7) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product7) * $invoice->quantity7;
            }
            if ($invoice->product8) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product8) * $invoice->quantity8;
            }
            if ($invoice->product9) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product9) * $invoice->quantity9;
            }
            if ($invoice->product10) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product10) * $invoice->quantity10;
            }

            $discount = $totalSubtotal * $invoice->discount / 100;
            $lessdiscount = $totalSubtotal - $discount;
            $tax = $lessdiscount * $invoice->tax / 100;
            echo $total = $totalSubtotal - $discount + $tax + $invoice->other;
        }
    }

    //    Retrive Single Invoice Price
    public static function getInvTotalPaid() {
        $invoices = DB::table('invoices')->get();
        $totalPaid = array();
        foreach ($invoices as $invoice) {
            $totalPaid[] = $invoice->paidamount;
        }
        return array_sum($totalPaid);
    }

    //    Retrive Total Invoice Partial Due
    public static function getInvTotalDue() {
        $invoices = DB::table('invoices')->get();

        $totalValue = array();
        $totalPaid = array();

        foreach ($invoices as $invoice) {

            $totalSubtotal = 0;
            if ($invoice->product1) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product1) * $invoice->quantity1;
            }

            if ($invoice->product2) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product2) * $invoice->quantity2;
            }

            if ($invoice->product3) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product3) * $invoice->quantity3;
            }
            if ($invoice->product4) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product4) * $invoice->quantity4;
            }
            if ($invoice->product5) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product5) * $invoice->quantity5;
            }
            if ($invoice->product6) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product6) * $invoice->quantity6;
            }
            if ($invoice->product7) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product7) * $invoice->quantity7;
            }
            if ($invoice->product8) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product8) * $invoice->quantity8;
            }
            if ($invoice->product9) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product9) * $invoice->quantity9;
            }
            if ($invoice->product10) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product10) * $invoice->quantity10;
            }

            $discount = $totalSubtotal * $invoice->discount / 100;
            $lessdiscount = $totalSubtotal - $discount;
            $tax = $lessdiscount * $invoice->tax / 100;
            $totalValue[] = $totalSubtotal - $discount + $tax + $invoice->other; //Net Total Of Sale
            $totalPaid[] = $invoice->paidamount;

        }

        return array_sum($totalValue) - Helper::getInvTotalUnpaid() - array_sum($totalPaid);

    }



    //    Retrive Single Invoice Price
    public static function getInvTotalUnpaid() {
        $invoices = DB::table('invoices')->get();
        $totalunpaid = array();
        foreach ($invoices as $invoice) {

            $totalSubtotal = 0;
            if ($invoice->product1) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product1) * $invoice->quantity1;
            }

            if ($invoice->product2) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product2) * $invoice->quantity2;
            }

            if ($invoice->product3) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product3) * $invoice->quantity3;
            }
            if ($invoice->product4) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product4) * $invoice->quantity4;
            }
            if ($invoice->product5) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product5) * $invoice->quantity5;
            }
            if ($invoice->product6) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product6) * $invoice->quantity6;
            }
            if ($invoice->product7) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product7) * $invoice->quantity7;
            }
            if ($invoice->product8) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product8) * $invoice->quantity8;
            }
            if ($invoice->product9) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product9) * $invoice->quantity9;
            }
            if ($invoice->product10) {
                $totalSubtotal = $totalSubtotal + Helper::getProductCost($invoice->product10) * $invoice->quantity10;
            }

            $discount = $totalSubtotal * $invoice->discount / 100;
            $lessdiscount = $totalSubtotal - $discount;
            $tax = $lessdiscount * $invoice->tax / 100;
            $total = $totalSubtotal - $discount + $tax + $invoice->other; //Net Total Of Sale


            if ($invoice->paidamount == 0) {
                $totalunpaid[] = $total;
            }
        }

        return array_sum($totalunpaid);
    }

}
