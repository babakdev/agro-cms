<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');             
            $table->string('photo')->nullable();
            $table->string('title');
            $table->string('tag');
            $table->string('currency');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('postal')->nullable();
            $table->string('license1')->nullable();
            $table->string('license2')->nullable();
            $table->string('license3')->nullable();
            $table->string('purchasecode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
