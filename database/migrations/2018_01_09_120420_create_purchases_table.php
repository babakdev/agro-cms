<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('photo')->nullable();
            $table->longText('description')->nullable();
            $table->string('purchasedate')->nullable();
            $table->string('category')->nullable();
            $table->string('supplier')->nullable();
            $table->string('unit')->nullable();
            $table->string('quantity')->nullable();
            $table->string('company')->nullable();
            $table->string('price');
            $table->string('commission')->nullable();
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('purchases');
    }

}
