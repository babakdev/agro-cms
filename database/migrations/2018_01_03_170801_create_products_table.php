<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo')->nullable();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('code')->nullable();
            $table->string('category');
            $table->string('supplier');
            $table->string('model')->nullable();
            $table->string('unit')->nullable();
            $table->string('unitquantity')->nullable();
            $table->string('materials')->nullable();
            $table->string('weight')->nullable();
            $table->string('dimension')->nullable();
            $table->string('color')->nullable();
            $table->string('design')->nullable();
            $table->string('capacity')->nullable();
            $table->string('quality')->nullable();
            $table->string('origin')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('manufacturedate')->nullable();
            $table->string('expiredate')->nullable();
            $table->string('link')->nullable();
            
            $table->string('price');
            $table->string('profit')->nullable();
            
            $table->string('costtitle1')->nullable();
            $table->string('costtitle2')->nullable();
            $table->string('costtitle3')->nullable();
            $table->string('costtitle4')->nullable();
            $table->string('costtitle5')->nullable();
            $table->string('cost1')->nullable();
            $table->string('cost2')->nullable();
            $table->string('cost3')->nullable();
            $table->string('cost4')->nullable();
            $table->string('cost5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }

}
