<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('saleid')->nullable();
            $table->string('merchant');
            $table->string('invoicedate');
            $table->string('orderid');
            $table->string('paymentdue');
            $table->string('paymentmethod')->nullable();
            $table->string('paymentaccount')->nullable();
            $table->string('tax')->nullable();
            $table->string('other')->nullable();
            $table->string('discount')->nullable();
            $table->longText('note')->nullable();
            $table->string('paymentstatus')->nullable();
            $table->string('paidamount')->nullable();
            $table->string('paidmethod')->nullable();
            $table->string('paidinfo')->nullable();

            $table->string('product1');
            $table->string('product2')->nullable();
            $table->string('product3')->nullable();
            $table->string('product4')->nullable();
            $table->string('product5')->nullable();
            $table->string('product6')->nullable();
            $table->string('product7')->nullable();
            $table->string('product8')->nullable();
            $table->string('product9')->nullable();
            $table->string('product10')->nullable();

            $table->string('quantity1');
            $table->string('quantity2')->nullable();
            $table->string('quantity3')->nullable();
            $table->string('quantity4')->nullable();
            $table->string('quantity5')->nullable();
            $table->string('quantity6')->nullable();
            $table->string('quantity7')->nullable();
            $table->string('quantity8')->nullable();
            $table->string('quantity9')->nullable();
            $table->string('quantity10')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
