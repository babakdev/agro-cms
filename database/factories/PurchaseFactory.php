<?php

use Faker\Generator as Faker;

$factory->define(App\Purchases::class, function (Faker $faker) {
    return [
        'title' => $faker->words($nb = 3, true),
        'photo' => $faker->imageUrl(250, 250, 'cats'),
        'description' => $faker->paragraphs(rand(2, 5), true),
        'category' => $faker->words($nb = 3, true),
        'supplier' => $faker->name(),
        'materials' => $faker->words($nb = 3, true),
        'weight' => $faker->words($nb = 3, true),
        'dimension' => $faker->words($nb = 3, true),
        'color' => $faker->words($nb = 3, true),
        'quantity' => $faker->numberBetween(10, 150),
        'price' => $faker->numberBetween(10, 150),
    ];
});
