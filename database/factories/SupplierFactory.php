<?php

use Faker\Generator as Faker;

$factory->define(App\Supplier::class, function (Faker $faker) {
    return [
        'supplierid' => $faker->numberBetween(10, 100),
        'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'company' => $faker->company,
        'address' => $faker->address,
        'about' => $faker->paragraphs(rand(2, 5), true),
    ];
});
