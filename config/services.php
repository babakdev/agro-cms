<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAIL_DOMAIN'),
        'secret' => env('MAIL_SECRETKEY'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'facebook' => [
       'client_id' => env('FB_CLIENT_ID'), //Facebook API
       'client_secret' => env('FB_CLIENT_SECRET'), //Facebook Secret
       'redirect' => env('FB_REDIRECT'), //Redirect URL Where User Go After Successful Login
    ],
    
    'google' => [
       'client_id' => env('GOOGLE_CLIENT_ID'), //Google API
       'client_secret' => env('GOOGLE_CLIENT_SECRET'), //Google Secret
       'redirect' => env('GOOGLE_REDIRECT'), //Redirect URL Where User Go After Successful Login
    ],

];
